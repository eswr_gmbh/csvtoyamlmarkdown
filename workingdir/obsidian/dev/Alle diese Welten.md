---
Search In Goodreads: https://www.goodreads.com/search?q=Dennis%20E.%20Taylor%20-%20Alle%20diese%20Welten
Authors: Dennis E. Taylor
Ratings: 6193
Isbn 10: 345331932X
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Random House Audio, Deutschland
Isbn 13: 9783453319325
Book Numbers: 3
Tags: 
    - Naturwissenschaften
    - Weltraumerkundung
Added: 79
Categories: 
    - Science Fiction & Fantasy 
    - Science Fiction
Sample: https://samples.audible.de/bk/rhde/003772/bk_rhde_003772_sample.mp3
Asin: 3837146618
Title: "Alle diese Welten: Bobiverse 3"
Title Short: "Alle diese Welten"
Narrators: Simon Jäger
Blurb: "Hundert Jahre nachdem Bob Johansson ausgezogen ist, um auf fremden Planeten eine neue Heimat für die Menschheit zu finden, kann man sagen, dass..."
Series: Bobiverse (book 3)
Cover: https://m.media-amazon.com/images/I/51maU6nV63L._SL500_.jpg
Parent Category: Science Fiction & Fantasy
Store Page Url: https://audible.de/pd/3837146618?ipRedirectOverride=true&overrideBaseCountry=true
Length: 8 Std. 49 Min.
Summary: "<p>Der triumphale Höhepunkt von Dennis E. Taylors revolutionärer Space Opera.</p> <p>Hundert Jahre nachdem Bob Johansson ausgezogen ist, um auf fremden Planeten eine neue Heimat für die Menschheit zu finden, kann man sagen, dass er seine Mission erfüllt hat: Unzählige Kolonien haben er und seine Kopien in der Galaxis gegründet und die Menschen vor dem Aussterben bewahrt. Dabei haben sie sich allerdings einen mächtigen Feind in Form einer hoch entwickelten Alien-Zivilisation gemacht, die nun die Menschheit bedroht. Um den Kampf gegen die Aliens zu gewinnen, bleibt Bob und seinen Kopien nur eine Chance: Sie müssen in den Deep Space...</p>"
Child Category: Science Fiction
Web Player: https://www.audible.com/webplayer?asin=3837146618
Release Date: 2019-06-07
---
# Alle diese Welten: Bobiverse 3

## Alle diese Welten

### Bobiverse (book 3)

[https://m.media-amazon.com/images/I/51maU6nV63L._SL500_.jpg](https://m.media-amazon.com/images/I/51maU6nV63L._SL500_.jpg)
