---
Search In Goodreads: https://www.goodreads.com/search?q=Dennis%20E.%20Taylor%20-%20Ich%20bin%20viele
Authors: Dennis E. Taylor
Ratings: 11657
Isbn 10: 3453319206
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Random House Audio, Deutschland
Isbn 13: 9783453319202
Book Numbers: 1
Tags: 
    - Gentechnik
    - Naturwissenschaften
    - Weltraumerkundung
Added: 77
Progress: 5 Min. verbleibend
Categories: 
    - Science Fiction & Fantasy 
    - Science Fiction
Sample: https://samples.audible.de/bk/rhde/003542/bk_rhde_003542_sample.mp3
Asin: B07F2ZZRRP
Title: "Ich bin viele: Bobiverse 1"
Title Short: "Ich bin viele"
Narrators: Simon Jäger
Blurb: "Bob kann es nicht fassen. Eben hat er noch seine Software-Firma verkauft und einen Vertrag über das Einfrieren seines Körpers nach seinem Tod unterschrieben..."
Series: Bobiverse (book 1)
Cover: https://m.media-amazon.com/images/I/51tMQO+DlNL._SL500_.jpg
Parent Category: Science Fiction & Fantasy
Store Page Url: https://audible.de/pd/B07F2ZZRRP?ipRedirectOverride=true&overrideBaseCountry=true
Length: 11h 14m 
Summary: "<p>Bob kann es nicht fassen. Eben hat er noch seine Software-Firma verkauft und einen Vertrag über das Einfrieren seines Körpers nach seinem Tod unterschrieben, da ist es auch schon vorbei mit ihm. Er wird beim Überqueren der Straße überfahren. Hundert Jahre später wacht Bob wieder auf, allerdings nicht als Mensch, sondern als Künstliche Intelligenz, die noch dazu Staatseigentum ist. Prompt bekommt er auch gleich seinen ersten Auftrag: Er soll neue bewohnbare Planeten finden. Versagt er, wird er abgeschaltet. Für Bob beginnt ein grandioses Abenteuer zwischen den Sternen - und ein gnadenloser Wettlauf gegen die Zeit...</p>"
Child Category: Science Fiction
Web Player: https://www.audible.com/webplayer?asin=B07F2ZZRRP
Release Date: 2018-07-06
---
# Ich bin viele: Bobiverse 1

## Ich bin viele

### Bobiverse (book 1)

[https://m.media-amazon.com/images/I/51tMQO+DlNL._SL500_.jpg](https://m.media-amazon.com/images/I/51tMQO+DlNL._SL500_.jpg)
