---
Search In Goodreads: https://www.goodreads.com/search?q=Pia%20Kabitzsch%20-%20It's%20a%20date!
Authors: Pia Kabitzsch
Ratings: 66
Isbn 10: 3644012695
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Argon Verlag
Isbn 13: 9783644012691
Tags: 
    - Liebe
    - Partnersuche & Attraktivität
Added: 170
Progress: Beendet
Categories: 
    - Beziehungen
    - Elternschaft & persönliche Entwicklung 
    - Beziehungen
Sample: https://samples.audible.de/bk/argo/003575/bk_argo_003575_sample.mp3
Asin: B09XFD57VR
Title: "It's a date!: Tindern, Ghosting, große Gefühle. Was die Psychologie über Dating weiß"
Title Short: "It's a date!"
Narrators: Charlotte Puder
Blurb: "Wie du beim Dating zwar dein Herz riskierst, aber nicht den Kopf verlierst. Sich nach 36 Fragen rettungslos ineinander verlieben, funktioniert..."
Cover: https://m.media-amazon.com/images/I/41iqf7chdHL._SL500_.jpg
Parent Category: Beziehungen, Elternschaft & persönliche Entwicklung
Store Page Url: https://audible.de/pd/B09XFD57VR?ipRedirectOverride=true&overrideBaseCountry=true
Length: 7h 8m 
Summary: "<p>Wie du beim Dating zwar dein Herz riskierst, aber nicht den Kopf verlierst.</p> <p>Sich nach 36 Fragen rettungslos ineinander verlieben, funktioniert das? Ist Online-Dating wirklich so oberflächlich, wie alle denken? Was sagt die Wissenschaft zu bekannten Dating-Mythen wie der 3-Tage-Regel? Und ist die Generation Tinder wirklich beziehungsunfähig?</p> <p>Pia Kabitzsch erzählt in diesem Buch von den Ups und Downs ihres eigenen Dating-Lebens. Ihr Kompass im (Online-)Dating-Dschungel: die Wissenschaft. Denn die hält spannende Erkenntnisse bereit, die dabei helfen können, das gottverdammte Dating-Game zu meistern. Gefühlschaos optional, Spaß und Leichtigkeit garantiert!</p>"
Child Category: Beziehungen
Web Player: https://www.audible.com/webplayer?asin=B09XFD57VR
Release Date: 2022-04-27
---
# It's a date!: Tindern, Ghosting, große Gefühle. Was die Psychologie über Dating weiß

## It's a date!

[https://m.media-amazon.com/images/I/41iqf7chdHL._SL500_.jpg](https://m.media-amazon.com/images/I/41iqf7chdHL._SL500_.jpg)
