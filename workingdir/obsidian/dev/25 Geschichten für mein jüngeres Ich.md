---
Search In Goodreads: https://www.goodreads.com/search?q=Dr.%20Julian%20Hosp%20-%2025%20Geschichten%20f%26uuml%3Br%20mein%20j%26uuml%3Bngeres%20Ich
Authors: Dr. Julian Hosp
Ratings: 882
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Julian Hosp Coaching LTD
Tags: 
    - Geschäftsentwicklung & Unternehmertum
    - Persönlicher Erfolg
Added: 14
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Geschäftsentwicklung & Unternehmertum
Sample: https://samples.audible.de/bk/acx0/052509/bk_acx0_052509_sample.mp3
Asin: B01B8OL7T4
Title: "25 Geschichten für mein jüngeres Ich"
Title Short: "25 Geschichten für mein jüngeres Ich"
Narrators: Julian Hosp
Blurb: "Basierend auf diesem Credo beschreibt Dr. Julian Hosp, was andere Leute davon lernen können, wie er bereits mit 16 Jahren alleine von österreich nach Amerika gezogen ist...."
Cover: https://m.media-amazon.com/images/I/41RtasaOS-L._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B01B8OL7T4?ipRedirectOverride=true&overrideBaseCountry=true
Length: 9h 36m 
Summary: "<p>\"Fehler zu machen ist reine Dummheit!\" </p> <p>\"Wirklich?\" </p> <p>\"Ja, wenn du von jemand anderem, der diese Fehler bereits vor dir gemacht hat, lernen könntest, sie zu vermeiden!\" </p> <p>Basierend auf diesem Credo beschreibt Dr. Julian Hosp, was andere Leute davon lernen können, wie er bereits mit 16 Jahren alleine von österreich nach Amerika gezogen ist. Warum einen Baum zu pflanzen ein wichtiges Verständnis für viele Dinge wie Universitätsabschluss oder Unternehmensgründung ist. Warum seine Erfahrungen als Profikitesurfer und -basketballer nicht nur im Extremsport anzuwenden sind, sondern auch auf Erfahrungen die wir täglich erleben zutreffen. Warum er zwar erfolgreich Medizin studiert hat, nun aber nicht als Arzt arbeitet, sondern als Unternehmer nach Hong Kong gezogen ist und wie er diesen Schritt gewagt hat. Und wie er aus einer Totalpleite in Brasilien, wo er fast EUR 100.000 im Alter von 24 Jahren verlor, Einblicke in den Immobilien- und Aktienmarkt gewann, um so nur fünf Jahre später finanziell frei geworden zu sein. </p> <p>Julian teilt all das und noch viel mehr, indem er Situationen beschreibt, die nicht nur auf ihn zutreffen, sondern uns allen tagtäglich widerfahren. Statt standardisierter, praxisferner Tipps aus anderen Erfolgsbüchern, gibt Julian handfeste und praxisnahe Anweisungen in 75 Lektionen, die in 25 spannenden Geschichten verpackt sind. </p> <p>Sie werden dich dazu inspirieren jene Entscheidungen im Leben zu treffen, welche du schon oft treffen wolltest, bis jetzt aber nie getan hast. </p> <p>Wenn du: </p> <p></p> <ul> <li>Etwas Zusatzmotivation brauchst, um den Schritt aus deiner Komfortzone zu wagen und dich persönlich weiterzuentwickeln </li> <li>Gerade dein Studium abgeschlossen hast oder noch mitten in deiner Schulbil </li> </ul> <p></p>"
Child Category: Geschäftsentwicklung & Unternehmertum
Web Player: https://www.audible.com/webplayer?asin=B01B8OL7T4
Release Date: 2016-02-01
---
# 25 Geschichten für mein jüngeres Ich

## 25 Geschichten für mein jüngeres Ich

![https://m.media-amazon.com/images/I/41RtasaOS-L._SL500_.jpg](https://m.media-amazon.com/images/I/41RtasaOS-L._SL500_.jpg)
