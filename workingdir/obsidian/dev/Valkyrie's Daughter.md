---
Search In Goodreads: https://www.goodreads.com/search?q=John%20E.%20Siers%20-%20Valkyrie's%20Daughter
Authors: John E. Siers
Format: Ungekürztes Hörbuch
Language: English
Publishers: Theogony Books
Book Numbers: 3
Tags: 
    - Abenteuer
    - Militär
    - Space Opera
Added: 192
Progress: Beendet
Categories: 
    - Science Fiction & Fantasy 
    - Science Fiction
Sample: https://samples.audible.de/bk/acx0/300178/bk_acx0_300178_sample.mp3
Asin: B09V38CHH8
Title: "Valkyrie's Daughter: The Lunar Free State, Book 3"
Title Short: "Valkyrie's Daughter"
Narrators: Jimmy Moreland
Blurb: "The alien race Akara have a secret; they've discovered the location of a planet that is home to a primitive human civilization, another ancient “seeding\" of the legendary Progenitors...."
Series: The Lunar Free State (book 3)
Cover: https://m.media-amazon.com/images/I/51m203uDpFL._SL500_.jpg
Parent Category: Science Fiction & Fantasy
Store Page Url: https://audible.de/pd/B09V38CHH8?ipRedirectOverride=true&overrideBaseCountry=true
Length: 12h 49m 
Summary: "<p><b>The Otuka are hunting a most dangerous game!</b></p> <p>The alien race Akara have a secret; they've discovered the location of a planet that is home to a primitive human civilization, another ancient “seeding\" of the legendary Progenitors. But, this planet lies in the territory of the Otuka, spacefaring alien predators who consider human flesh to be a delicacy and the planet to be their private hunting preserve. Unwilling to confront the Otuka, the Akara have passed the problem on to their human allies, the Lunar Free State. Now, the LFS needs to decide what to do about it.</p> <p>Are the “Moonies\" willing to take responsibility for an entire planet's population? One thing seems certain: a favorite food of the Otuka is about to get much more expensive, as the LFS Marines stand ready to teach the Otuka hunters a hard lesson in the perils of hunting the humans in this part of the galaxy. But, no one knows the technology state of the Okuta.</p> <p><b>Will the Marines be the hunters or find themselves being hunted in turn? </b></p>"
Child Category: Science Fiction
Web Player: https://www.audible.com/webplayer?asin=B09V38CHH8
Release Date: 2022-03-08
---
