---
Search In Goodreads: https://www.goodreads.com/search?q=Stefan%20Merath%20-%20Der%20Weg%20zum%20erfolgreichen%20Unternehmer
Authors: Stefan Merath
Ratings: 3085
Isbn 10: 389749793X
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Gabal
Isbn 13: 9783897497931
Tags: 
    - Management & Leadership
Added: 118
Progress: <1 min verbleibend
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/gaba/000118/bk_gaba_000118_sample.mp3
Asin: B004V0LW0C
Title: "Der Weg zum erfolgreichen Unternehmer"
Title Short: "Der Weg zum erfolgreichen Unternehmer"
Narrators: Heiko Grauel, Sonngard Dressler
Blurb: "Du bist Selbstständiger oder Unternehmer? Und du hattest einen Traum? Den Traum von Freiheit und Selbstbestimmung..."
Cover: https://m.media-amazon.com/images/I/51EtZbHqnrL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B004V0LW0C?ipRedirectOverride=true&overrideBaseCountry=true
Length: 14h 17m 
Summary: "<p>Du bist Selbstständiger oder Unternehmer? Und du hattest einen Traum? Den Traum von Freiheit und Selbstbestimmung? Etwas bewirken oder ändern zu wollen? Und davon blieben nur Überlastung, Finanzprobleme sowie nervige Kunden oder Mitarbeiter übrig?</p> <p>Dann ist jetzt die Zeit für eine grundlegende Änderung! Das Hörbuch <i>Der Weg zum erfolgreichen Unternehmer</i> ist DAS Standardwerk für Unternehmer mit bis zu 50 Mitarbeitern geworden. Es hat nachweisbar die Leben von mehreren zehntausend Unternehmern verändert. Einer war fast pleite und sechs Jahre später bester Arbeitgeber Deutschlands. Andere reduzierten ihre 80-Stunden-Wochen auf 20 Stunden pro Woche. Manche verdoppelten ihre Umsätze in einem Jahr und wieder andere gewannen Strategiepreise oder bei Great Place To Work. Noch einer brachte sein Elf-Mitarbeiter-Unternehmen zum Fliegen und verkaufte es zwei Jahre später für einen mittleren zweistelligen Millionenbetrag. Und in vielen Fällen führten diese Änderungen auch zu einer besseren Gesundheit und einem erfüllteren Familienleben.</p> <p>Was dieses Hörbuch von vielen anderen Business-Hörbüchern unterscheidet: Der Autor weiß nicht nur theoretisch, wie etwas funktionieren könnte, sondern ist selbst seit über 25 Jahren Unternehmer mit fünf Unternehmen und teils über 30 Mitarbeitern - reines Praxiswissen mit breitem theoretischen Fundament.</p> <p>Was dieses Hörbuch noch von vielen anderen Business-Hörbüchern unterscheidet: Es ist nicht brottrocken, sondern erzählt eine spannende, leicht lesbare und dennoch tiefgründige Geschichte, mit der sich jeder identifizieren kann. <i>Der Weg zum erfolgreichen Unternehmer</i> ist die Geschichte des Unternehmers Thomas Willmann, der lernt, sich und sein Unternehmen aus einer ganz neuen Perspektive zu sehen. Er baut sein Unternehmen um und spürt hinterher selbst wieder die Freiheit und Begeisterung, die uns alle hat Unternehmer werden lassen. Einfach, spannend und für jeden nachvollziehbar. Und vor allem, da aus der Praxis entstanden, auch umsetzbar.</p> <p>Wenn das Unternehmen wächst, hast du zwei Möglichkeiten: Entweder du wächst als Persönlichkeit mit und hast Erfolg. Oder das Unternehmen wächst dir über den Kopf und du gehst unter!</p> <p>Du hast die Wahl. Jetzt.</p>"
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=B004V0LW0C
Release Date: 2009-10-28
---
# Der Weg zum erfolgreichen Unternehmer

## Der Weg zum erfolgreichen Unternehmer

[https://m.media-amazon.com/images/I/51EtZbHqnrL._SL500_.jpg](https://m.media-amazon.com/images/I/51EtZbHqnrL._SL500_.jpg)
