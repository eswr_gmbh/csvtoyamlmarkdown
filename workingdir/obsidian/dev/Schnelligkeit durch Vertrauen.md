---
Search In Goodreads: https://www.goodreads.com/search?q=Stephen%20M.%20R.%20Covey%20-%20Schnelligkeit%20durch%20Vertrauen
Authors: Stephen M. R. Covey, Rebecca R. Merrill
Ratings: 197
Isbn 10: 3897499088
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Gabal
Isbn 13: 9783897499089
Tags: 
    - Management & Leadership
Added: 41
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/gaba/000160/bk_gaba_000160_sample.mp3
Asin: B004SC8JFO
Title: "Schnelligkeit durch Vertrauen: Die unterschätzte ökonomische Macht"
Title Short: "Schnelligkeit durch Vertrauen"
Narrators: Heiko Grauel, Gisa Bergmann
Blurb: "Vertrauen ist einer der Schlüsselfaktoren erfolgreicher Unternehmensführung. Covey räumt auf mit der Vorstellung, Vertrauen lasse sich wirtschaftlich nicht messen..."
Cover: https://m.media-amazon.com/images/I/51Cy7tHjo-L._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B004SC8JFO?ipRedirectOverride=true&overrideBaseCountry=true
Length: 11h 47m 
Summary: "Vertrauen ist einer der Schlüsselfaktoren erfolgreicher Unternehmensführung. Covey räumt auf mit der Vorstellung, Vertrauen lasse sich wirtschaftlich nicht messen. Er zeigt anhand von zahlreichen Beispielen und Fakten, dass erst Vertrauen den ökonomischen Erfolg nachhaltig sichert. \"Vertrauen hat man oder eben nicht.\" - Ein fataler Irrtum! Vertrauen lässt sich systematisch aufbauen. Der Hörer erfährt, wie er dabei ganz konkret vorgeht - als Einzelner, im Team, in der gesamten Organisation, beim Kunden und im Markt."
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=B004SC8JFO
Release Date: 2011-03-18
---
# Schnelligkeit durch Vertrauen: Die unterschätzte ökonomische Macht

## Schnelligkeit durch Vertrauen

[https://m.media-amazon.com/images/I/51Cy7tHjo-L._SL500_.jpg](https://m.media-amazon.com/images/I/51Cy7tHjo-L._SL500_.jpg)
