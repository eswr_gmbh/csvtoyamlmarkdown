---
Search In Goodreads: https://www.goodreads.com/search?q=Hourly%20History%20-%20Thomas%20Edison
Authors: Hourly History
Ratings: 2
Isbn 10: 1520674465
Format: Ungekürztes Hörbuch
My Rating: 1
Language: English
Publishers: Hourly History
Isbn 13: 9781520674469
Tags: 
    - Wissenschaft & Technik
    - Nord-
    - Mittel- & Südamerika
Added: 55
Progress: Beendet
Categories: 
    - Biografien & Erinnerungen 
    - Akademiker & Spezialisten
Sample: https://samples.audible.de/bk/acx0/115646/bk_acx0_115646_sample.mp3
Asin: B07CYZS8N6
Title: "Thomas Edison: A Life from Beginning to End"
Title Short: "Thomas Edison"
Narrators: Jimmy Kieffer
Blurb: "Thomas Edison passed on many decades ago, but his inventions still echo loudly through time. If you watch TV, listen to your favorite songs, or simply click on the lamp next to your bed, it was Thomas Edison who brought all of these innovations into the world...."
Cover: https://m.media-amazon.com/images/I/51IpT501QuL._SL500_.jpg
Parent Category: Biografien & Erinnerungen
Store Page Url: https://audible.de/pd/B07CYZS8N6?ipRedirectOverride=true&overrideBaseCountry=true
Length: 1h 3m 
Summary: "<p>Thomas Edison passed on many decades ago, but his inventions still echo loudly through time. If you watch TV, listen to your favorite songs, or simply click on the lamp next to your bed, it was Thomas Edison who brought all of these innovations into the world.  </p> <p>Inside you will hear about...  </p> <ul> <li>Edison's early life  </li> <li>The electric light  </li> <li>The war of the currents  </li> <li>Other inventions and projects  </li> <li>Final years and death  </li> <li>Edison's legacy  </li> </ul> <p> And much more! </p> <p>Edison is sometimes regarded as someone who loved arguing with other inventors who were going in different directions from him, yet his tenacity and dedication to his own work were what made so many of his inventions workable.  </p> <p>No matter which way you look at Edison, from failed businessman, renowned inventor, distant father to his children, to an argumentative scientist, there is one thing everyone can agree on: Thomas Edison was pure genius. After all, in his world, nothing less would do.</p>"
Child Category: Akademiker & Spezialisten
Web Player: https://www.audible.com/webplayer?asin=B07CYZS8N6
Release Date: 2018-05-11
---
# Thomas Edison: A Life from Beginning to End

## Thomas Edison

[https://m.media-amazon.com/images/I/51IpT501QuL._SL500_.jpg](https://m.media-amazon.com/images/I/51IpT501QuL._SL500_.jpg)
