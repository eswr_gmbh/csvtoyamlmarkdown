---
Search In Goodreads: https://www.goodreads.com/search?q=Henry%20Hazlitt%20-%20Economics%20in%20One%20Lesson
Authors: Henry Hazlitt
Ratings: 48
Isbn 10: 0307760626
Format: Ungekürztes Hörbuch
My Rating: 5
Language: English
Publishers: Blackstone Audio, Inc.
Isbn 13: 9780307760623
Tags: 
    - Wirtschaft & Karriere
    - Ökonomie
    - Persönliche Finanzen
Added: 130
Progress: 5 Std. 40 Min. verbleibend
Categories: 
    - Geld & Finanzen 
    - Ökonomie
Sample: https://samples.audible.de/bk/blak/000058/bk_blak_000058_sample.mp3
Asin: B004V3FH28
Title: "Economics in One Lesson"
Title Short: "Economics in One Lesson"
Narrators: Jeff Riggenbach
Blurb: "Called by H.L. Mencken, \"one of the few economists in history who could really write,\" Henry Hazlitt achieved lasting fame..."
Cover: https://m.media-amazon.com/images/I/51MaWgvRFrL._SL500_.jpg
Parent Category: Geld & Finanzen
Store Page Url: https://audible.de/pd/B004V3FH28?ipRedirectOverride=true&overrideBaseCountry=true
Length: 6h 55m 
Summary: "<p>Called by H.L. Mencken, \"one of the few economists in history who could really write,\" Henry Hazlitt achieved lasting fame for his brilliant but concise work. In it, he explains basic truths about economics and the economic fallacies responsible for unemployment, inflation, high taxes, and recession. Covering considerable ground, Hazlitt illustrates the destructive effects of taxes, rent and price controls, inflation, trade restrictions, and minimum wage laws. And, he writes about key classical liberal thinkers like John Locke, Adam Smith, Thomas Jefferson, John Stuart Mill, Alexis de Tocqueville, and Herbert Spencer. </p>"
Child Category: Ökonomie
Web Player: https://www.audible.com/webplayer?asin=B004V3FH28
Release Date: 1999-12-15
---
# Economics in One Lesson

## Economics in One Lesson

[https://m.media-amazon.com/images/I/51MaWgvRFrL._SL500_.jpg](https://m.media-amazon.com/images/I/51MaWgvRFrL._SL500_.jpg)
