---
Search In Goodreads: https://www.goodreads.com/search?q=Robert%20Greene%20-%20The%20Laws%20of%20Human%20Nature
Authors: Robert Greene
Ratings: 538
Isbn 10: 0698184548
Format: Ungekürztes Hörbuch
My Rating: 4
Language: English
Publishers: Penguin Audio
Isbn 13: 9780698184541
Tags: 
    - Management & Leadership
    - Erfolg im Beruf
    - Seelische & Geistige Gesundheit
    - Persönlicher Erfolg
Added: 35
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/peng/004049/bk_peng_004049_sample.mp3
Asin: B07DKV5TZJ
Title: "The Laws of Human Nature"
Title Short: "The Laws of Human Nature"
Narrators: Paul Michael, Robert Greene
Blurb: "Robert Greene is a master guide, distilling wisdom and philosophy into essential texts for seekers of power, understanding, and mastery. Now he turns to the most important subject of all - understanding people's drives and motivations, even when they are unconscious of them themselves...."
Cover: https://m.media-amazon.com/images/I/41y1paqEhmL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B07DKV5TZJ?ipRedirectOverride=true&overrideBaseCountry=true
Length: 28h 26m 
Summary: "<p><b>From the number-one </b><b><i>New York Times </i></b><b>best-selling author of </b><b><i>The 48 Laws of Power</i></b><b> comes the definitive new audiobook on decoding the behavior of the people around you.</b></p> <p>Robert Greene is a master guide for millions of listeners, distilling ancient wisdom and philosophy into essential texts for seekers of power, understanding, and mastery. Now he turns to the most important subject of all - understanding people's drives and motivations, even when they are unconscious of them themselves.  </p> <p>We are social animals. Our very lives depend on our relationships with people. Knowing why people do what they do is the most important tool we can possess, without which our other talents can only take us so far. Drawing from the ideas and examples of Pericles, Queen Elizabeth I, Martin Luther King Jr, and many others, Greene teaches us how to detach ourselves from our own emotions and master self-control, how to develop the empathy that leads to insight, how to look behind people's masks, and how to resist conformity to develop your singular sense of purpose. Whether at work, in relationships, or in shaping the world around you, <i>The Laws of Human Nature</i> offers brilliant tactics for success, self-improvement, and self-defense.</p>"
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=B07DKV5TZJ
Release Date: 2018-10-23
---
# The Laws of Human Nature

## The Laws of Human Nature

[https://m.media-amazon.com/images/I/41y1paqEhmL._SL500_.jpg](https://m.media-amazon.com/images/I/41y1paqEhmL._SL500_.jpg)
