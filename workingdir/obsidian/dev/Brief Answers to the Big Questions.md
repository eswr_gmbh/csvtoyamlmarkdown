---
Search In Goodreads: https://www.goodreads.com/search?q=Stephen%20Hawking%20-%20Brief%20Answers%20to%20the%20Big%20Questions
Authors: Stephen Hawking, Professor Kip Thorne - foreword
Ratings: 330
Isbn 10: 1473696003
Format: Ungekürztes Hörbuch
Language: English
Publishers: John Murray
Isbn 13: 9781473696006
Tags: 
    - Wissenschaft
Added: 144
Progress: Beendet
Categories: 
    - Wissenschaft & Technik 
    - Wissenschaft
Sample: https://samples.audible.de/bk/hodd/001959/bk_hodd_001959_sample.mp3
Asin: B07D4QF4LF
Title: "Brief Answers to the Big Questions"
Title Short: "Brief Answers to the Big Questions"
Narrators: Ben Whishaw, Garrick Hagon - foreword, Lucy Hawking - afterword
Blurb: "The final book from Professor Stephen Hawking, the best-selling author of A Brief History of Time and arguably the most famous scientist of our age...."
Cover: https://m.media-amazon.com/images/I/41Y1tbNChaL._SL500_.jpg
Parent Category: Wissenschaft & Technik
Store Page Url: https://audible.de/pd/B07D4QF4LF?ipRedirectOverride=true&overrideBaseCountry=true
Length: 4h 52m 
Summary: "<p>Read by Ben Whishaw and Garrick Hagon, with an afterword read by Lucy Hawking.</p> <p>The world-famous cosmologist and number one best-selling author of <i>A Brief History of Time</i> leaves us with his final thoughts on the universe's biggest questions in this brilliant posthumous work.</p> <p>How did the universe begin? Will humanity survive on Earth? Is there intelligent life beyond our solar system? Could artificial intelligence ever outsmart us?</p> <p>Throughout his extraordinary career, Stephen Hawking expanded our understanding of the universe and unravelled some of its greatest mysteries. But even as his theoretical work on black holes, imaginary time and multiple histories took his mind to the furthest reaches of space, Hawking always believed that science could also be used to fix the problems on our planet.</p> <p>And now, as we face potentially catastrophic changes here on Earth - from climate change to dwindling natural resources to the threat of artificial superintelligence - Stephen Hawking turns his attention to the most urgent issues for humankind.</p> <p>Wide-ranging, intellectually stimulating, passionately argued and infused with his characteristic humour, <i>Brief Answers to the Big Questions</i>, the final book from one of the greatest minds in history, is a personal view on the challenges we face as a human race and where we, as a planet, are heading next.</p>"
Child Category: Wissenschaft
Web Player: https://www.audible.com/webplayer?asin=B07D4QF4LF
Release Date: 2018-10-16
---
# Brief Answers to the Big Questions

## Brief Answers to the Big Questions

[https://m.media-amazon.com/images/I/41Y1tbNChaL._SL500_.jpg](https://m.media-amazon.com/images/I/41Y1tbNChaL._SL500_.jpg)
