---
Search In Goodreads: https://www.goodreads.com/search?q=Carlo%20Rovelli%20-%20Sieben%20kurze%20Lektionen%20%26uuml%3Bber%20Physik
Authors: Carlo Rovelli
Ratings: 165
Isbn 10: 3644052212
Format: Ungekürztes Hörbuch
Language: German
Publishers: Argon Verlag
Isbn 13: 9783644052215
Tags: 
    - Philosophie
    - Wissenschaft
Added: 199
Categories: 
    - Politik & Sozialwissenschaften 
    - Philosophie
Sample: https://samples.audible.de/bk/argo/001016/bk_argo_001016_sample.mp3
Asin: B0153G2TTK
Title: "Sieben kurze Lektionen über Physik"
Title Short: "Sieben kurze Lektionen über Physik"
Narrators: Frank Arnold
Blurb: "\"Von Natur aus wollen wir immer mehr wissen und immer weiter lernen. Unser Wissen über die Welt wächst. Uns treibt der Drang nach Erkenntnis..."
Cover: https://m.media-amazon.com/images/I/61VH6s9xeIL._SL500_.jpg
Parent Category: Politik & Sozialwissenschaften
Store Page Url: https://audible.de/pd/B0153G2TTK?ipRedirectOverride=true&overrideBaseCountry=true
Length: 1 Std. 47 Min.
Summary: "\"Von Natur aus wollen wir immer mehr wissen und immer weiter lernen. Unser Wissen über die Welt wächst. Uns treibt der Drang nach Erkenntnis und lernend stoßen wir an Grenzen. In den tiefsten Tiefen des Raumgewebes, im Ursprung des Kosmos, im Wesen der Zeit, im Schicksal der Schwarzen Löcher und im Funktionieren unseres eigenen Denkens. Hier, an den Grenzen unseres Wissens, wo sich das Meer unseres Nichtwissens vor uns auftut, leuchten das Geheimnis der Welt, die Schönheit der Welt, und es verschlägt uns den Atem.\", schreibt Carlo Rovelli. <br> <br>Wo kommen wir her? Seit ihren umwälzenden Entdeckungen im 20. Jahrhundert spüren Physiker den Kräften und Teilchen nach, die die Welt im Innersten und Äußersten zusammenhalten. Was können wir wissen? Für jedermann verständlich, hat Carlo Rovelli dieses zauberhafte Buch darüber geschrieben, das in eleganten, klaren Sätzen die Physik der Moderne erklärt: Einstein und die Relativitätstheorie, Max Planck und die Quantenmechanik, die Entstehung des Universums, Schwarze Löcher, die Elementarteilchen, die Beschaffenheit von Raum und Zeit - und die Loop-Theorie, Carlo Rovellis ureigenstes Arbeitsfeld. <br> <br>Physik, die jeder verstehen kann - ein Hörvergnügen zum Staunen, Genießen und Mitreden können..."
Child Category: Philosophie
Web Player: https://www.audible.com/webplayer?asin=B0153G2TTK
Release Date: 2015-09-24
---
