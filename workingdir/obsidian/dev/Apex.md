---
Search In Goodreads: https://www.goodreads.com/search?q=Ramez%20Naam%20-%20Apex
Authors: Ramez Naam
Ratings: 1597
Isbn 10: 3958352987
Format: Ungekürztes Hörbuch
My Rating: 3
Language: German
Publishers: Ronin Hörverlag
Isbn 13: 9783958352988
Book Numbers: 3
Tags: 
    - Technothriller
    - Postapokalyptisch
Added: 113
Progress: Beendet
Categories: 
    - Krimis & Thriller 
    - Thriller
Sample: https://samples.audible.de/bk/klan/000084/bk_klan_000084_sample.mp3
Asin: B0792XD8XC
Title: "Apex: Nexus-Trilogie 3"
Title Short: "Apex"
Narrators: Uve Teschner
Blurb: "Die Vereinigten Staaten, China und viele andere Länder werden von Aufständen erschüttert. Geheimnisse, Lügen und der technische Fortschritt..."
Series: Nexus-Trilogie (book 3)
Cover: https://m.media-amazon.com/images/I/51ZsRazipRL._SL500_.jpg
Parent Category: Krimis & Thriller
Store Page Url: https://audible.de/pd/B0792XD8XC?ipRedirectOverride=true&overrideBaseCountry=true
Length: 24h 33m 
Summary: "Die Vereinigten Staaten, China und viele andere Länder werden von Aufständen erschüttert. Geheimnisse, Lügen und der technische Fortschritt haben auf der ganzen Welt Schockwellen ausgelöst, die von Land zu Land und von Kopf zu Kopf ziehen. Polizeistreitkräfte liefern sich Kämpfe mit neural vernetzten Protestanten. Das Militär wurde mobilisiert, überall droht die politische Ordnung zusammenzubrechen. Die durch die Nano-Droge Nexus ausgelöste Revolution hat begonnen. <br> <br>Innerhalb dieser Wirren und in einer Welt, in der die Kinder jener tiefgreifenden Veränderungen des menschlichen Wesens an Einfluss gewinnen, verfolgt eine totgeglaubte Wissenschaftlerin einen Rachefeldzug, sämtliche elektronisch gesteuerten Systeme des Planeten unter ihre Kontrolle zu bringen und die Welt nach ihrem Bild neu zu erschaffen. Nichts wird jemals wieder so sein wie zuvor."
Child Category: Thriller
Web Player: https://www.audible.com/webplayer?asin=B0792XD8XC
Release Date: 2018-02-01
---
# Apex: Nexus-Trilogie 3

## Apex

### Nexus-Trilogie (book 3)

[https://m.media-amazon.com/images/I/51ZsRazipRL._SL500_.jpg](https://m.media-amazon.com/images/I/51ZsRazipRL._SL500_.jpg)
