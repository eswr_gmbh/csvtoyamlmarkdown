---
Search In Goodreads: https://www.goodreads.com/search?q=Adam%20Grant%20-%20Originals
Authors: Adam Grant, Sheryl Sandberg - foreword
Ratings: 208
Isbn 10: 0698405773
Format: Ungekürztes Hörbuch
Language: English
Publishers: Penguin Audio
Isbn 13: 9780698405776
Tags: 
    - Management & Leadership
    - Erfolg im Beruf
    - Arbeitsplatz- & Organisationsverhalten
    - Seelische & Geistige Gesundheit
    - Persönlicher Erfolg
Added: 129
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/peng/002742/bk_peng_002742_sample.mp3
Asin: B01A7Q86CU
Title: "Originals: How Non-Conformists Move the World"
Title Short: "Originals"
Narrators: Fred Sanders, Susan Denaker
Blurb: "With Give and Take, Adam Grant not only introduced a landmark new paradigm for success but also established himself as one of his generation’s most compelling and provocative thought leaders. In Originals he again addresses the challenge of improving the world...."
Cover: https://m.media-amazon.com/images/I/51tXnHCIW2L._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B01A7Q86CU?ipRedirectOverride=true&overrideBaseCountry=true
Length: 10 Std. 1 Min.
Summary: "<p>The number one <i>New York Times</i> best seller that examines how people can champion new ideas in their careers and everyday life - and how leaders can fight groupthink, from the author of <i>Think Again</i> and co-author of <i>Option B</i>.</p> <p>With <i>Give and Take</i>, Adam Grant not only introduced a landmark new paradigm for success but also established himself as one of his generation’s most compelling and provocative thought leaders. In <i>Originals</i> he again addresses the challenge of improving the world, but now from the perspective of becoming original: choosing to champion novel ideas and values that go against the grain, battle conformity, and buck outdated traditions. How can we originate new ideas, policies, and practices without risking it all?</p> <p>Using surprising studies and stories spanning business, politics, sports, and entertainment, Grant explores how to recognize a good idea, speak up without getting silenced, build a coalition of allies, choose the right time to act, and manage fear and doubt; how parents and teachers can nurture originality in children; and how leaders can build cultures that welcome dissent. Learn from an entrepreneur who pitches his start-ups by highlighting the reasons not to invest, a woman at Apple who challenged Steve Jobs from three levels below, an analyst who overturned the rule of secrecy at the CIA, a billionaire financial wizard who fires employees for failing to criticize him, and a TV executive who didn’t even work in comedy but saved <i>Seinfeld</i> from the cutting-room floor. The payoff is a set of groundbreaking insights about rejecting conformity and improving the status quo.</p>"
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=B01A7Q86CU
Release Date: 2016-02-02
---
# Originals: How Non-Conformists Move the World

## Originals

[https://m.media-amazon.com/images/I/51tXnHCIW2L._SL500_.jpg](https://m.media-amazon.com/images/I/51tXnHCIW2L._SL500_.jpg)
