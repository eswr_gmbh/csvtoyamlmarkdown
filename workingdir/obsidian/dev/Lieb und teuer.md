---
Search In Goodreads: https://www.goodreads.com/search?q=Ilan%20Stephani%20-%20Lieb%20und%20teuer
Authors: Ilan Stephani
Ratings: 275
Isbn 10: 3711051936
Format: Ungekürztes Hörbuch
Language: German
Publishers: Audible Studios
Isbn 13: 9783711051936
Tags: 
    - Frauen
Added: 145
Progress: 4 Std. 56 Min. verbleibend
Categories: 
    - Biografien & Erinnerungen 
    - Frauen
Sample: https://samples.audible.de/bk/adko/004289/bk_adko_004289_sample.mp3
Asin: B07N6C3WW9
Title: "Lieb und teuer: Was ich im Puff über das Leben gelernt habe"
Title Short: "Lieb und teuer"
Narrators: Jana Kozewa
Blurb: "Zwei Jahre lang arbeitet Ilan Stephani in einem Berliner Bordell als Prostituierte. Sie erschafft sich ein Alter Ego, mit dem sie diesen..."
Cover: https://m.media-amazon.com/images/I/41euNoYuTIL._SL500_.jpg
Parent Category: Biografien & Erinnerungen
Store Page Url: https://audible.de/pd/B07N6C3WW9?ipRedirectOverride=true&overrideBaseCountry=true
Length: 6h 13m 
Summary: "<p>Zwei Jahre lang arbeitet Ilan Stephani in einem Berliner Bordell als Prostituierte. Sie erschafft sich ein Alter Ego, mit dem sie diesen tabuisierten Randbereich der Gesellschaft erforscht. Neugierig begegnet die junge Frau dieser für sie bis dahin völlig unbekannten Welt und macht erstaunliche Entdeckungen: Statt Huren und Freiern im Zwielicht erlebt sie den Puff als Spiegel der Gesellschaft. Die Menschen hier haben mit denselben Ängsten, Mechanismen und Zuschreibungen zu kämpfen wie überall sonst, nur, dass sie offener damit umgehen.</p> <p>Sehr ehrlich und nachdenklich beschreibt die Autorin einen Mikrokosmos, in dem sie viel über die menschlichen Besonderheiten lernen konnte. Solidarität und Offenheit sind Werte, die überall gelebt werden können - dann ginge es allen besser, nicht nur im Bett.</p> <p><b>Wir weisen dich darauf hin, dass dieser Titel nicht für dich geeignet ist, wenn du unter 18 Jahre alt bist.</b><br>  </p> <p>>> Diese ungekürzte Hörbuch-Fassung genießt du exklusiv nur bei Audible.</p>"
Child Category: Frauen
Web Player: https://www.audible.com/webplayer?asin=B07N6C3WW9
Release Date: 2019-02-21
---
# Lieb und teuer: Was ich im Puff über das Leben gelernt habe

## Lieb und teuer

[https://m.media-amazon.com/images/I/41euNoYuTIL._SL500_.jpg](https://m.media-amazon.com/images/I/41euNoYuTIL._SL500_.jpg)
