---
Search In Goodreads: https://www.goodreads.com/search?q=Michael%20Leister%20-%20Drauf%20geschissen!
Authors: Michael Leister
Ratings: 1453
Isbn 10: 3948187002
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Ata Medien Verlag GmbH
Isbn 13: 9783948187002
Tags: 
    - Selbstwertgefühl
Added: 141
Progress: 33 Min. verbleibend
Categories: 
    - Beziehungen
    - Elternschaft & persönliche Entwicklung 
    - Persönliche Entwicklung
Sample: https://samples.audible.de/bk/atam/000508/bk_atam_000508_sample.mp3
Asin: 3948187029
Title: "Drauf geschissen!: Wie dir endlich egal wird, was die anderen denken"
Title Short: "Drauf geschissen!"
Narrators: Michael Leister
Blurb: "Das Hörbuch für alle, die müde davon sind, es immer nur den anderen recht zu machen. Für alle, die keine Angst mehr vor Geläster haben..."
Cover: https://m.media-amazon.com/images/I/51HCufUjX5L._SL500_.jpg
Parent Category: Beziehungen, Elternschaft & persönliche Entwicklung
Store Page Url: https://audible.de/pd/3948187029?ipRedirectOverride=true&overrideBaseCountry=true
Length: 3h 40m 
Summary: "<p>Das Hörbuch für alle, die müde davon sind, es immer nur den anderen recht zu machen. Für alle, die keine Angst mehr vor Geläster haben wollen. Für alle, die endlich ihr eigenes Ding durchziehen wollen und für alle, die keine Lust mehr auf den sozialen Druck des Alltags haben. Humorvoll, erstaunlich sachlich und ohne ein Blatt vor den Mund zu nehmen, beschreibt Bestsellerautor Michael Leister, wie wir gepflegt auf das scheißen, was uns davon abhält, das Leben zu genießen: Die Angst vor der Meinung unserer Mitmenschen.</p> <p>Warum ist es uns so wichtig, was andere über uns denken? Wie können wir uns vom sozialen Druck lösen? In welchen Lebensbereichen werden wir besonders durch die Meinung anderer eingeschränkt und vor allem: Wie schaffen wir es, wirklich darauf zu scheißen, ohne ein schlechter Mensch zu sein? Die Antworten auf diese Fragen und noch sehr viel mehr findest Du in diesem Hörbuch! Es ist ein erfrischendes Plädoyer für mehr Selbstbewusstsein, Unabhängigkeit und die Lebensfreude, die Du verdienst. Scheiß auf das, was Dich herunterzieht und fang an, das Leben zu genießen!</p>"
Child Category: Persönliche Entwicklung
Web Player: https://www.audible.com/webplayer?asin=3948187029
Release Date: 2019-07-26
---
# Drauf geschissen!: Wie dir endlich egal wird, was die anderen denken

## Drauf geschissen!

[https://m.media-amazon.com/images/I/51HCufUjX5L._SL500_.jpg](https://m.media-amazon.com/images/I/51HCufUjX5L._SL500_.jpg)
