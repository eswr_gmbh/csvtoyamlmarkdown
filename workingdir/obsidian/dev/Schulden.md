---
Search In Goodreads: https://www.goodreads.com/search?q=David%20Graeber%20-%20Schulden
Authors: David Graeber
Ratings: 219
Isbn 10: 3608947671
Format: Ungekürztes Hörbuch
Language: German
Publishers: Der Hörverlag
Isbn 13: 9783608947670
Tags: 
    - Politik & Regierungen
Added: 110
Categories: 
    - Politik & Sozialwissenschaften 
    - Politik & Regierungen
Sample: https://samples.audible.de/bk/hoer/001493/bk_hoer_001493_sample.mp3
Asin: B00W9V8LNQ
Title: "Schulden: Die ersten 5000 Jahre"
Title Short: "Schulden"
Narrators: Patrick Feiter
Blurb: "Seit der Erfindung des Kredits vor 5000 Jahren treibt das Versprechen auf Rückzahlung Menschen in die Sklaverei..."
Cover: https://m.media-amazon.com/images/I/41S+RpK2-qL._SL500_.jpg
Parent Category: Politik & Sozialwissenschaften
Store Page Url: https://audible.de/pd/B00W9V8LNQ?ipRedirectOverride=true&overrideBaseCountry=true
Length: 21 Std. 21 Min.
Summary: "Ein Plädoyer für einen radikalen Neuanfang <br> <br>Seit der Erfindung des Kredits vor 5000 Jahren treibt das Versprechen auf Rückzahlung Menschen in die Sklaverei. Die Geschichte der Menschheit erzählt David Graeber als eine Geschichte der Schulden: Eines moralischen Prinzips, das nur die Macht der Herrschenden stützt. Damit durchbricht er die Logik des Kapitalismus und befreit unser Denken vom Primat der Ökonomie. Schulden sind nur Versprechungen, so Graeber, und die Welt ist voll von Versprechungen, die nicht gehalten wurden. Jede grundlegende gesellschaftliche Veränderung aber beginnt mit der Frage: Wie können wir eine neue, eine bessere Welt schaffen?"
Child Category: Politik & Regierungen
Web Player: https://www.audible.com/webplayer?asin=B00W9V8LNQ
Release Date: 2015-04-20
---
# Schulden: Die ersten 5000 Jahre

## Schulden

[https://m.media-amazon.com/images/I/41S+RpK2-qL._SL500_.jpg](https://m.media-amazon.com/images/I/41S+RpK2-qL._SL500_.jpg)
