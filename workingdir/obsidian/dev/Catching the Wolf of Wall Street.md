---
Search In Goodreads: https://www.goodreads.com/search?q=Jordan%20Belfort%20-%20Catching%20the%20Wolf%20of%20Wall%20Street
Authors: Jordan Belfort
Ratings: 12
Isbn 10: 1444786849
Format: Ungekürztes Hörbuch
Language: English
Publishers: John Murray Learning
Isbn 13: 9781444786842
Tags: 
    - Wirtschaft
    - True Crime
    - Banken & Bankwesen
Added: 106
Categories: 
    - Biografien & Erinnerungen 
    - Akademiker & Spezialisten
Sample: https://samples.audible.de/bk/hodd/001921/bk_hodd_001921_sample.mp3
Asin: B07B7PZ7L6
Title: "Catching the Wolf of Wall Street: More Incredible True Stories of Fortunes, Schemes, Parties, and Prison"
Title Short: "Catching the Wolf of Wall Street"
Narrators: Ray Porter
Blurb: "Listen to what happened next in the follow-up to the best-selling The Wolf of Wall Street...."
Cover: https://m.media-amazon.com/images/I/61f1N78P3yL._SL500_.jpg
Parent Category: Biografien & Erinnerungen
Store Page Url: https://audible.de/pd/B07B7PZ7L6?ipRedirectOverride=true&overrideBaseCountry=true
Length: 16 Std. 39 Min.
Summary: "<p>Listen to what happened next in the follow-up to the best-selling <i>The Wolf of Wall Street</i>, now a major motion picture directed by Martin Scorsese.</p> <p>In the 1990s Jordan Belfort became one of the most infamous names in American finance: a brilliant, conniving stock chopper. He was the Wolf of Wall Street, whose life of greed, power and excess was so outrageous it could only be true; no-one could make this up! But the day Jordan was arrested and taken away in handcuffs was not the end of the madness.</p> <p><i>Catching the Wolf of Wall Street</i> tells of what happened next. After getting out of jail on $10 million bail, he had to choose whether to plead guilty and act as a government witness or fight the charges and see his wife be charged as well. He cooperated.</p> <p>With his trademark brash, brazen and thoroughly enthralling storytelling, Jordan details more incredible true tales of fortunes made and lost, moneymaking schemes, parties, sex, drugs, marriage, divorce and prison.</p>"
Child Category: Akademiker & Spezialisten
Web Player: https://www.audible.com/webplayer?asin=B07B7PZ7L6
Release Date: 2018-04-12
---
# Catching the Wolf of Wall Street: More Incredible True Stories of Fortunes, Schemes, Parties, and Prison

## Catching the Wolf of Wall Street

[https://m.media-amazon.com/images/I/61f1N78P3yL._SL500_.jpg](https://m.media-amazon.com/images/I/61f1N78P3yL._SL500_.jpg)
