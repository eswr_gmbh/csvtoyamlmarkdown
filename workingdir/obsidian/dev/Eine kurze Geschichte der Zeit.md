---
Search In Goodreads: https://www.goodreads.com/search?q=Stephen%20Hawking%20-%20Eine%20kurze%20Geschichte%20der%20Zeit
Authors: Stephen Hawking
Ratings: 4526
Isbn 10: 3499626004
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Der Hörverlag
Isbn 13: 9783499626005
Tags: 
    - Philosophie
    - Wissenschaft
Added: 64
Progress: Beendet
Categories: 
    - Politik & Sozialwissenschaften 
    - Philosophie
Sample: https://samples.audible.de/bk/hoer/001214/bk_hoer_001214_sample.mp3
Asin: B00FZZ50I2
Title: "Eine kurze Geschichte der Zeit"
Title Short: "Eine kurze Geschichte der Zeit"
Narrators: Frank Arnold
Blurb: "In diesem Buch erzählt Hawking die Geschichte der Kosmologie..."
Cover: https://m.media-amazon.com/images/I/41v1iUNASxL._SL500_.jpg
Parent Category: Politik & Sozialwissenschaften
Store Page Url: https://audible.de/pd/B00FZZ50I2?ipRedirectOverride=true&overrideBaseCountry=true
Length: 7h 56m 
Summary: "In diesem Buch erzählt Hawking die Geschichte der Kosmologie. Er beginnt mit dem archaischen Bild des Schildkrötenturms, der die Erde trägt, gelangt über Aristoteles, Newton und Galilei zu den Superstring-Theorien, die darauf bauen, daß die Raumzeit zehn oder gar 26 Dimensionen hat, macht den Leser mit den Grundlagen der Relativitätstheorie vertraut, der Quantenmechanik und der Quantentheorie."
Child Category: Philosophie
Web Player: https://www.audible.com/webplayer?asin=B00FZZ50I2
Release Date: 2013-10-21
---
# Eine kurze Geschichte der Zeit

## Eine kurze Geschichte der Zeit

[https://m.media-amazon.com/images/I/41v1iUNASxL._SL500_.jpg](https://m.media-amazon.com/images/I/41v1iUNASxL._SL500_.jpg)
