---
Search In Goodreads: https://www.goodreads.com/search?q=Robert%20Greene%20-%20Perfekt!
Authors: Robert Greene
Ratings: 339
Isbn 10: 3446438203
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Der Hörverlag
Isbn 13: 9783446438200
Tags: 
    - Management & Leadership
Added: 5
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/hoer/001173/bk_hoer_001173_sample.mp3
Asin: B00EOO76HI
Title: "Perfekt!: Der überlegene Weg zum Erfolg"
Title Short: "Perfekt!"
Narrators: Patrick Feiter
Blurb: "Wie haben wir Erfolg, ohne verbissen zu sein? Wie schaffen wir es, unser Privatleben und unseren Beruf so miteinander zu verbinden, dass sie sich ergänzen und bereichern..."
Cover: https://m.media-amazon.com/images/I/41rNDXKz-VL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B00EOO76HI?ipRedirectOverride=true&overrideBaseCountry=true
Length: 18h 40m 
Summary: "Wie haben wir Erfolg, ohne verbissen zu sein? Wie schaffen wir es, unser Privatleben und unseren Beruf so miteinander zu verbinden, dass sie sich ergänzen und bereichern? Wie schöpfen wir aus der Fülle, statt uns vom Alltag auffressen zu lassen? <br> <br>Um all diese Fragen geht es in \"Perfekt!\", dem neuen Hörbuch von Robert Greene. Und der Bestseller-Autor aus den USA bietet Lösungen: Mit Beispielen aus der Welt der Literatur und der Geschichte zeigt er, wie wir Schritt für Schritt herausfinden, wo unsere wirklichen Talente liegen und wie wir jene elegante Souveränität erlangen, nach der viele streben, die aber nur wenige erreichen."
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=B00EOO76HI
Release Date: 2013-08-26
---
# Perfekt!: Der überlegene Weg zum Erfolg

## Perfekt!

[https://m.media-amazon.com/images/I/41rNDXKz-VL._SL500_.jpg](https://m.media-amazon.com/images/I/41rNDXKz-VL._SL500_.jpg)
