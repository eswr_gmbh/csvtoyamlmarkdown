---
Search In Goodreads: https://www.goodreads.com/search?q=Cixin%20Liu%20-%20Der%20dunkle%20Wald
Authors: Cixin Liu
Ratings: 2587
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Random House Audio, Deutschland
Book Numbers: 2
Tags: 
    - Erstkontakt
Added: 42
Progress: Beendet
Categories: 
    - Science Fiction & Fantasy 
    - Science Fiction
Sample: https://samples.audible.de/bk/rhde/002970/bk_rhde_002970_sample.mp3
Asin: B07B2ZCS73
Title: "Der dunkle Wald: Die Trisolaris-Trilogie 2"
Title Short: "Der dunkle Wald"
Narrators: Mark Bremer
Blurb: "Der erste Kontakt mit einer außerirdischen Spezies hat die Menschheit in eine Krise gestürzt, denn die fremde Zivilisation hat sich Zugang zu jeglicher..."
Series: Die Trisolaris-Trilogie (book 2)
Cover: https://m.media-amazon.com/images/I/51qX3vH5KAL._SL500_.jpg
Parent Category: Science Fiction & Fantasy
Store Page Url: https://audible.de/pd/B07B2ZCS73?ipRedirectOverride=true&overrideBaseCountry=true
Length: 21h 53m 
Summary: "Invasion <br> <br>Der erste Kontakt mit einer außerirdischen Spezies hat die Menschheit in eine Krise gestürzt, denn die fremde Zivilisation hat sich Zugang zu jeglicher menschlichen Informationstechnologie verschafft. Der einzige Informationsspeicher, der noch vor den Aliens geschützt ist, ist das menschliche Gehirn, weshalb das Wallfacer-Projekt ins Leben gerufen wird: Vier Wissenschaftler sollen die ultimative Verteidigungsstrategie gegen die Aliens ausarbeiten - doch können sie einander trauen? <br> <br>Mark Bremer zieht den Hörer hinein in Lius gewaltigen Epos.<p>>> Diese ungekürzte Hörbuch-Fassung genießt du digital exklusiv nur bei Audible.</p>"
Child Category: Science Fiction
Web Player: https://www.audible.com/webplayer?asin=B07B2ZCS73
Release Date: 2018-03-09
---
# Der dunkle Wald: Die Trisolaris-Trilogie 2

## Der dunkle Wald

### Die Trisolaris-Trilogie (book 2)

[https://m.media-amazon.com/images/I/51qX3vH5KAL._SL500_.jpg](https://m.media-amazon.com/images/I/51qX3vH5KAL._SL500_.jpg)
