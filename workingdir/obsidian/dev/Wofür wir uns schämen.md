---
Search In Goodreads: https://www.goodreads.com/search?q=Tomas%20Blum%20-%20Wof%26uuml%3Br%20wir%20uns%20sch%26auml%3Bmen
Authors: Tomas Blum
Ratings: 2
Isbn 10: 3945491088
Format: Ungekürztes Hörbuch
Language: German
Publishers: Liesmich Verlag
Isbn 13: 9783945491089
Tags: 
    - Zeitgenössische Liebesromane
Added: 169
Progress: 5 Std. 7 Min. verbleibend
Categories: 
    - Liebesromane 
    - Zeitgenössische Liebesromane
Sample: https://samples.audible.de/bk/edel/015418/bk_edel_015418_sample.mp3
Asin: 394549110X
Title: "Wofür wir uns schämen"
Title Short: "Wofür wir uns schämen"
Narrators: Tomas Blum, Alina Tillenburg
Blurb: "Was hindert dich? Das düstere Ereignis in der Jugendzeit liegt so weit zurück, als würde es nicht mehr zu euch gehören. Zufällig trefft..."
Cover: https://m.media-amazon.com/images/I/41pjH+ezazL._SL500_.jpg
Parent Category: Liebesromane
Store Page Url: https://audible.de/pd/394549110X?ipRedirectOverride=true&overrideBaseCountry=true
Length: 6h 23m 
Summary: "<p>Was hindert dich? Das düstere Ereignis in der Jugendzeit liegt so weit zurück, als würde es nicht mehr zu euch gehören. Zufällig trefft ihr einander Jahrzehnte später im Geschäftsleben wieder, inzwischen als Kollegin und Kollege, und das Kennenlernen gerät zur Tour de Force mit der Erinnerung. Gregor und Marie haben keine Lust mehr auf Inszenierungen. Was hält sie bloß davon ab, zu lieben? So tritt hinter der Suche nach der Liebesbeziehung eine viel größere Sehnsucht hervor. Es ist die Suche nach dem Glück.</p>"
Child Category: Zeitgenössische Liebesromane
Web Player: https://www.audible.com/webplayer?asin=394549110X
Release Date: 2020-04-09
---
# Wofür wir uns schämen

## Wofür wir uns schämen

[https://m.media-amazon.com/images/I/41pjH+ezazL._SL500_.jpg](https://m.media-amazon.com/images/I/41pjH+ezazL._SL500_.jpg)
