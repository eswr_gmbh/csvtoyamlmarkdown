---
Search In Goodreads: https://www.goodreads.com/search?q=Matthew%20Dicks%20-%20Storyworthy
Authors: Matthew Dicks, Dan Kennedy - foreword
Ratings: 42
Isbn 10: 1608685489
Format: Ungekürztes Hörbuch
Language: English
Publishers: Brilliance Audio
Isbn 13: 9781608685486
Tags: 
    - Unterhaltung & Darstellende Künste
    - Erfolg im Beruf
    - Kommunikation & soziale Kompetenz
Added: 175
Progress: 25 Min. verbleibend
Categories: 
    - Kunst & Unterhaltung 
    - Unterhaltung & Darstellende Künste
Sample: https://samples.audible.de/bk/brll/011002/bk_brll_011002_sample.mp3
Asin: 197860517X
Title: "Storyworthy: Engage, Teach, Persuade, and Change Your Life Through the Power of Storytelling"
Title Short: "Storyworthy"
Narrators: Matthew Dicks, John Glouchevitch
Blurb: "A five-time Moth GrandSLAM winner and bestselling novelist shows how to tell a great story - and why doing so matters...."
Cover: https://m.media-amazon.com/images/I/51foPoGk58L._SL500_.jpg
Parent Category: Kunst & Unterhaltung
Store Page Url: https://audible.de/pd/197860517X?ipRedirectOverride=true&overrideBaseCountry=true
Length: 10h 1m 
Summary: "<p><b>A five-time Moth GrandSLAM winner and bestselling novelist shows how to tell a great story - and why doing so matters</b>.</p> <p>Whether we realize it or not, we are always telling stories. On a first date or job interview, at a sales presentation or therapy appointment, with family or friends, we are constantly narrating events and interpreting emotions and actions. In this compelling book, storyteller extraordinaire Matthew Dicks presents wonderfully straightforward and engaging tips and techniques for constructing, telling, and polishing stories that will hold the attention of your audience (no matter how big or small). He shows that anyone can learn to be an appealing storyteller, that everyone has something “storyworthy” to express, and, perhaps most important, that the act of creating and telling a tale is a powerful way of understanding and enhancing your own life.</p>"
Child Category: Unterhaltung & Darstellende Künste
Web Player: https://www.audible.com/webplayer?asin=197860517X
Release Date: 2018-08-27
---
# Storyworthy: Engage, Teach, Persuade, and Change Your Life Through the Power of Storytelling

## Storyworthy

[https://m.media-amazon.com/images/I/51foPoGk58L._SL500_.jpg](https://m.media-amazon.com/images/I/51foPoGk58L._SL500_.jpg)
