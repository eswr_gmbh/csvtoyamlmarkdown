---
Search In Goodreads: https://www.goodreads.com/search?q=Cixin%20Liu%20-%20Jenseits%20der%20Zeit
Authors: Cixin Liu
Ratings: 1972
Isbn 10: 3453317661
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Random House Audio, Deutschland
Isbn 13: 9783453317666
Book Numbers: 3
Tags: 
    - Erstkontakt
Added: 46
Progress: Beendet
Categories: 
    - Science Fiction & Fantasy 
    - Science Fiction
Sample: https://samples.audible.de/bk/rhde/003726/bk_rhde_003726_sample.mp3
Asin: 3837144763
Title: "Jenseits der Zeit: Die Trisolaris-Trilogie 3"
Title Short: "Jenseits der Zeit"
Narrators: Mark Bremer
Blurb: "Ein halbes Jahrhundert nach der Entscheidungsschlacht hält der Waffenstillstand mit den Trisolariern immer noch an. Die Hochtechnologie..."
Series: Die Trisolaris-Trilogie (book 3)
Cover: https://m.media-amazon.com/images/I/51D0pAZbKCL._SL500_.jpg
Parent Category: Science Fiction & Fantasy
Store Page Url: https://audible.de/pd/3837144763?ipRedirectOverride=true&overrideBaseCountry=true
Length: 26h 51m 
Summary: "<p>Ein halbes Jahrhundert nach der Entscheidungsschlacht hält der Waffenstillstand mit den Trisolariern immer noch an. Die Hochtechnologie der Außerirdischen hat der Erde zu neuem Wohlstand verholfen, auch die Trisolarier haben dazugelernt und eine friedliche Koexistenz scheint möglich. Als mit Cheng Xin eine Raumfahrtingenieurin des 21. Jahrhunderts aus dem Kälteschlaf erwacht, bringt sie das Wissen um ein längst vergangenes Geheimprogramm mit in die neue Zeit. Wird die junge Frau den Frieden mit Trisolaris ins Wanken bringen?</p> <p>Packend und temporeich gelesen von Mark Bremer.</p>"
Child Category: Science Fiction
Web Player: https://www.audible.com/webplayer?asin=3837144763
Release Date: 2019-04-05
---
# Jenseits der Zeit: Die Trisolaris-Trilogie 3

## Jenseits der Zeit

### Die Trisolaris-Trilogie (book 3)

[https://m.media-amazon.com/images/I/51D0pAZbKCL._SL500_.jpg](https://m.media-amazon.com/images/I/51D0pAZbKCL._SL500_.jpg)
