---
Search In Goodreads: https://www.goodreads.com/search?q=Cal%20Newport%20-%20Deep%20Work
Authors: Cal Newport
Ratings: 1728
Isbn 10: 0349413681
Format: Ungekürztes Hörbuch
My Rating: 4
Language: English
Publishers: Hachette Audio UK
Isbn 13: 9780349413686
Tags: 
    - Management & Leadership
Added: 30
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/twuk/001123/bk_twuk_001123_sample.mp3
Asin: B01CYLJ49U
Title: "Deep Work: Rules for Focused Success in a Distracted World"
Title Short: "Deep Work"
Narrators: Jeff Bottoms
Blurb: "Popular blogger Cal Newport reveals the new key to achieving success and true meaning in professional life...."
Cover: https://m.media-amazon.com/images/I/51EJRm2IHOL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B01CYLJ49U?ipRedirectOverride=true&overrideBaseCountry=true
Length: 7h 44m 
Summary: "<p>Popular blogger Cal Newport reveals the new key to achieving success and true meaning in professional life: the ability to master distraction. </p> <p>Many modern knowledge workers now spend most of their brain power battling distraction and interruption, whether because of the incessant pinging of devices, noisy open-plan offices or the difficulty of deciding what deserves their attention the most. When Cal Newport coined the term <i>deep work</i> on his popular blog, <i>Study Hacks</i>, in 2012, he found the concept quickly hit a nerve. Most of us, after all, are excruciatingly familiar with shallow work instead - distractedly skimming the surface of our workload and never getting to the important part. Newport began exploring the methods and mind-set that foster a practice of distraction-free productivity at work, and now, in <i>Deep Work</i>, he shows how anyone can achieve this elusive state. </p> <p>Through revealing portraits of both historical and modern-day thinkers, academics and leaders in the fields of technology, science and culture, and their deep work habits, Newport shares an inspiring collection of tools to wring every last drop of value out of your intellectual capacity. He explains why mastering this shift in work practices is crucial for anyone who intends to stay ahead in a complex information economy and how to systematically train the mind to focus. Put simply: developing and cultivating a deep work practice is one of the best decisions we can make in an increasingly distracted world. </p>"
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=B01CYLJ49U
Release Date: 2016-07-26
---
# Deep Work: Rules for Focused Success in a Distracted World

## Deep Work

[https://m.media-amazon.com/images/I/51EJRm2IHOL._SL500_.jpg](https://m.media-amazon.com/images/I/51EJRm2IHOL._SL500_.jpg)
