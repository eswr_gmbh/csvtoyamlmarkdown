---
Search In Goodreads: https://www.goodreads.com/search?q=Ramez%20Naam%20-%20Crux
Authors: Ramez Naam
Ratings: 2200
Format: Ungekürztes Hörbuch
My Rating: 4
Language: German
Publishers: Ronin Hörverlag
Book Numbers: 2
Tags: 
    - Technothriller
    - Fantasy
    - Science Fiction
Added: 111
Progress: Beendet
Categories: 
    - Krimis & Thriller 
    - Thriller
Sample: https://samples.audible.de/bk/klan/000035/bk_klan_000035_sample.mp3
Asin: B011CQHQM0
Title: "Crux: Nexus-Trilogie 2"
Title Short: "Crux"
Narrators: Uve Teschner
Blurb: "Die Erfindung der Nano-Droge Nexus hat die Menschheit in ein neues Zeitalter katapultiert. Die Verschmelzung von Mensch und Internet..."
Series: Nexus-Trilogie (book 2)
Cover: https://m.media-amazon.com/images/I/51MQ+8TPywL._SL500_.jpg
Parent Category: Krimis & Thriller
Store Page Url: https://audible.de/pd/B011CQHQM0?ipRedirectOverride=true&overrideBaseCountry=true
Length: 17h 27m 
Summary: "Die Erfindung der Nano-Droge Nexus hat die Menschheit in ein neues Zeitalter katapultiert. Die Verschmelzung von Mensch und Internet hat zu einer internationalen Terrorkrise geführt. Kade Lane, ein brillanter Forscher und Miterfinder von Nexus, findet sich plötzlich wieder in einem undurchsichtigen Kampf zwischen unerbittlichen US-Behörden und skrupellosen Terroristen. Sein größter Gegner, die Chinesin Su-Yong, tritt ihm als scheinbar übermächtiges Cyberwesen entgegen. Eine atemlose Jagd beginnt."
Child Category: Thriller
Web Player: https://www.audible.com/webplayer?asin=B011CQHQM0
Release Date: 2015-08-10
---
# Crux: Nexus-Trilogie 2

## Crux

### Nexus-Trilogie (book 2)

![https://m.media-amazon.com/images/I/51MQ+8TPywL._SL500_.jpg](https://m.media-amazon.com/images/I/51MQ+8TPywL._SL500_.jpg)
