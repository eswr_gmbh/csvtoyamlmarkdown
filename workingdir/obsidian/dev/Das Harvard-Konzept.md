---
Search In Goodreads: https://www.goodreads.com/search?q=Roger%20Fisher%20-%20Das%20Harvard-Konzept
Authors: Roger Fisher, William Ury, Bruce Patton
Ratings: 722
Isbn 10: 3593502674
Format: Ungekürztes Hörbuch
Language: German
Publishers: Der Hörverlag
Isbn 13: 9783593502670
Tags: 
    - Management & Leadership
Added: 89
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/hoer/002575/bk_hoer_002575_sample.mp3
Asin: 3844530142
Title: "Das Harvard-Konzept: Die unschlagbare Methode für beste Verhandlungsergebnisse"
Title Short: "Das Harvard-Konzept"
Narrators: Herbert Schäfer
Blurb: "DAS Standardwerk zum Thema Verhandeln - weltweit. Aktualisiert und erstmals als Komplettlesung! \"Das Harvard-Konzept\" hat die Kunst des..."
Cover: https://m.media-amazon.com/images/I/41YqvmQ0xuL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/3844530142?ipRedirectOverride=true&overrideBaseCountry=true
Length: 10h 0m 
Summary: "<p>DAS Standardwerk zum Thema Verhandeln - weltweit. Aktualisiert und erstmals als Komplettlesung!</p> <p>\"Das Harvard-Konzept\" hat die Kunst des Verhandelns radikal verändert. Es lehrt, sich auf Interessen zu konzentrieren und zwischen Menschen und Problemen stets zu trennen. So wird es möglich, dass Parteien zum beiderseitigen Vorteil verhandeln und Win-Win-Situationen schaffen. Egal ob politische Konflikte, Vertrags- und Gehaltsverhandlungen oder Tarifgespräche - dieses Konzept verändert die Art und Weise, wie wir Differenzen beilegen und Lösungen finden.</p> <p>Der Klassiker als Komplettlesung, neu übersetzt und um aktuelle Fallstudien aus dem europäischen Raum erweitert.</p> <p>In deiner Audible-Bibliothek findest du für dieses Hörerlebnis eine PDF-Datei mit zusätzlichem Material.</p>"
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=3844530142
Release Date: 2018-08-17
---
# Das Harvard-Konzept: Die unschlagbare Methode für beste Verhandlungsergebnisse

## Das Harvard-Konzept

[https://m.media-amazon.com/images/I/41YqvmQ0xuL._SL500_.jpg](https://m.media-amazon.com/images/I/41YqvmQ0xuL._SL500_.jpg)
