---
Search In Goodreads: https://www.goodreads.com/search?q=Paul%20VII%20-%20Agile%20Product%20Management%20Box%20Set
Authors: Paul VII
Format: Ungekürztes Hörbuch
Language: English
Publishers: Pashun Consulting Ltd.
Tags: 
    - Management & Leadership
    - Programmieren & Softwareentwicklung
Added: 88
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/acx0/071492/bk_acx0_071492_sample.mp3
Asin: B01MQ0NJ96
Title: "Agile Product Management Box Set: Product Backlog: 21 Tips & Scrum Master: 21 Sprint Problems, Impediments and Solutions"
Title Short: "Agile Product Management Box Set"
Narrators: Randal Schaffer
Blurb: "This class gives you a full introduction to the concept of the product backlog. I then walk you step by step through the steps involved in managing a backlog...."
Cover: https://m.media-amazon.com/images/I/61wWoINfukL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B01MQ0NJ96?ipRedirectOverride=true&overrideBaseCountry=true
Length: 2h 30m 
Summary: "<p>This class gives you a full introduction to the concept of the product backlog. I then walk you step by step through the steps involved in managing a backlog. Following this, I give you tips for improving product backlog management in your team or business from the ground up. Along the way, I give you plenty of examples and give you best practices for product backlog management within agile scrum. In this class, you will learn: </p> <p></p> <ul> <li>A brief recap of agile and scrum principles </li> <li>What is a product backlog and how is it different from traditional requirements documents </li> <li>How to create a product backlog from a product vision </li> <li>What user stories are and how they are simpler for managing requirements </li> <li>Concise techniques for improving your product backlog management </li> </ul> <p></p> <p>So let's get started and let me teach you how to improve product backlog management. </p>"
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=B01MQ0NJ96
Release Date: 2016-10-26
---
# Agile Product Management Box Set: Product Backlog: 21 Tips & Scrum Master: 21 Sprint Problems, Impediments and Solutions

## Agile Product Management Box Set

[https://m.media-amazon.com/images/I/61wWoINfukL._SL500_.jpg](https://m.media-amazon.com/images/I/61wWoINfukL._SL500_.jpg)
