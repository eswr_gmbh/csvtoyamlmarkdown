---
Search In Goodreads: https://www.goodreads.com/search?q=Scott%20E.%20Page%20-%20Understanding%20Complexity
Authors: Scott E. Page, The Great Courses
From Plus Catalog: true
Ratings: 144
Isbn 10: 1598035606
Format: Vortrag
My Rating: 4
Language: English
Publishers: The Great Courses
Isbn 13: 9781598035605
Tags: 
    - Ökonomie
    - Sozialwissenschaften
Added: 196
Progress: <1 min verbleibend
Categories: 
    - Geld & Finanzen 
    - Ökonomie
Sample: https://samples.audible.de/bk/tcco/001125/bk_tcco_001125_sample.mp3
Asin: 1629976849
Title: "Understanding Complexity"
Title Short: "Understanding Complexity"
Narrators: Scott E. Page
Blurb: "In recent years, the new and exciting field of complexity science has brought us concepts such as tipping points, the wisdom of crowds, six degrees of separation (or Kevin Bacon), and emergence...."
Cover: https://m.media-amazon.com/images/I/51FnCAYMjJL._SL500_.jpg
Parent Category: Geld & Finanzen
Store Page Url: https://audible.de/pd/1629976849?ipRedirectOverride=true&overrideBaseCountry=true
Length: 6h 4m 
Summary: "<p>Recent years have seen the introduction of concepts from the new and exciting field of complexity science that have captivated the attention of economists, sociologists, engineers, businesspeople, and many others. These include tipping points, the wisdom of crowds, six degrees of separation (or Kevin Bacon), and emergence. </p> <p>Interest in these intriguing concepts is widespread because of the utility of this field. Complexity science can shed light on why businesses or economies succeed and fail, how epidemics spread and can be stopped, and what causes ecological systems to rebalance themselves after a disaster. </p> <p>In fact, complexity science is a discipline that may well hold the key to unlocking the secrets of some of the most important forces on Earth. But it's also a science that remains largely unknown, even among well-educated people. </p> <p>Now you can discover and grasp the fundamentals and applications of this amazing field with <i>Understanding Complexity</i>. Professor Scott E. Page of the University of Michigan - one of the field's most highly regarded teachers, researchers, and real-world practitioners - introduces you to this vibrant and still evolving discipline. In 12 lucid lectures, you learn how complexity science helps us understand the nature and behavior of systems formed of financial markets, corporations, native cultures, governments, and more. </p> <p><b>PLEASE NOTE: When you purchase this title, the accompanying PDF will be available in your Audible Library along with the audio.</b> </p>"
Child Category: Ökonomie
Web Player: https://www.audible.com/webplayer?asin=1629976849
Release Date: 2019-03-20
---
