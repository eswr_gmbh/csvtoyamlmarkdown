---
Search In Goodreads: https://www.goodreads.com/search?q=Rolf%20Dobelli%20-%20Die%20Kunst%20des%20klaren%20Denkens%3A%2052%20Denkfehler%2C%20die%20Sie%20besser%20anderen%20%C3%BCberlassen
Authors: Rolf Dobelli
My Rating: 4
Tags: 
    - 
Added: 10
Progress: Beendet
Categories: 
    - 
Asin: B00VKS5YE8
Title: "Die Kunst des klaren Denkens: 52 Denkfehler, die Sie besser anderen überlassen"
Title Short: "Die Kunst des klaren Denkens: 52 Denkfehler, die Sie besser anderen überlassen"
Narrators: Frank Stöckle
Blurb: "Unser Gehirn ist für ein Leben als Jäger und Sammler optimiert. Heute leben wir in einer radikal anderen Welt. Das führt zu systematischen Denkfehlern..."
Cover: https://m.media-amazon.com/images/I/51Qi-HEle7L._SL500_.jpg
Store Page Url: https://audible.de/pd/B00VKS5YE8?ipRedirectOverride=true&overrideBaseCountry=true
Summary: ""
Store Page Missing: true
Web Player: https://www.audible.com/webplayer?asin=B00VKS5YE8
---
# Die Kunst des klaren Denkens: 52 Denkfehler, die Sie besser anderen überlassen

## Die Kunst des klaren Denkens: 52 Denkfehler, die Sie besser anderen überlassen

[https://m.media-amazon.com/images/I/51Qi-HEle7L._SL500_.jpg](https://m.media-amazon.com/images/I/51Qi-HEle7L._SL500_.jpg)
