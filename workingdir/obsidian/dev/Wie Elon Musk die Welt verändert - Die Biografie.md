---
Search In Goodreads: https://www.goodreads.com/search?q=Ashlee%20Vance%20-%20Wie%20Elon%20Musk%20die%20Welt%20ver%26auml%3Bndert%20-%20Die%20Biografie
Authors: Ashlee Vance, Elon Musk
Ratings: 13003
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: FinanzBuch Verlag
Tags: 
    - Wirtschaft
    - Wissenschaft & Technik
Added: 3
Progress: Beendet
Categories: 
    - Biografien & Erinnerungen 
    - Akademiker & Spezialisten
Sample: https://samples.audible.de/bk/riva/000024/bk_riva_000024_sample.mp3
Asin: B018GG91F4
Title: "Wie Elon Musk die Welt verändert - Die Biografie"
Title Short: "Wie Elon Musk die Welt verändert - Die Biografie"
Narrators: Michael J. Diekmann
Blurb: "Elon Musk ist der da Vinci des 21. Jahrhunderts. Seine Firmengründungen lesen sich wie das Who's who der zukunftsträchtigsten Unternehmen der Welt..."
Cover: https://m.media-amazon.com/images/I/51LJouP2j6L._SL500_.jpg
Parent Category: Biografien & Erinnerungen
Store Page Url: https://audible.de/pd/B018GG91F4?ipRedirectOverride=true&overrideBaseCountry=true
Length: 13h 7m 
Summary: "Elon Musk ist der da Vinci des 21. Jahrhunderts. Seine Firmengründungen lesen sich wie das Who's who der zukunftsträchtigsten Unternehmen der Welt. Alles, was dieser Mann anfasst, scheint zu Gold zu werden. Mit PayPal revolutionierte er das Zahlen im Internet, mit Tesla schreckte er die Autoindustrie auf und sein Raumfahrtunternehmen SpaceX ist aktuell das weltweit einzige Unternehmen, das ein Raumschiff mit großer Nutzlast wieder auf die Erde zurückbringen kann. <br> <br>Dies ist die persönliche Geschichte hinter einem der größten Unternehmer seit Thomas Edison, Henry Ford oder Howard Hughes. Das Buch erzählt seinen kometenhaften Aufstieg von seiner Flucht aus Südafrika mit 17 Jahren bis heute. Elon Musk gilt als der \"Real Iron Man\" - in Anlehnung an einen der erfolgreichsten Comichelden der Welt. Es ist die gleichsam inspirierende, persönliche und spannende Geschichte eines der erfolgreichsten Querdenker der Welt. In einem Umfang wie noch kein Journalist zuvor hatte Ashlee Vance für diese Biografie exklusiven und direkten Zugang zu Elon Musk, seinem familiären Umfeld und persönlichen Freunden."
Child Category: Akademiker & Spezialisten
Web Player: https://www.audible.com/webplayer?asin=B018GG91F4
Release Date: 2015-11-24
---
# Wie Elon Musk die Welt verändert - Die Biografie

## Wie Elon Musk die Welt verändert - Die Biografie

[https://m.media-amazon.com/images/I/51LJouP2j6L._SL500_.jpg](https://m.media-amazon.com/images/I/51LJouP2j6L._SL500_.jpg)
