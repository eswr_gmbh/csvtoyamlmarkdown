---
Search In Goodreads: https://www.goodreads.com/search?q=Nassim%20Nicholas%20Taleb%20-%20Skin%20in%20the%20Game
Authors: Nassim Nicholas Taleb
Ratings: 293
Isbn 10: 0425284638
Format: Ungekürztes Hörbuch
My Rating: 4
Language: English
Publishers: Random House Audio
Isbn 13: 9780425284636
Book Numbers: ∞
Tags: 
    - Management & Leadership
    - Arbeitsplatz- & Organisationsverhalten
    - Geld & Finanzen
    - Philosophie
    - Politik & Regierungen
    - Politik & Sozialwissenschaften
Added: 13
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/rand/005408/bk_rand_005408_sample.mp3
Asin: B079F24ZBG
Title: "Skin in the Game: Hidden Asymmetries in Daily Life"
Title Short: "Skin in the Game"
Narrators: Joe Ochman
Blurb: "From the New York Times best-selling author of The Black Swan, a bold new work that challenges many of our long-held beliefs about risk and reward, politics and religion...."
Series: Incerto
Cover: https://m.media-amazon.com/images/I/41eC68wVhUL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B079F24ZBG?ipRedirectOverride=true&overrideBaseCountry=true
Length: 8h 20m 
Summary: "<p>Number-one <i>New York Times</i> best seller</p> <p>A bold work from the author of <i>The Black Swan</i> that challenges many of our long-held beliefs about risk and reward, politics and religion, finance and personal responsibility.</p> <p>In his most provocative and practical book yet, one of the foremost thinkers of our time redefines what it means to understand the world, succeed in a profession, contribute to a fair and just society, detect nonsense, and influence others. Citing examples ranging from Hammurabi to Seneca, Antaeus the Giant to Donald Trump, Nassim Nicholas Taleb shows how the willingness to accept one's own risks is an essential attribute of heroes, saints, and flourishing people in all walks of life. </p> <p>As always both accessible and iconoclastic, Taleb challenges long-held beliefs about the values of those who spearhead military interventions, make financial investments, and propagate religious faiths. Among his insights: </p> <p></p> <ul> <li><b>For social justice, focus on symmetry and risk sharing.</b> You cannot make profits and transfer the risks to others, as bankers and large corporations do. You cannot get rich without owning your own risk and paying for your own losses. Forcing skin in the game corrects this asymmetry better than thousands of laws and regulations. </li> <li><b>Ethical rules aren't universal. </b>You're part of a group larger than you, but it's still smaller than humanity in general. </li> <li><b>Minorities, not majorities, run the world. </b>The world is not run by consensus but by stubborn minorities asymmetrically imposing their tastes and ethics on others. </li> <li><b>You can be an intellectual yet still be an idiot.</b> \"Educated philistines\" have been wrong on everything from Stalinism to Iraq to low carb diets. </li> <li><b>Beware of complicated solutions (that someone was paid to find).</b> A simple barbell can build muscle better than expensive new machines. </li> <li><b>True religion is commitment, not just faith</b>. How much you believe in something is manifested only by what you’re willing to risk for it.</li> </ul> <p>The phrase \"skin in the game\" is one we have often heard but have rarely stopped to truly dissect. It is the backbone of risk management, but it's also an astonishingly rich worldview that, as Taleb shows in this book, applies to all aspects of our lives. As Taleb says, \"The symmetry of skin in the game is a simple rule that's necessary for fairness and justice and the ultimate BS-buster,\" and \"Never trust anyone who doesn't have skin in the game. Without it, fools and crooks will benefit, and their mistakes will never come back to haunt them.\" </p>"
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=B079F24ZBG
Release Date: 2018-02-27
---
# Skin in the Game: Hidden Asymmetries in Daily Life

## Skin in the Game

### Incerto

[https://m.media-amazon.com/images/I/41eC68wVhUL._SL500_.jpg](https://m.media-amazon.com/images/I/41eC68wVhUL._SL500_.jpg)
