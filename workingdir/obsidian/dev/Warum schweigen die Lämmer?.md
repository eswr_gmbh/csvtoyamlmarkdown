---
Search In Goodreads: https://www.goodreads.com/search?q=Rainer%20Mausfeld%20-%20Warum%20schweigen%20die%20L%26auml%3Bmmer%3F
Authors: Rainer Mausfeld
Ratings: 454
Isbn 10: 3864892252
Format: Ungekürztes Hörbuch
My Rating: 4
Language: German
Publishers: ABOD Verlag
Isbn 13: 9783864892257
Tags: 
    - Politik & Regierungen
Added: 117
Progress: 1 Std. 47 Min. verbleibend
Categories: 
    - Politik & Sozialwissenschaften 
    - Politik & Regierungen
Sample: https://samples.audible.de/bk/xaod/001035/bk_xaod_001035_sample.mp3
Asin: B07KR3PF28
Title: "Warum schweigen die Lämmer?: Wie Elitendemokratie und Neoliberalismus unsere Gesellschaft und unsere Lebensgrundlagen bedrohen"
Title Short: "Warum schweigen die Lämmer?"
Narrators: Sebastian Pappenberger
Blurb: "In den vergangenen Jahrzehnten wurde die Demokratie in einer beispiellosen Weise ausgehöhlt. Demokratie wurde durch die Illusion von Demokratie..."
Cover: https://m.media-amazon.com/images/I/51LwJsfiM3L._SL500_.jpg
Parent Category: Politik & Sozialwissenschaften
Store Page Url: https://audible.de/pd/B07KR3PF28?ipRedirectOverride=true&overrideBaseCountry=true
Length: 9h 7m 
Summary: "<p>In den vergangenen Jahrzehnten wurde die Demokratie in einer beispiellosen Weise ausgehöhlt. Demokratie wurde durch die Illusion von Demokratie ersetzt, die freie öffentliche Debatte durch ein Meinungs- und Empörungsmanagement, das Leitideal des mündigen Bürgers durch das des politisch apathischen Konsumenten. Wahlen spielen mittlerweile für grundlegende politische Fragen praktisch keine Rolle mehr. Die wichtigen politischen Fragen werden von politisch-ökonomischen Gruppierungen entschieden, die weder demokratisch legitimiert noch demokratisch rechenschaftspflichtig sind. Die destruktiven ökologischen, sozialen und psychischen Folgen dieser Form der Elitenherrschaft bedrohen immer mehr unsere Gesellschaft und unsere Lebensgrundlagen. Rainer Mausfeld deckt die Systematik dieser Indoktrination auf, zeigt dabei auch Ihre historischen Konstanten und macht uns sensibel für die vielfältigen psychologischen Beeinflussungsmethoden.</p>"
Child Category: Politik & Regierungen
Web Player: https://www.audible.com/webplayer?asin=B07KR3PF28
Release Date: 2018-11-29
---
# Warum schweigen die Lämmer?: Wie Elitendemokratie und Neoliberalismus unsere Gesellschaft und unsere Lebensgrundlagen bedrohen

## Warum schweigen die Lämmer?

![https://m.media-amazon.com/images/I/51LwJsfiM3L._SL500_.jpg](https://m.media-amazon.com/images/I/51LwJsfiM3L._SL500_.jpg)
