---
Search In Goodreads: https://www.goodreads.com/search?q=Ben%20Horowitz%20-%20The%20Hard%20Thing%20About%20Hard%20Things
Authors: Ben Horowitz
Ratings: 803
Isbn 10: 0062273213
Format: Ungekürztes Hörbuch
My Rating: 5
Language: English
Publishers: HarperAudio
Isbn 13: 9780062273215
Tags: 
    - Management & Leadership
    - Erfolg im Beruf
    - Geschäftsentwicklung & Unternehmertum
Added: 60
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/harp/003837/bk_harp_003837_sample.mp3
Asin: B00I0ALULK
Title: "The Hard Thing About Hard Things: Building a Business When There Are No Easy Answers"
Title Short: "The Hard Thing About Hard Things"
Narrators: Kevin Kenerly
Blurb: "Ben Horowitz, cofounder of Andreessen Horowitz and one of Silicon Valley’s most respected and experienced entrepreneurs, offers essential advice on building and running a startup...."
Cover: https://m.media-amazon.com/images/I/51j1P236E3L._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B00I0ALULK?ipRedirectOverride=true&overrideBaseCountry=true
Length: 7h 57m 
Summary: "<p>Ben Horowitz, cofounder of Andreessen Horowitz and one of Silicon Valley’s most respected and experienced entrepreneurs, offers essential advice on building and running a startup - practical wisdom for managing the toughest problems business school doesn’t cover, based on his popular ben’s blog. </p> <p>While many people talk about how great it is to start a business, very few are honest about how difficult it is to run one. Ben Horowitz analyzes the problems that confront leaders every day, sharing the insights he’s gained developing, managing, selling, buying, investing in, and supervising technology companies. A lifelong rap fanatic, he amplifies business lessons with lyrics from his favorite songs, telling it straight about everything from firing friends to poaching competitors, cultivating and sustaining a CEO mentality to knowing the right time to cash in. </p> <p>Filled with his trademark humor and straight talk, <i>The Hard Thing About Hard Things</i> is invaluable for veteran entrepreneurs as well as those aspiring to their own new ventures, drawing from Horowitz’s personal and often humbling experiences.</p>"
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=B00I0ALULK
Release Date: 2014-03-04
---
# The Hard Thing About Hard Things: Building a Business When There Are No Easy Answers

## The Hard Thing About Hard Things

[https://m.media-amazon.com/images/I/51j1P236E3L._SL500_.jpg](https://m.media-amazon.com/images/I/51j1P236E3L._SL500_.jpg)
