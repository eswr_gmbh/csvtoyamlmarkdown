---
Search In Goodreads: https://www.goodreads.com/search?q=Peter%20Buffett%20-%20Life%20Is%20What%20You%20Make%20It
Authors: Peter Buffett
Ratings: 40
Isbn 10: 0307464733
Format: Ungekürztes Hörbuch
My Rating: 5
Language: English
Publishers: Random House Audio
Isbn 13: 9780307464736
Tags: 
    - Unterhaltung & Stars
    - Persönlicher Erfolg
Added: 63
Progress: Beendet
Categories: 
    - Biografien & Erinnerungen 
    - Unterhaltung & Stars
Sample: https://samples.audible.de/bk/rand/002264/bk_rand_002264_sample.mp3
Asin: B004V1AHSO
Title: "Life Is What You Make It: Find Your Own Path to Fulfillment"
Title Short: "Life Is What You Make It"
Narrators: Peter Buffett
Blurb: "Which will you choose: the path of least resistance or the path of potentially greatest satisfaction? Peter Buffett offers inspiration and wisdom...."
Cover: https://m.media-amazon.com/images/I/41NNZ+DqqCL._SL500_.jpg
Parent Category: Biografien & Erinnerungen
Store Page Url: https://audible.de/pd/B004V1AHSO?ipRedirectOverride=true&overrideBaseCountry=true
Length: 5h 24m 
Summary: "<p>From composer, musician, and philanthropist Peter Buffett comes a warm, wise, and inspirational book that asks which you will choose: the path of least resistance or the path of potentially greatest satisfaction?</p> <p>You may think that with a last name like his, Buffett has enjoyed a life of endless privilege. But the son of billionaire investor Warren Buffett says that the only real inheritance handed down from his parents was a philosophy: Forge your own path in life. It is a creed that has allowed him to follow his own passions, establish his own identity, and reap his own successes.</p> <p>In <i>Life Is What You Make It</i>, Buffett expounds on the strong set of values given to him by his trusting and broadminded mother, his industrious and talented father, and the many life teachers he has met along the way.</p> <p>Today's society, Buffett posits, has begun to replace a work ethic, relishing what you do, with a wealth ethic, honoring the payoff instead of the process. We confuse privilege with material accumulation, character with external validation. Yet, by focusing more on substance and less on reward, we can open doors of opportunity and strive toward a greater sense of fulfillment. In clear and concise terms, Buffett reveals a great truth: Life is random, neither fair nor unfair.</p> <p>From there, it becomes easy to recognize the equal dignity and value of every human life. Our circumstances may vary, but our essences do not. We see that our journey in life rarely follows a straight line but is often met with false starts, crises, and blunders. How we push through and persevere in these challenging moments is where we begin to create the life of our dreams - from discovering our vocations to living out our bliss to giving back to others.</p> <p>Personal and revealing, instructive and intuitive, <i>Life Is What You Make It</i> is about transcending your circumstances, taking up the reins of your destiny, and living your life to the fullest.</p>"
Child Category: Unterhaltung & Stars
Web Player: https://www.audible.com/webplayer?asin=B004V1AHSO
Release Date: 2010-04-27
---
# Life Is What You Make It: Find Your Own Path to Fulfillment

## Life Is What You Make It

[https://m.media-amazon.com/images/I/41NNZ+DqqCL._SL500_.jpg](https://m.media-amazon.com/images/I/41NNZ+DqqCL._SL500_.jpg)
