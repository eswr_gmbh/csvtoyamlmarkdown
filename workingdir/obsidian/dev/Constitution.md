---
Search In Goodreads: https://www.goodreads.com/search?q=Nick%20Webb%20-%20Constitution
Authors: Nick Webb
Ratings: 6
Isbn 10: 151476993X
Format: Ungekürztes Hörbuch
Language: English
Publishers: Audible Studios
Isbn 13: 9781514769935
Book Numbers: 1, 1
Tags: 
    - Technothriller
    - Erstkontakt
    - Militär
    - Weltraumerkundung
    - Space Opera
Added: 162
Progress: 11 Min. verbleibend
Categories: 
    - Krimis & Thriller 
    - Thriller
Sample: https://samples.audible.de/bk/adbl/025441/bk_adbl_025441_sample.mp3
Asin: B013V9A7DG
Title: "Constitution"
Title Short: "Constitution"
Narrators: Greg Tremblay
Blurb: "The year is 2650. Seventy-five years ago, an alien fleet attacked Earth. Without warning. Without mercy. We were not prepared...."
Series: The Legacy Fleet Series (book 1), Legacy Fleet Trilogy (book 1)
Cover: https://m.media-amazon.com/images/I/61X0JgJQINL._SL500_.jpg
Parent Category: Krimis & Thriller
Store Page Url: https://audible.de/pd/B013V9A7DG?ipRedirectOverride=true&overrideBaseCountry=true
Length: 7h 5m 
Summary: "<p>The year is 2650. Seventy-five years ago, an alien fleet attacked Earth. Without warning. Without mercy. We were not prepared. Hundreds of millions perished. Dozens of cities burned. We nearly lost everything. Then the aliens abruptly left. We rebuilt. We armed ourselves. We swore: never again. But the aliens never came back. Until now. With overwhelming force the aliens have returned, striking deep into our territory, sending Earth into a panic. Our new technology is useless. Our new ships burn like straw. All our careful preparations are wasted. Now only one man, one crew, and the oldest starship in the fleet stand between the Earth and certain destruction: ISS <i>Constitution</i>. </p>"
Child Category: Thriller
Web Player: https://www.audible.com/webplayer?asin=B013V9A7DG
Release Date: 2015-08-18
---
# Constitution

## Constitution

### Legacy Fleet Trilogy (book 1), The Legacy Fleet Series (book 1)

[https://m.media-amazon.com/images/I/61X0JgJQINL._SL500_.jpg](https://m.media-amazon.com/images/I/61X0JgJQINL._SL500_.jpg)
