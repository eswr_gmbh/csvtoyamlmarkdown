---
Search In Goodreads: https://www.goodreads.com/search?q=Chip%20Heath%20-%20The%20Power%20of%20Moments
Authors: Chip Heath, Dan Heath
Ratings: 64
Isbn 10: 1473544580
Format: Ungekürztes Hörbuch
Language: English
Publishers: Random House Audiobooks
Isbn 13: 9781473544581
Tags: 
    - Erfolg im Beruf
    - Geschäftsentwicklung & Unternehmertum
    - Arbeitsplatz- & Organisationsverhalten
    - Seelische & Geistige Gesundheit
    - Politik & Regierungen
    - Selbstwertgefühl
Added: 166
Categories: 
    - Wirtschaft & Karriere 
    - Erfolg im Beruf
Sample: https://samples.audible.de/bk/rhuk/002963/bk_rhuk_002963_sample.mp3
Asin: B075ZWTTWP
Title: "The Power of Moments: Why Certain Experiences Have Extraordinary Impact"
Title Short: "The Power of Moments"
Narrators: Jeremy Bobb
Blurb: "Chip and Dan Heath explore why certain brief experiences can jolt, elevate and change us - and how we can learn to create such extraordinary moments in our life and work...."
Cover: https://m.media-amazon.com/images/I/51-IBy8llFL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B075ZWTTWP?ipRedirectOverride=true&overrideBaseCountry=true
Length: 6 Std. 24 Min.
Summary: "<p><b>The <i>New York Times</i> best-selling authors of <i>Switch</i> and <i>Made to Stick</i> explore why certain brief experiences can jolt, elevate and change us - and how we can learn to <i>create</i> such extraordinary moments in our life and work</b>. </p> <p>What if a teacher could design a lesson that he knew his students would remember 20 years later? What if a doctor or nurse knew how to orchestrate moments that would bring more comfort to patients? What if you had a better sense of how to create memories that matter for your children? Many of the defining moments in our lives are the result of accident or luck - but why leave our most meaningful, memorable moments to chance when we can create them? </p> <p>In <i>The Power of Moments</i>, Chip and Dan Heath explore the stories of people who have created standout moments, from the owners who transformed an utterly mediocre hotel into one of the best-loved properties in Los Angeles by conjuring moments of magic for guests, to the scrappy team that turned around one of the worst elementary schools in the country by embracing an intervention that lasts less than an hour. </p> <p>Filled with remarkable tales and practical insights, <i>The Power of Moments</i> proves we all have the power to transform ordinary experiences into unforgettable ones. </p>"
Child Category: Erfolg im Beruf
Web Player: https://www.audible.com/webplayer?asin=B075ZWTTWP
Release Date: 2017-10-05
---
# The Power of Moments: Why Certain Experiences Have Extraordinary Impact

## The Power of Moments

[https://m.media-amazon.com/images/I/51-IBy8llFL._SL500_.jpg](https://m.media-amazon.com/images/I/51-IBy8llFL._SL500_.jpg)
