---
Search In Goodreads: https://www.goodreads.com/search?q=Titus%20Gebel%20-%20Freie%20Privatst%26auml%3Bdte
Authors: Titus Gebel
Ratings: 30
Isbn 10: 3000626778
Format: Ungekürztes Hörbuch
Language: German
Publishers: AQUILA URBIS
Isbn 13: 9783000626777
Tags: 
    - Politik & Regierungen
    - Politik & Sozialwissenschaften
Added: 194
Progress: Beendet
Categories: 
    - Politik & Sozialwissenschaften 
    - Politik & Regierungen
Sample: https://samples.audible.de/bk/edel/030132/bk_edel_030132_sample.mp3
Asin: B09P51R2BV
Title: "Freie Privatstädte: Mehr Wettbewerb im wichtigsten Markt der Welt"
Title Short: "Freie Privatstädte"
Narrators: Till-Albrecht Jann, Titus Gebel
Blurb: "Stellen Sie sich vor, ein privates Unternehmen bietet Ihnen als \"Staatsdienstleister\" Schutz von Leben, Freiheit und Eigentum in einem abgegrenzten..."
Cover: https://m.media-amazon.com/images/I/41weKNvhaQL._SL500_.jpg
Parent Category: Politik & Sozialwissenschaften
Store Page Url: https://audible.de/pd/B09P51R2BV?ipRedirectOverride=true&overrideBaseCountry=true
Length: 11h 1m 
Summary: "<p>Stellen Sie sich vor, ein privates Unternehmen bietet Ihnen als \"Staatsdienstleister\" Schutz von Leben, Freiheit und Eigentum in einem abgegrenzten Gebiet. Diese Leistung umfasst Sicherheits- und Rettungskräfte, einen Rechts- und Ordnungsrahmen sowie eine unabhängige Streitschlichtung. Sie zahlen einen vertraglich fixierten Betrag für diese Leistungen pro Jahr. Der Staatsdienstleister als Betreiber des Gemeinwesens kann den Vertrag später nicht einseitig ändern. Sie haben einen Rechtsanspruch darauf, dass er eingehalten wird und einen Schadensersatzanspruch bei Schlechterfüllung. Um alles andere kümmern Sie sich selbst, können aber auch machen was Sie wollen, solange Sie die Rechte anderer nicht beeinträchtigen. Und Sie nehmen nur teil, wenn und solange Ihnen das Angebot zusagt. Streitigkeiten zwischen Ihnen und dem Staatsdienstleister werden vor unabhängigen Schiedsgerichten verhandelt, wie im internationalen Handelsrecht üblich. Ignoriert der Betreiber die Schiedssprüche oder missbraucht er seine Macht auf andere Weise, wandern seine Kunden ab und er geht in die Insolvenz. Im ersten Teil des Buches werden grundsätzliche Fragestellungen behandelt, denen sich jede Gesellschaftsordnung stellen muss. Daraus leitet sich das im zweiten Teil beschriebene Konzept Freier Privatstädte ab; weiter werden historische und aktuelle Vorbilder betrachtet. Der dritte Teil bespricht konkrete Umsetzungsfragen bei der Errichtung Freier Privatstädte. Schließlich wird im vierten Teil ein Ausblick auf künftige Entwicklungen gegeben.</p>"
Child Category: Politik & Regierungen
Web Player: https://www.audible.com/webplayer?asin=B09P51R2BV
Release Date: 2021-12-29
---

# Freie Privatstädte Mehr Wettbewerb im wichtigsten Markt der Welt

![https://m.media-amazon.com/images/I/41weKNvhaQL._SL500_.jpg](https://m.media-amazon.com/images/I/41weKNvhaQL._SL500_.jpg)


Wissen ist dezentral.
Jeder weis das beste was er will oder braucht.












#book
