---
Search In Goodreads: https://www.goodreads.com/search?q=Nassim%20Nicholas%20Taleb%20-%20Antifragile
Authors: Nassim Nicholas Taleb
Ratings: 473
Isbn 10: 0679645276
Format: Ungekürztes Hörbuch
Language: English
Publishers: Random House Audio
Isbn 13: 9780679645276
Book Numbers: ∞
Tags: 
    - Erfolg im Beruf
    - Geld & Finanzen
    - Philosophie
    - Sozialwissenschaften
    - Politik & Sozialwissenschaften
Added: 179
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Erfolg im Beruf
Sample: https://samples.audible.de/bk/rand/003327/bk_rand_003327_sample.mp3
Asin: B009WRO8EG
Title: "Antifragile: Things That Gain from Disorder"
Title Short: "Antifragile"
Narrators: Joe Ochman
Blurb: "From the best-selling author of The Black Swan and one of the foremost thinkers of our time, a book on how some things actually benefit from disorder. Taleb’s message is revolutionary: What is not antifragile will surely perish...."
Series: Incerto
Cover: https://m.media-amazon.com/images/I/51yVeIZ2ymL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B009WRO8EG?ipRedirectOverride=true&overrideBaseCountry=true
Length: 16h 14m 
Summary: "<p><b>From the best-selling author of </b><b><i>The Black Swan</i></b><b> and one of the foremost thinkers of our time, Nassim Nicholas Taleb, a book on how some things actually benefit from disorder.</b> </p> <p>In <i>The Black Swan</i> Taleb outlined a problem, and in <i>Antifragile</i> he offers a definitive solution: how to gain from disorder and chaos while being protected from fragilities and adverse events. For what Taleb calls the \"antifragile\" is actually beyond the robust, because it benefits from shocks, uncertainty, and stressors, just as human bones get stronger when subjected to stress and tension. The antifragile needs disorder in order to survive and flourish. </p> <p>Taleb stands uncertainty on its head, making it desirable, even necessary, and proposes that things be built in an antifragile manner. The antifragile is immune to prediction errors. Why is the city-state better than the nation-state, why is debt bad for you, and why is everything that is both modern and complicated bound to fail? The audiobook spans innovation by trial and error, health, biology, medicine, life decisions, politics, foreign policy, urban planning, war, personal finance, and economic systems. And throughout, in addition to the street wisdom of Fat Tony of Brooklyn, the voices and recipes of ancient wisdom, from Roman, Greek, Semitic, and medieval sources, are heard loud and clear. </p> <p>Extremely ambitious and multidisciplinary, <i>Antifragile</i> provides a blueprint for how to behave - and thrive - in a world we don't understand, and which is too uncertain for us to even try to understand and predict. Erudite and witty, Taleb’s message is revolutionary: What is not antifragile will surely perish.</p> <p><b>Please note: The bleeps in the audio are intentional and are as written by the author. No material is censored, and no audio content is missing.</b></p> <p><i>PLEASE NOTE: When you purchase this title, the accompanying PDF will be available in your Audible Library along with the audio. </i></p>"
Child Category: Erfolg im Beruf
Web Player: https://www.audible.com/webplayer?asin=B009WRO8EG
Release Date: 2012-11-27
---
# Antifragile: Things That Gain from Disorder

## Antifragile

### Incerto

[https://m.media-amazon.com/images/I/51yVeIZ2ymL._SL500_.jpg](https://m.media-amazon.com/images/I/51yVeIZ2ymL._SL500_.jpg)
