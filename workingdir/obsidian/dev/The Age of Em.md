---
Search In Goodreads: https://www.goodreads.com/search?q=Robin%20Hanson%20-%20The%20Age%20of%20Em
Authors: Robin Hanson
Ratings: 12
Isbn 10: 0198817827
Format: Ungekürztes Hörbuch
My Rating: 5
Language: English
Publishers: Audible Studios
Isbn 13: 9780198817826
Tags: 
    - Ökonomie
    - Informatik
    - Geschichte & Kultur
    - Technik
Added: 180
Progress: 14 Std. 39 Min. verbleibend
Categories: 
    - Geld & Finanzen 
    - Ökonomie
Sample: https://samples.audible.de/bk/adbl/027803/bk_adbl_027803_sample.mp3
Asin: B01KMDAW3Y
Title: "The Age of Em: Work, Love, and Life When Robots Rule the Earth"
Title Short: "The Age of Em"
Narrators: Michael Butler Murray
Blurb: "Robots may one day rule the world, but what is a robot-ruled Earth like? Many think the first truly smart robots will be brain emulations, or ems...."
Cover: https://m.media-amazon.com/images/I/51LiscTU5yL._SL500_.jpg
Parent Category: Geld & Finanzen
Store Page Url: https://audible.de/pd/B01KMDAW3Y?ipRedirectOverride=true&overrideBaseCountry=true
Length: 15h 51m 
Summary: "<p>Robots may one day rule the world, but what is a robot-ruled Earth like? </p> <p>Many think the first truly smart robots will be brain emulations, or ems. Scan a human brain, then run a model with the same connections on a fast computer, and you have a robot brain, but recognizably human. </p> <p>Train an em to do some job and copy it a million times; an army of workers is at your disposal. When they can be made cheaply, within perhaps a century, ems will displace humans in most jobs. In this new economic era, the world economy may double in size every few weeks. </p> <p>Some say we can't know the future, especially following such a disruptive new technology, but Professor Robin Hanson sets out to prove them wrong. Applying decades of expertise in physics, computer science, and economics, he uses standard theories to paint a detailed picture of a world dominated by ems. </p> <p>While human lives don't change greatly in the em era, em lives are as different from ours as our lives are from those of our farmer and forager ancestors. Ems make us question common assumptions of moral progress, because they reject many of the values we hold dear. </p> <p>Read about em mind speeds, body sizes, job training and career paths, energy use and cooling infrastructure, virtual reality, aging and retirement, death and immortality, security, wealth inequality, religion, teleportation, identity, cities, politics, law, war, status, friendship, and love. </p> <p>This book shows you just how strange your descendants may be, though ems are no stranger than we would appear to our ancestors. To most ems, it seems good to be an em. </p>"
Child Category: Ökonomie
Web Player: https://www.audible.com/webplayer?asin=B01KMDAW3Y
Release Date: 2016-08-19
---
# The Age of Em: Work, Love, and Life When Robots Rule the Earth

## The Age of Em

[https://m.media-amazon.com/images/I/51LiscTU5yL._SL500_.jpg](https://m.media-amazon.com/images/I/51LiscTU5yL._SL500_.jpg)
