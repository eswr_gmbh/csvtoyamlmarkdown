---
Search In Goodreads: https://www.goodreads.com/search?q=Cal%20Newport%20-%20Konzentriert%20arbeiten
Authors: Cal Newport
Ratings: 794
Isbn 10: 3868816577
Format: Ungekürztes Hörbuch
Language: German
Publishers: Redline
Isbn 13: 9783868816570
Tags: 
    - Erfolg im Beruf
Added: 119
Categories: 
    - Wirtschaft & Karriere 
    - Erfolg im Beruf
Sample: https://samples.audible.de/bk/riva/000572/bk_riva_000572_sample.mp3
Asin: 3962670335
Title: "Konzentriert arbeiten: Regeln für eine Welt voller Ablenkungen"
Title Short: "Konzentriert arbeiten"
Narrators: Michael A. Grimm
Blurb: "Ständige Ablenkung ist heute das Hindernis Nummer eins für ein effizienteres Arbeiten. Sei es aufgrund lauter Großraumbüros..."
Cover: https://m.media-amazon.com/images/I/41rn9vY7nJL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/3962670335?ipRedirectOverride=true&overrideBaseCountry=true
Length: 8 Std. 48 Min.
Summary: "<p>Ständige Ablenkung ist heute das Hindernis Nummer eins für ein effizienteres Arbeiten. Sei es aufgrund lauter Großraumbüros, vieler paralleler Kommunikationskanäle, dauerhaftem Online-Sein oder der Schwierigkeit zu entscheiden, was davon nun unsere Aufmerksamkeit am meisten benötigt. Sich ganz auf eine Sache konzentrieren zu können wird damit zu einer raren, aber wertvollen und entscheidenden Fähigkeit im Arbeitsalltag.</p> <p>Cal Newport prägte hierfür den Begriff \"Deep Work\", der einen Zustand völlig konzentrierter und fokussierter Arbeit beschreibt, und begann die Regeln und Denkweisen zu erforschen, die solch fokussiertes Arbeiten fördern. Mit seiner Deep-Work-Methode verrät Newport, wie man sich systematisch darauf trainiert, zu fokussieren, und wie wir unser Arbeitsleben nach den Regeln der Deep-Work-Methode neu organisieren können.</p> <p>Wer in unserer schnelllebigen und sprunghaften Zeit nicht untergehen will, für den ist dieses Konzept unerlässlich. Kurz gesagt: Die Entscheidung für Deep Work ist eine der besten, die man in einer Welt voller Ablenkungen treffen kann.</p>"
Child Category: Erfolg im Beruf
Web Player: https://www.audible.com/webplayer?asin=3962670335
Release Date: 2019-02-26
---
# Konzentriert arbeiten: Regeln für eine Welt voller Ablenkungen

## Konzentriert arbeiten

[https://m.media-amazon.com/images/I/41rn9vY7nJL._SL500_.jpg](https://m.media-amazon.com/images/I/41rn9vY7nJL._SL500_.jpg)
