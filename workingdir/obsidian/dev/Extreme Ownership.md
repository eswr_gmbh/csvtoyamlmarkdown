---
Search In Goodreads: https://www.goodreads.com/search?q=Jocko%20Willink%20-%20Extreme%20Ownership
Authors: Jocko Willink, Leif Babin
Ratings: 1159
Isbn 10: 1250067057
Format: Ungekürztes Hörbuch
My Rating: 5
Language: English
Publishers: Macmillan Audio
Isbn 13: 9781250067050
Book Numbers: ∞
Tags: 
    - Management & Leadership
    - Militär
Added: 23
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/aren/002092/bk_aren_002092_sample.mp3
Asin: B015TVHO6M
Title: "Extreme Ownership: How U.S. Navy SEALs Lead and Win"
Title Short: "Extreme Ownership"
Narrators: Jocko Willink, Leif Babin
Blurb: "In Extreme Ownership, Jocko Willink and Leif Babin share hard-hitting Navy SEAL combat stories that translate into lessons for business and life...."
Series: Extreme Ownership
Cover: https://m.media-amazon.com/images/I/51EDhEokzbL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B015TVHO6M?ipRedirectOverride=true&overrideBaseCountry=true
Length: 9h 33m 
Summary: "<p><b>\"These guys are intense. And they bring that same fire to their narration in the audiobook. Listen to these insanely competent SEAL officers tell you exactly how to make a team successful through their firsthand experiences in business and combat.\" (</b><b><i>The Hustle</i></b><b>)</b></p> <p><b>An updated edition of the blockbuster best-selling leadership audiobook that took America and the world by storm, two U.S. Navy SEAL officers who led the most highly decorated special operations unit of the Iraq War demonstrate how to apply powerful leadership principles from the battlefield to business and life.</b></p> <p>Combat, the most intense and dynamic environment imaginable, teaches the toughest leadership lessons, with absolutely everything at stake. Jocko Willink and Leif Babin learned this reality first-hand on the most violent and dangerous battlefield in Iraq. As leaders of SEAL Team Three’s Task Unit Bruiser, their mission was one many thought impossible: help U.S. forces secure Ramadi, a violent, insurgent-held city deemed “all but lost.” In gripping, firsthand accounts of heroism, tragic loss, and hard-won victories, they learned that leadership - at every level - is the most important factor in whether a team succeeds or fails. Willink and Babin returned home from deployment and instituted SEAL leadership training to pass on their harsh lessons learned in combat to help forge the next generation of SEAL leaders. After leaving the SEAL Teams, they launched a company, Echelon Front, to teach those same leadership principles to leaders in businesses, companies, and organizations across the civilian sector. Since that time, they have trained countless leaders and worked with hundreds of companies in virtually every industry across the U.S. and internationally, teaching them how to develop their own high-performance teams and most effectively lead those teams to dominate their battlefields.</p> <p>Since it’s release in October 2015, <i>Extreme Ownership</i> has revolutionized leadership development and set a new standard for literature on the subject. Required listening for many of the most successful organizations, it has become an integral part of the official leadership training programs for scores of business teams, military units, and first responders. Detailing the mindset and principles that enable SEAL units to accomplish the most difficult combat missions, <i>Extreme Ownership</i> demonstrates how to apply them to any team or organization, in any leadership environment. A compelling narrative with powerful instruction and direct application, <i>Extreme Ownership</i> challenges leaders everywhere to fulfill their ultimate purpose: lead and win. </p>"
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=B015TVHO6M
Release Date: 2015-10-19
---
# Extreme Ownership: How U.S. Navy SEALs Lead and Win

## Extreme Ownership

### Extreme Ownership

[https://m.media-amazon.com/images/I/51EDhEokzbL._SL500_.jpg](https://m.media-amazon.com/images/I/51EDhEokzbL._SL500_.jpg)
