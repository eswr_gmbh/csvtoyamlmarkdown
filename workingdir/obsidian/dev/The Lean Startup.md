---
Search In Goodreads: https://www.goodreads.com/search?q=Eric%20Ries%20-%20The%20Lean%20Startup
Authors: Eric Ries
Ratings: 763
Isbn 10: 030788791X
Format: Ungekürztes Hörbuch
Language: English
Publishers: Random House Audio
Isbn 13: 9780307887917
Book Numbers: ∞
Tags: 
    - Management & Leadership
    - Geschäftsentwicklung & Unternehmertum
Added: 17
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/rand/002759/bk_rand_002759_sample.mp3
Asin: B005LY5K9G
Title: "The Lean Startup: How Today's Entrepreneurs Use Continuous Innovation to Create Radically Successful Businesses"
Title Short: "The Lean Startup"
Narrators: Eric Ries
Blurb: "Most startups fail. But many of those failures are preventable. The Lean Startup is a new approach being adopted across the globe...."
Series: The Lean Startup
Cover: https://m.media-amazon.com/images/I/51PAIR77wJL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B005LY5K9G?ipRedirectOverride=true&overrideBaseCountry=true
Length: 8h 38m 
Summary: "<p>Most startups fail. But many of those failures are preventable. <i>The Lean Startup</i> is a new approach being adopted across the globe, changing the way companies are built and new products are launched.</p> <p>Eric Ries defines a startup as an organization dedicated to creating something new under conditions of extreme uncertainty. This is just as true for one person in a garage or a group of seasoned professionals in a Fortune 500 boardroom. What they have in common is a mission to penetrate that fog of uncertainty to discover a successful path to a sustainable business. </p> <p><i>The Lean Startup</i> approach fosters companies that are both more capital efficient and that leverage human creativity more effectively. Inspired by lessons from lean manufacturing, it relies on “validated learning”, rapid scientific experimentation, as well as a number of counter-intuitive practices that shorten product-development cycles, measure actual progress without resorting to vanity metrics, and learn what customers really want. It enables a company to shift directions with agility, altering plans inch by inch, minute by minute. </p> <p>Rather than wasting time creating elaborate business plans, <i>The Lean Startup</i> offers entrepreneurs - in companies of all sizes - a way to test their vision continuously, to adapt and adjust before it’s too late. Ries provides a scientific approach to creating and managing successful startups in a age when companies need to innovate more than ever.</p>"
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=B005LY5K9G
Release Date: 2011-09-13
---
# The Lean Startup: How Today's Entrepreneurs Use Continuous Innovation to Create Radically Successful Businesses

## The Lean Startup

### The Lean Startup

[https://m.media-amazon.com/images/I/51PAIR77wJL._SL500_.jpg](https://m.media-amazon.com/images/I/51PAIR77wJL._SL500_.jpg)
