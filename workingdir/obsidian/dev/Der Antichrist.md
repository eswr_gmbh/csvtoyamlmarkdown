---
Search In Goodreads: https://www.goodreads.com/search?q=Friedrich%20Nietzsche%20-%20Der%20Antichrist
Authors: Friedrich Nietzsche
Ratings: 94
Isbn 10: 3937872736
Format: Gekürztes Hörbuch
Language: German
Publishers: Hörwerk Leipzig
Isbn 13: 9783937872735
Tags: 
    - Essays
    - Philosophie
Added: 161
Categories: 
    - Literatur & Belletristik 
    - Essays
Sample: https://samples.audible.de/bk/hole/000022/bk_hole_000022_sample.mp3
Asin: B004V4KDBM
Title: "Der Antichrist"
Title Short: "Der Antichrist"
Narrators: Axel Thielmann
Blurb: "Nietzsches Auseinandersetzung mit der Religion und seine polemisch vorgetragene Kritik am Christentum mündet schließlich in eine Kritik an seiner Zeit..."
Cover: https://m.media-amazon.com/images/I/41lQLBr87OL._SL500_.jpg
Parent Category: Literatur & Belletristik
Store Page Url: https://audible.de/pd/B004V4KDBM?ipRedirectOverride=true&overrideBaseCountry=true
Length: 3 Std. 6 Min.
Summary: "\"Diese ewige Anklage des Christentums will ich an alle Wände schreiben, wo es nur Wände gibt - ich habe Buchstaben, um auch Blinde sehend zu machen... Ich heiße das Christentum den einen großen Fluch, die eine große innerlichste Verdorbenheit, den einen großen Instinkt der Rache, dem kein Mittel giftig, heimlich, unterirdisch, klein genug ist - ich heiße es den einen unsterblichen Schandfleck der Menschheit...\" Friedrich Nietzsche 1895 <p> Nietzsches Auseinandersetzung mit der Religion und seine polemisch vorgetragene Kritik am Christentum mündet schließlich in eine Kritik an seiner Zeit. Der Antichrist ist eines von Nietzsches Spätwerken, welches (in wenigen Wochen) 1888 niedergeschrieben wurde. Dieses an sich kleine Werk beinhaltet unglaublich viel. </p> Ist der Hörer kritisch genug und Liebhaber wundervoller Sprache, so wird er durch dieses Hörbuch, das Axel Thielmann exzellent eingesprochen hat, auf beeindruckende Weise bereichert!"
Child Category: Essays
Web Player: https://www.audible.com/webplayer?asin=B004V4KDBM
Release Date: 2009-10-12
---
# Der Antichrist

## Der Antichrist

[https://m.media-amazon.com/images/I/41lQLBr87OL._SL500_.jpg](https://m.media-amazon.com/images/I/41lQLBr87OL._SL500_.jpg)
