---
Search In Goodreads: https://www.goodreads.com/search?q=Dennis%20E.%20Taylor%20-%20Die%20Singularit%26auml%3Btsfalle
Authors: Dennis E. Taylor
Ratings: 3135
Isbn 10: 3641223911
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Random House Audio, Deutschland
Isbn 13: 9783641223915
Tags: 
    - Weltraumerkundung
Added: 85
Progress: Beendet
Categories: 
    - Science Fiction & Fantasy 
    - Science Fiction
Sample: https://samples.audible.de/bk/rhde/003933/bk_rhde_003933_sample.mp3
Asin: 3837149552
Title: "Die Singularitätsfalle"
Title Short: "Die Singularitätsfalle"
Narrators: Simon Jäger
Blurb: "Schon sein ganzes Leben lang wird Bergmann Ivan Pritchard vom Pech verfolgt. Um seiner Familie endlich ein komfortableres Leben zu..."
Cover: https://m.media-amazon.com/images/I/614UKbANnbL._SL500_.jpg
Parent Category: Science Fiction & Fantasy
Store Page Url: https://audible.de/pd/3837149552?ipRedirectOverride=true&overrideBaseCountry=true
Length: 11h 13m 
Summary: "<p>Schon sein ganzes Leben lang wird Bergmann Ivan Pritchard vom Pech verfolgt. Um seiner Familie endlich ein komfortableres Leben zu ermöglichen, möchte er sein Glück nun mit Asteroiden versuchen und heuert auf der \"Mad Astra\" an. Doch der Neue auf dem Schiff zu sein, ist gar nicht so einfach: Die Crew schikaniert ihn, und gewöhnt man sich eigentlich jemals an diese Schwerelosigkeit? Als sich aus den Tiefen des Alls eine dunkle Bedrohung nähert, die die Erde auslöschen könnte, beschließt Ivan, sein Schicksal endlich selbst in die Hand zu nehmen ...<br> </p>"
Child Category: Science Fiction
Web Player: https://www.audible.com/webplayer?asin=3837149552
Release Date: 2020-02-24
---
# Die Singularitätsfalle

## Die Singularitätsfalle

[https://m.media-amazon.com/images/I/614UKbANnbL._SL500_.jpg](https://m.media-amazon.com/images/I/614UKbANnbL._SL500_.jpg)
