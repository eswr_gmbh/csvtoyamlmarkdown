---
Search In Goodreads: https://www.goodreads.com/search?q=Robert%20B.%20Cialdini%20-%20Die%20Psychologie%20des%20%26Uuml%3Bberzeugens
Authors: Robert B. Cialdini
Ratings: 2627
Isbn 10: 3456851502
Format: Ungekürztes Hörbuch
Language: German
Publishers: Hogrefe Verlag GmbH & Co. KG
Isbn 13: 9783456851501
Tags: 
    - Seelische & Geistige Gesundheit
    - Kommunikation & soziale Kompetenz
    - Beziehungen
Added: 125
Progress: 3 Std. 7 Min. verbleibend
Categories: 
    - Gesundheit & Wellness 
    - Seelische & Geistige Gesundheit
Sample: https://samples.audible.de/bk/edel/005039/bk_edel_005039_sample.mp3
Asin: B0727Q9TPR
Title: "Die Psychologie des Überzeugens"
Title Short: "Die Psychologie des Überzeugens"
Narrators: Helmut Winkelmann, Karin Grüger
Blurb: "Robert B. Cialdini, Professor für Psychologie an der Arizona State University, ist der international führende Experte zum Thema Überzeugen..."
Cover: https://m.media-amazon.com/images/I/51437IuYWsL._SL500_.jpg
Parent Category: Gesundheit & Wellness
Store Page Url: https://audible.de/pd/B0727Q9TPR?ipRedirectOverride=true&overrideBaseCountry=true
Length: 7h 17m 
Summary: "Robert B. Cialdini, Professor für Psychologie an der Arizona State University, ist der international führende Experte zum Thema Überzeugen. Er ist ehemaliger Vorsitzender der amerikanischen Fachgesellschaft für Persönlichkeits- und Sozialpsychologie. Sein Interesse am komplexen Zusammenspiel der Faktoren, die Menschen dazu bringen, sich auf eine bestimmte Art zu verhalten, schreibt er selbst der Tatsache zu, dass er in einer italienischstämmigen Familie in einem überwiegend polnischen Viertel einer deutsch geprägten Stadt (Milwaukee) aufgewachsen ist, die ihrerseits in einem ländlichen Bundesstaat liegt. Seit Erscheinen des Buches wird es für seine praktischen Anregungen sowie seine wissenschaftliche Genauigkeit hoch gelobt und findet großes Interesse unter Wirtschaftsfachleuten, Fundraisingspezialisten und Menschen mit Interesse an Psychologie."
Child Category: Seelische & Geistige Gesundheit
Web Player: https://www.audible.com/webplayer?asin=B0727Q9TPR
Release Date: 2017-04-26
---
# Die Psychologie des Überzeugens

## Die Psychologie des Überzeugens

[https://m.media-amazon.com/images/I/51437IuYWsL._SL500_.jpg](https://m.media-amazon.com/images/I/51437IuYWsL._SL500_.jpg)
