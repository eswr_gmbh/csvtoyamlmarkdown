---
Search In Goodreads: https://www.goodreads.com/search?q=William%20Strauss%20-%20The%20Fourth%20Turning
Authors: William Strauss, Neil Howe
Ratings: 79
Isbn 10: 0307485056
Format: Gekürztes Hörbuch
Language: English
Publishers: Random House Audio
Isbn 13: 9780307485052
Tags: 
    - Nord-
    - Mittel- & Südamerika
    - Politik & Regierungen
    - Zukunftsstudien
    - Politik & Sozialwissenschaften
Added: 135
Progress: 1 Std. 39 Min. verbleibend
Categories: 
    - Geschichte 
    - Nord-
    - Mittel- & Südamerika
Sample: https://samples.audible.de/bk/bant/000419/bk_bant_000419_sample.mp3
Asin: B004V8089U
Title: "The Fourth Turning: An American Prophecy"
Title Short: "The Fourth Turning"
Narrators: William Strauss, Neil Howe
Blurb: "William Strauss and Neil Howe will change the way you see the world - and your place in it. With blazing originality, The Fourth Turning illuminates the past, explains the present, and reimagines the future. Most remarkably, it offers an utterly persuasive prophecy...."
Cover: https://m.media-amazon.com/images/I/51gAnJK3tdL._SL500_.jpg
Parent Category: Geschichte
Store Page Url: https://audible.de/pd/B004V8089U?ipRedirectOverride=true&overrideBaseCountry=true
Length: 6h 1m 
Summary: "<p><b>National best seller</b></p> <p><b>“A startling vision of what the cycles of history predict for the future.” (</b><b><i>USA Weekend</i></b><b>)</b><br> <br> William Strauss and Neil Howe will change the way you see the world - and your place in it. With blazing originality, <i>The Fourth Turning</i> illuminates the past, explains the present, and reimagines the future. Most remarkably, it offers an utterly persuasive prophecy about how America’s past will predict its future. </p> <p>Strauss and Howe base this vision on a provocative theory of American history. The authors look back 500 years and uncover a distinct pattern: Modern history moves in cycles, each one lasting about the length of a long human life, each composed of four eras - or \"turnings\" - that last about 20 years and that always arrive in the same order. In <i>The Fourth Turning</i>, the authors illustrate these cycles using a brilliant analysis of the post-World War II period.</p> <p>First comes a High, a period of confident expansion as a new order takes root after the old has been swept away. Next comes an Awakening, a time of spiritual exploration and rebellion against the now-established order. Then comes an Unraveling, an increasingly troubled era in which individualism triumphs over crumbling institutions. Last comes a Crisis - the Fourth Turning - when society passes through a great and perilous gate in history. Together, the four turnings comprise history's seasonal rhythm of growth, maturation, entropy, and rebirth.</p> <p><i>The Fourth Turning </i>offers bold predictions about how all of us can prepare, individually and collectively, for America’s next rendezvous with destiny.</p>"
Child Category: Nord-, Mittel- & Südamerika
Web Player: https://www.audible.com/webplayer?asin=B004V8089U
Release Date: 2001-12-07
---
# The Fourth Turning: An American Prophecy

## The Fourth Turning

[https://m.media-amazon.com/images/I/51gAnJK3tdL._SL500_.jpg](https://m.media-amazon.com/images/I/51gAnJK3tdL._SL500_.jpg)
