---
Search In Goodreads: https://www.goodreads.com/search?q=The%20Science%20of%20Creativity%20%26%20How%20to%20Enhance%20Creative%20Innovation
From Plus Catalog: true
Tags: 
    - 
Added: 197
Progress: Beendet
Categories: 
    - 
Asin: B0BQJ3Z5JJ
Title: "The Science of Creativity & How to Enhance Creative Innovation"
Title Short: "The Science of Creativity & How to Enhance Creative Innovation"
Blurb: "In this episode, I explain how the brain engages in creative thinking and, based on that mechanistic understanding, the tools to improve one’s ..."
Cover: https://m.media-amazon.com/images/I/41S8y6XeemL._SL500_.jpg
Store Page Url: https://audible.de/pd/B0BQJ3Z5JJ?ipRedirectOverride=true&overrideBaseCountry=true
Summary: ""
Store Page Missing: true
Web Player: https://www.audible.com/webplayer?asin=B0BQJ3Z5JJ
---
