---
Search In Goodreads: https://www.goodreads.com/search?q=Robert%20Greene%20-%20Die%20Gesetze%20der%20menschlichen%20Natur%20-%20The%20Laws%20of%20Human%20Nature
Authors: Robert Greene
Ratings: 272
Isbn 10: 3959722303
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: FinanzBuch Verlag
Isbn 13: 9783959722308
Tags: 
    - Seelische & Geistige Gesundheit
    - Anthropologie
Added: 114
Progress: Beendet
Categories: 
    - Gesundheit & Wellness 
    - Seelische & Geistige Gesundheit
Sample: https://samples.audible.de/bk/riva/000713/bk_riva_000713_sample.mp3
Asin: 3960927991
Title: "Die Gesetze der menschlichen Natur - The Laws of Human Nature: Mit einzigartigen Strategien wie Sie menschliches Denken und Handeln entschlüsseln"
Title Short: "Die Gesetze der menschlichen Natur - The Laws of Human Nature"
Narrators: Markus Böker
Blurb: "Robert Greene versteht es auf meisterhafte Weise, Weisheit und Philosophie der alten Denker für Millionen von Lesern auf der Suche nach..."
Cover: https://m.media-amazon.com/images/I/41ig8RdYj9L._SL500_.jpg
Parent Category: Gesundheit & Wellness
Store Page Url: https://audible.de/pd/3960927991?ipRedirectOverride=true&overrideBaseCountry=true
Length: 34h 27m 
Summary: "<p>Robert Greene versteht es auf meisterhafte Weise, Weisheit und Philosophie der alten Denker für Millionen von Lesern auf der Suche nach Wissen, Macht und Selbstvervollkommnung zugänglich zu machen. In seinem neuen Buch ist er dem wichtigsten Thema überhaupt auf der Spur: Der Entschlüsselung menschlicher Antriebe und Motivationen, auch derer, die uns selbst nicht bewusst sind.</p> <p>Der Mensch ist ein Gesellschaftstier. Sein Leben hängt von der Beziehung zu Seinesgleichen ab. Zu wissen, warum wir tun, was wir tun, gibt uns ein weit wirksameres Werkzeug an die Hand als all unsere Talente es könnten. Ausgehend von den Ideen und Beispielen von Perikles, Queen Elizabeth I, Martin Luther King Jr und vielen anderen zeigt Greene, wie wir einerseits von unseren eigenen Emotionen unabhängig werden und Selbstbeherrschung lernen und andererseits Empathie anderen gegenüber entwickeln können, um hinter ihre Masken zu blicken.</p> <p>Die Gesetze der menschlichen Natur bietet dem Leser nicht zuletzt einzigartige Strategien, um im professionellen und privaten Bereich eigene Ziele zu erreichen und zu verteidigen.</p>"
Child Category: Seelische & Geistige Gesundheit
Web Player: https://www.audible.com/webplayer?asin=3960927991
Release Date: 2020-11-23
---
# Die Gesetze der menschlichen Natur - The Laws of Human Nature: Mit einzigartigen Strategien wie Sie menschliches Denken und Handeln entschlüsseln

## Die Gesetze der menschlichen Natur - The Laws of Human Nature

[https://m.media-amazon.com/images/I/41ig8RdYj9L._SL500_.jpg](https://m.media-amazon.com/images/I/41ig8RdYj9L._SL500_.jpg)
