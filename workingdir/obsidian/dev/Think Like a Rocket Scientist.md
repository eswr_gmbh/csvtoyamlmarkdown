---
Search In Goodreads: https://www.goodreads.com/search?q=Ozan%20Varol%20-%20Think%20Like%20a%20Rocket%20Scientist
Authors: Ozan Varol
Ratings: 32
Isbn 10: 1541762614
Format: Ungekürztes Hörbuch
Language: English
Publishers: PublicAffairs
Isbn 13: 9781541762619
Tags: 
    - Management & Leadership
    - Erfolg im Beruf
    - Wissenschaft
Added: 100
Progress: 5 Std. 29 Min. verbleibend
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/hach/005552/bk_hach_005552_sample.mp3
Asin: 1549144936
Title: "Think Like a Rocket Scientist: Simple Strategies You Can Use to Make Giant Leaps in Work and Life"
Title Short: "Think Like a Rocket Scientist"
Narrators: Ozan Varol
Blurb: "A former rocket scientist reveals the habits, ideas, and strategies that will empower you to turn the seemingly impossible into the possible...."
Cover: https://m.media-amazon.com/images/I/41PiY8UYHwL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/1549144936?ipRedirectOverride=true&overrideBaseCountry=true
Length: 10h 44m 
Summary: "<p><b>One of Inc.com’s “Six Books You Need to Read in 2020 (According to Bill Gates, Satya Nadella, and Adam Grant)”</b></p> <p><b>Adam Grant’s number one pick of his top 20 books of 2020</b></p> <p><b>One of Six Groundbreaking Books of Spring 2020 (according to Malcolm Gladwell, Susan Cain, Dan Pink, and Adam Grant).</b></p> <p>A former rocket scientist reveals the habits, ideas, and strategies that will empower you to turn the seemingly impossible into the possible.</p> <p>Rocket science is often celebrated as the ultimate triumph of technology. But it's not. Rather, it's the apex of a certain thought process - a way to imagine the unimaginable and solve the unsolvable. It's the same thought process that enabled Neil Armstrong to take his giant leap for mankind, that allows spacecraft to travel millions of miles through outer space and land on a precise spot, and that brings us closer to colonizing other planets. </p> <p>Fortunately, you don't have to be a rocket scientist to think like one. </p> <p>In this accessible and practical book, Ozan Varol reveals nine simple strategies from rocket science that you can use to make your own giant leaps in work and life - whether it's landing your dream job, accelerating your business, learning a new skill, or creating the next breakthrough product. Today, thinking like a rocket scientist is a necessity. We all encounter complex and unfamiliar problems in our lives. Those who can tackle these problems - without clear guidelines and with the clock ticking - enjoy an extraordinary advantage. </p> <p><i>Think Like a Rocket Scientist</i> will inspire you to take your own moonshot and enable you to achieve liftoff.</p>"
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=1549144936
Release Date: 2020-04-14
---
# Think Like a Rocket Scientist: Simple Strategies You Can Use to Make Giant Leaps in Work and Life

## Think Like a Rocket Scientist

[https://m.media-amazon.com/images/I/41PiY8UYHwL._SL500_.jpg](https://m.media-amazon.com/images/I/41PiY8UYHwL._SL500_.jpg)
