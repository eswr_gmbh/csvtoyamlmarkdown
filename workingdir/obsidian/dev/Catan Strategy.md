---
Search In Goodreads: https://www.goodreads.com/search?q=Mark%20Oxer%20-%20Catan%20Strategy
Authors: Mark Oxer
Isbn 10: 1709494174
Format: Ungekürztes Hörbuch
Language: English
Publishers: Mark Oxer
Isbn 13: 9781709494178
Tags: 
    - 
Added: 187
Progress: 48 Min. verbleibend
Categories: 
    - 
Sample: https://samples.audible.de/bk/acx0/179793/bk_acx0_179793_sample.mp3
Asin: B0843W9336
Title: "Catan Strategy: A Complete Guide to Winning the Popular Board Game"
Title Short: "Catan Strategy"
Narrators: Mike Goodrick
Blurb: "This is an advanced guide to the game of Catan and goes beyond anything authored previously. It is intended to become a reference piece rather than something that you listen to once and forget. I hope that it inspires you to study the game, your opponents, and your own level of play...."
Cover: https://m.media-amazon.com/images/I/411XNZkaS8L._SL500_.jpg
Store Page Url: https://audible.de/pd/B0843W9336?ipRedirectOverride=true&overrideBaseCountry=true
Length: 2h 40m 
Summary: "<p>After winning the Canadian Championship in 2005 (defeating the 2004 champion in the round-robin play), I read every Catan strategy guide I could find, in my attempt to win the world championship. At that time, there was not a lot of information available, with the exception of a few online articles. There is more information available these days, but I have yet to find anything comprehensive written by another tournament player.</p> <p>This is an advanced guide to the game of Catan and goes beyond anything authored previously. It is intended to become a reference piece rather than something that you listen to once and forget. I hope that it inspires you to study the game, your opponents, and your own level of play. This is not just a book of rules, scenarios, and statistics; it is an audiobook designed to change the way you think about Catan. </p> <p>I want you to learn the intricacies of the game and show you how to solve problems using methods implemented by the top players in the world. I am not suggesting that my way is the only way to win; there are more ways than one way to win at Catan, however, in order to win, you will need to learn how to think through a situation and develop your own winning strategy. </p> <p>I truly believe that whatever level of play you are currently at, the additional skills you will learn by listening to this audiobook will significantly improve your Catan gaming ability.</p>"
Web Player: https://www.audible.com/webplayer?asin=B0843W9336
Release Date: 2020-01-28
---
# Catan Strategy: A Complete Guide to Winning the Popular Board Game

## Catan Strategy

![https://m.media-amazon.com/images/I/411XNZkaS8L._SL500_.jpg](https://m.media-amazon.com/images/I/411XNZkaS8L._SL500_.jpg)
