---
Search In Goodreads: https://www.goodreads.com/search?q=Hermann%20H.%20Wala%20-%20Meine%20Marke
Authors: Hermann H. Wala
Ratings: 206
Isbn 10: 386881745X
Format: Ungekürztes Hörbuch
My Rating: 3
Language: German
Publishers: Redline
Isbn 13: 9783868817454
Tags: 
    - Marketing & Vertrieb
Added: 72
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Marketing & Vertrieb
Sample: https://samples.audible.de/bk/riva/000523/bk_riva_000523_sample.mp3
Asin: 3962670807
Title: "Meine Marke: Was Unternehmen authentisch, unverwechselbar und langfristig erfolgreich macht"
Title Short: "Meine Marke"
Narrators: Konstantin Moreth
Blurb: "Was macht eine Marke stark, einzigartig und damit wertvoll? Erfolgreiche Marken erzeugen ein \"Wir-Gefühl, weiß der Marketing-Stratege Hermann..."
Cover: https://m.media-amazon.com/images/I/51BC-kftH1L._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/3962670807?ipRedirectOverride=true&overrideBaseCountry=true
Length: 8h 13m 
Summary: "<p> Starke Marken setzen sich durch.</p> <p>Was macht eine Marke stark, einzigartig und damit wertvoll? Erfolgreiche Marken erzeugen ein \"Wir-Gefühl, weiß der Marketing-Stratege Hermann H. Wala. Für den Aufbau einer unverwechselbaren Marke bietet sein \"Wir-Marken-Konzept\" klare und gut umsetzbare Strategien.</p> <p>\"Nicht mehr die Großen fressen die Kleinen oder die Schnellen die Langsamen, sondern die \"Wert-vollen\" diejenigen, die es nicht schafften, ihre Wer-te glaubwürdig zu transportieren\", sagt Hermann H. Wala. Der bekannte Marketing-Stratege ist davon überzeugt, dass sich Unternehmen für ihre Kunden öffnen und ihnen ein auf gemeinsamen Werten beruhendes Wir-Gefühl vermitteln müssen, um erfolgreich zu sein. Dies gelingt nur über starke Marken, die in unserer unübersichtlich gewordenen Welt ein Identifikationsangebot darstellen.</p> <p>In seinem Hörbuch \"Meine Marke - Was Unterneh-men authentisch, unverwechselbar und langfristig erfolgreich macht\" zeigt Wala am Beispiel erfolgreicher Marketingstrategien und unternehmerischer Fehleinschätzungen auf, wie es gelingt, eine erfolgreiche \"Wir-Marke\" aufzubauen. Auf Basis seiner langjährigen Erfahrung hat er dazu sieben Tools entwickelt, mit deren Hilfe sich Unternehmen unverwechselbar machen. Zu jedem Tool führte Wala aufschlussreiche Interviews mit prominenten Markenbotschaftern, die einen interessanten Einblick in die jeweiligen Erfolgsstrategien bieten. So sprach er mit Herbert Hainer (adidas) über das Thema Positionierung, mit Franz Beckenbauer <br> über Storytelling, mit Rolf Kreiner (McDonald's) über Emotionen, mit Prof. Hermann Simon (Simon-Kucher & Partners) über Selbstvertrauen, mit Josef Lutz (BayWa AG) über Werte, mit Dr. Rudolf Gröger (O2) über Vertrauen und mit Thomas Ebe-ling (ProSiebenSat.1 Media) über Dynamik.</p>"
Child Category: Marketing & Vertrieb
Web Player: https://www.audible.com/webplayer?asin=3962670807
Release Date: 2018-11-20
---
# Meine Marke: Was Unternehmen authentisch, unverwechselbar und langfristig erfolgreich macht

## Meine Marke

[https://m.media-amazon.com/images/I/51BC-kftH1L._SL500_.jpg](https://m.media-amazon.com/images/I/51BC-kftH1L._SL500_.jpg)
