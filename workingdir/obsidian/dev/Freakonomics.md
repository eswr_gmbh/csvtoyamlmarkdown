---
Search In Goodreads: https://www.goodreads.com/search?q=Steven%20D.%20Levitt%20-%20Freakonomics
Authors: Steven D. Levitt, Stephen J. Dubner
Ratings: 106
Isbn 10: 0062132342
Format: Ungekürztes Hörbuch
Language: English
Publishers: HarperAudio
Isbn 13: 9780062132345
Tags: 
    - Ökonomie
    - Popularkultur
    - Mathematik
Added: 18
Progress: 5 Std. 54 Min. verbleibend
Categories: 
    - Geld & Finanzen 
    - Ökonomie
Sample: https://samples.audible.de/bk/harp/001479/bk_harp_001479_sample.mp3
Asin: B079TM16F5
Title: "Freakonomics: Revised Edition"
Title Short: "Freakonomics"
Narrators: Stephen J. Dubner
Blurb: "Levitt and co-author Stephen J. Dubner show that economics is, at root, the study of incentives: how people get what they want, or need, especially when other people want or need the same thing...."
Cover: https://m.media-amazon.com/images/I/51CUvlxvifL._SL500_.jpg
Parent Category: Geld & Finanzen
Store Page Url: https://audible.de/pd/B079TM16F5?ipRedirectOverride=true&overrideBaseCountry=true
Length: 7h 51m 
Summary: "<p><i>Which is more dangerous: a gun or a swimming pool? What do schoolteachers and sumo wrestlers have in common? Why do drug dealers still live with their moms? How much do parents really matter? How did the legalization of abortion affect the rate of violent crime?</i></p> <p>These may not sound like typical questions for an economist to ask. But Steven D. Levitt is not a typical economist. He is a much-heralded scholar who studies the riddles of everyday life, from cheating and crime to sports and child-rearing, and whose conclusions turn the conventional wisdom on its head. Thus the new field of study contained in this audiobook: <i>Freakonomics</i>.</p> <p>Levitt and co-author Stephen J. Dubner show that economics is, at root, the study of incentives: how people get what they want, or need, especially when other people want or need the same thing. In <i>Freakonomics</i>, they explore the hidden side of...well, everything. The inner working of a crack gang...the truth about real-estate agents...the secrets of the Klu Klux Klan.</p> <p>What unites all these stories is a belief that the modern world is even more intriguing than we think. All it takes is a new way of looking, and <i>Freakonomics</i> will redefine the way we view the modern world.</p>"
Child Category: Ökonomie
Web Player: https://www.audible.com/webplayer?asin=B079TM16F5
Release Date: 2007-06-14
---
# Freakonomics: Revised Edition

## Freakonomics

[https://m.media-amazon.com/images/I/51CUvlxvifL._SL500_.jpg](https://m.media-amazon.com/images/I/51CUvlxvifL._SL500_.jpg)
