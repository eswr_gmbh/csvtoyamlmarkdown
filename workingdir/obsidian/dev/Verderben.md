---
Search In Goodreads: https://www.goodreads.com/search?q=Peter%20F.%20Hamilton%20-%20Verderben
Authors: Peter F. Hamilton
Ratings: 1098
Isbn 10: 349299606X
Format: Ungekürztes Hörbuch
My Rating: 3
Language: German
Publishers: HörbucHHamburg HHV GmbH
Isbn 13: 9783492996068
Book Numbers: 2
Tags: 
    - Space Opera
Added: 99
Progress: Beendet
Categories: 
    - Science Fiction & Fantasy 
    - Science Fiction
Sample: https://samples.audible.de/bk/hamb/003108/bk_hamb_003108_sample.mp3
Asin: 3844918736
Title: "Verderben: Die Salvation-Saga 2"
Title Short: "Verderben"
Narrators: Oliver Siebeck
Blurb: "Der zweite Teil von Peter F. Hamiltons epischer Trilogie als ungekürzte Lesung Im 23. Jahrhundert haben sich die Menschen eine Art Utopia..."
Series: Die Salvation-Saga (book 2)
Cover: https://m.media-amazon.com/images/I/51Nr4uSeVhS._SL500_.jpg
Parent Category: Science Fiction & Fantasy
Store Page Url: https://audible.de/pd/3844918736?ipRedirectOverride=true&overrideBaseCountry=true
Length: 19h 25m 
Summary: "<p>Der zweite Teil von Peter F. Hamiltons epischer Trilogie als ungekürzte Lesung Im 23. Jahrhundert haben sich die Menschen eine Art Utopia erschaffen. Bis eine vermeintlich friedliche Alienrasse sich als die größte Bedrohung für unsere Zivilisation herausstellt: Getrieben von religiösem Extremismus, setzen die Olyix alles daran, die anderen Völker des Universums zu unterwerfen. Aber die Menschheit könnte zum ebenbürtigen Gegner werden, der nicht die Absicht hat, aufzugeben und sich bis zum Ende der Zeit zu verstecken. Im Gegenteil wächst die Überzeugung der Menschen, den Feind ein für alle Mal auszulöschen. Allerdings ist nichts im Universum so festgeschrieben wie die Tatsache, dass auch der beste Plan nicht jedes Ereignis vorhersehen kann...</p> <p>In deiner Audible-Bibliothek findest du für dieses Hörerlebnis eine PDF-Datei mit zusätzlichem Material.<br>  </p>"
Child Category: Science Fiction
Web Player: https://www.audible.com/webplayer?asin=3844918736
Release Date: 2020-06-02
---
# Verderben: Die Salvation-Saga 2

## Verderben

### Die Salvation-Saga (book 2)

[https://m.media-amazon.com/images/I/51Nr4uSeVhS._SL500_.jpg](https://m.media-amazon.com/images/I/51Nr4uSeVhS._SL500_.jpg)
