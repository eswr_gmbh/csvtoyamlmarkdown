---
Search In Goodreads: https://www.goodreads.com/search?q=Gerald%20H%C3%B6rhan%20-%20Null%20Bock%20Komplott
Authors: Gerald Hörhan
Ratings: 451
Isbn 10: 3990010697
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: ABOD Verlag
Isbn 13: 9783990010693
Tags: 
    - Erfolg im Beruf
Added: 24
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Erfolg im Beruf
Sample: https://samples.audible.de/bk/xaod/000129/bk_xaod_000129_sample.mp3
Asin: B00FWMTIDG
Title: "Null Bock Komplott: Warum immer die Falschen Karriere machen und wie ihr es trotzdem schafft"
Title Short: "Null Bock Komplott"
Narrators: Matthias Lühn
Blurb: "Gesetze, Benimm-Terror, Überwachung und davon immer mehr: Unser politisches und ökonomisches System nimmt Menschen..."
Cover: https://m.media-amazon.com/images/I/51U+lzszE1L._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B00FWMTIDG?ipRedirectOverride=true&overrideBaseCountry=true
Length: 3h 11m 
Summary: "Gesetze, Benimm-Terror, Überwachung und davon immer mehr: Unser politisches und ökonomisches System nimmt Menschen mit eigenen Visionen Raum und Motivation. Es spült Systemerhalter nach oben, die keine Verantwortung mehr übernehmen, verwalten statt gestalten und damit die Volkswirtschaft ruinieren. Wer mehr will, kann es trotzdem schaffen. Er muss nur anders denken, als das System es verlangt. Hörhan gibt eine Anleitung für den inneren Widerstand gegen Kontrollstaat und Gleichmacherei, indem er zeigt, wie erfolgreiche Menschen ticken."
Child Category: Erfolg im Beruf
Web Player: https://www.audible.com/webplayer?asin=B00FWMTIDG
Release Date: 2013-10-18
---
# Null Bock Komplott: Warum immer die Falschen Karriere machen und wie ihr es trotzdem schafft

## Null Bock Komplott

[https://m.media-amazon.com/images/I/51U+lzszE1L._SL500_.jpg](https://m.media-amazon.com/images/I/51U+lzszE1L._SL500_.jpg)
