---
Search In Goodreads: https://www.goodreads.com/search?q=Michael%20Ende%20-%20Die%20unendliche%20Geschichte
Authors: Michael Ende
Ratings: 10006
Isbn 10: 3522202600
Format: Ungekürztes Hörbuch
Language: German
Publishers: HörbucHHamburg HHV GmbH
Isbn 13: 9783522202602
Tags: 
    - Literatur & Belletristik
Added: 148
Progress: <1 min verbleibend
Categories: 
    - Jugendliche & Heranwachsende 
    - Literatur & Belletristik
Sample: https://samples.audible.de/bk/hamb/000717/bk_hamb_000717_sample.mp3
Asin: B00BM5F96W
Title: "Die unendliche Geschichte"
Title Short: "Die unendliche Geschichte"
Narrators: Gert Heidenreich
Blurb: "Michael Endes Meisterwerk erstmals in ungekürzter Lesung: Phantásien ist in Gefahr. Das Nichts droht alles zu verschlingen, denn die Kindliche Kaiserin..."
Cover: https://m.media-amazon.com/images/I/619-iEO72UL._SL500_.jpg
Parent Category: Jugendliche & Heranwachsende
Store Page Url: https://audible.de/pd/B00BM5F96W?ipRedirectOverride=true&overrideBaseCountry=true
Length: 15h 6m 
Summary: "Phantásien ist in Gefahr. Das Nichts droht alles zu verschlingen, denn die Kindliche Kaiserin ist krank. Wenn ihr nicht geholfen wird, muss Phantásien sich auflösen. Bastian möchte helfen - aber wie? Phantásien ist doch nur eine fiktive Welt, in die er sich durch ein Buch hineinflüchtet. Schließlich findet er einen Weg, um in der Geschichte zu bleiben. Gemeinsam mit dem jungen Krieger Atréju muss er nun etliche Abenteuer bestehen, um die Kindliche Kaiserin und seine neue Wirklichkeit zu retten. <p>Michael Endes Meisterwerk erstmals in ungekürzter Lesung. </p>"
Child Category: Literatur & Belletristik
Web Player: https://www.audible.com/webplayer?asin=B00BM5F96W
Release Date: 2013-03-06
---
# Die unendliche Geschichte

## Die unendliche Geschichte

[https://m.media-amazon.com/images/I/619-iEO72UL._SL500_.jpg](https://m.media-amazon.com/images/I/619-iEO72UL._SL500_.jpg)
