---
Search In Goodreads: https://www.goodreads.com/search?q=Thomas%20Sowell%20-%20Basic%20Economics%2C%20Fifth%20Edition
Authors: Thomas Sowell
Ratings: 138
Isbn 10: 0465056849
Format: Rede
My Rating: 5
Language: English
Publishers: Blackstone Audio, Inc.
Isbn 13: 9780465056842
Tags: 
    - Ökonomie
Added: 128
Progress: 13 Std. 59 Min. verbleibend
Categories: 
    - Geld & Finanzen 
    - Ökonomie
Sample: https://samples.audible.de/bk/blak/007126/bk_blak_007126_sample.mp3
Asin: B00PKS5HB4
Title: "Basic Economics, Fifth Edition: A Common Sense Guide to the Economy"
Title Short: "Basic Economics, Fifth Edition"
Narrators: Tom Weiner
Blurb: "Drawing on lively examples from around the world and from centuries of history, Sowell explains basic economic principles for the general public in plain English...."
Cover: https://m.media-amazon.com/images/I/51iZMhizapL._SL500_.jpg
Parent Category: Geld & Finanzen
Store Page Url: https://audible.de/pd/B00PKS5HB4?ipRedirectOverride=true&overrideBaseCountry=true
Length: 23h 47m 
Summary: "<p>In this fifth edition of <i>Basic Economics</i>, Thomas Sowell revises and updates his popular book on commonsense economics, bringing the world into clearer focus through a basic understanding of the fundamental economic principles and how they explain our lives. Drawing on lively examples from around the world and from centuries of history, Sowell explains basic economic principles for the general public in plain English. </p> <p><i>Basic Economics</i>,which has now been translated into six languages and has additional material online, remains true to its core principle: that the fundamental facts and principles of economics do not require jargon, graphs, or equations and can be learned in a relaxed and even enjoyable way. </p>"
Child Category: Ökonomie
Web Player: https://www.audible.com/webplayer?asin=B00PKS5HB4
Release Date: 2014-12-02
---
# Basic Economics, Fifth Edition: A Common Sense Guide to the Economy

## Basic Economics, Fifth Edition

[https://m.media-amazon.com/images/I/51iZMhizapL._SL500_.jpg](https://m.media-amazon.com/images/I/51iZMhizapL._SL500_.jpg)
