---
Search In Goodreads: https://www.goodreads.com/search?q=Stefan%20Merath%20-%20Die%20Kunst%20seine%20Kunden%20zu%20lieben
Authors: Stefan Merath
Ratings: 1529
Isbn 10: 386936176X
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Gabal
Isbn 13: 9783869361765
Tags: 
    - Marketing & Vertrieb
Added: 133
Progress: 8 Std. 47 Min. verbleibend
Categories: 
    - Wirtschaft & Karriere 
    - Marketing & Vertrieb
Sample: https://samples.audible.de/bk/gaba/000168/bk_gaba_000168_sample.mp3
Asin: B005N5HBTK
Title: "Die Kunst seine Kunden zu lieben: Neurostrategie für Unternehmer"
Title Short: "Die Kunst seine Kunden zu lieben"
Narrators: Heiko Grauel, Gabi Franke
Blurb: "Kommen deine Kunden von allein zu dir? Sind sie bereit, ohne Diskussion einen angemessenen Preis zu bezahlen..."
Cover: https://m.media-amazon.com/images/I/51Mk6OxKnWL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B005N5HBTK?ipRedirectOverride=true&overrideBaseCountry=true
Length: 10h 46m 
Summary: "<p>Kommen deine Kunden von allein zu dir? Sind sie bereit, ohne Diskussion einen angemessenen Preis zu bezahlen? Und hast du noch nicht mal ernstzunehmende Wettbewerber? Dann hast du in deinem Unternehmen sehr wahrscheinlich eine super Strategie entwickelt. Glückwunsch!</p> <p>Bei den meisten Selbstständigen und Unternehmern ist das aber eher nicht so. Und wenn, dann hält diese Situation vielleicht nur einige Monate oder wenige Jahre. Die herausragenden kleinen und mittleren Unternehmen haben alle eine klare Spezialisierung und ganz besondere Beziehungen zu ihren Kunden aufgebaut.</p> <p>Strategielehren basieren auf folgender Idee: Es gibt eine Methode zur Entwicklung einer Strategie. Diese ist dann nur umzusetzen. Aus der Arbeit mit tausenden Unternehmern in den letzten 17 Jahren wurde Autor Stefan Merath jedoch klar, dass man manchen Unternehmern die beste Strategie hinlegen kann und sie scheitern, wohingegen andere auch mit einer mittelmäßigen Strategie sehr erfolgreich werden. Der Unterschied liegt nicht im Konzept, sondern im Unternehmer. Und genau dort liegt der blinde Fleck aller anderen Strategielehren. <b>Das revolutionäre Konzept der Neurostrategie unterscheidet sich dadurch, dass der Unternehmer, der Stratege, selbst in den Mittelpunkt des Konzepts rückt.</b> Hast du gelernt, nicht nur eine Strategie zu entwickeln, sondern kontinuierlich strategisch zu denken, zu fühlen und zu handeln?</p> <p><i>Die Kunst, seine Kunden zu lieben</i> ist ein Praxishörbuch - eine Geschichte - und keine trockene Theorie. In diesem Businessroman triffst du auf den Unternehmer Olli Steinbach, der aufgrund frustrierender Erlebnisse mit den eigenen Kunden in ein tiefes Motivationsloch fällt. Der Unternehmer und Coach Wolfgang Radies (bekannt aus <i>Der Weg zum erfolgreichen Unternehmer</i>) hinterfragt und unterstützt. Er zeigt Olli nicht nur, warum die meisten Strategien in der Praxis nicht funktionieren, sondern nimmt ihn auch mit auf eine emotionale Reise, bei der Olli <b>sowohl für die Strategie seines Unternehmens als auch für sich und sein eigenes Leben völlig neue Perspektiven entwickelt.</b></p> <p>Mehrere Hörer dieses Hörbuchs gewannen den Strategiepreis des Strategieforums. Viele verfünf- oder verzehnfachten ihren Umsatz innerhalb eines Jahres. Handwerker erzielten einen Deckungsbeitrag von 230 Euro pro Handwerkerstunde usw. Die meisten reduzierten ihren Stress mit Kunden dramatisch und schufen viel erfüllendere Kundenbeziehungen. Und viele erzielten eine solche Alleinstellung, dass sie schlicht keine Wettbewerber mehr hatten.</p> <p><b>Wie du selbst zum Strategen wirst, erfährst du in diesem Hörbuch.</b></p>"
Child Category: Marketing & Vertrieb
Web Player: https://www.audible.com/webplayer?asin=B005N5HBTK
Release Date: 2011-09-15
---
# Die Kunst seine Kunden zu lieben: Neurostrategie für Unternehmer

## Die Kunst seine Kunden zu lieben

[https://m.media-amazon.com/images/I/51Mk6OxKnWL._SL500_.jpg](https://m.media-amazon.com/images/I/51Mk6OxKnWL._SL500_.jpg)
