---
Search In Goodreads: https://www.goodreads.com/search?q=Stephen%20Hawking%20-%20Kurze%20Antworten%20auf%20gro%26szlig%3Be%20Fragen
Authors: Stephen Hawking
Ratings: 3370
Isbn 10: 3763271260
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Der Hörverlag
Isbn 13: 9783763271269
Tags: 
    - Wissenschaft
Added: 65
Progress: Beendet
Categories: 
    - Wissenschaft & Technik 
    - Wissenschaft
Sample: https://samples.audible.de/bk/hoer/002619/bk_hoer_002619_sample.mp3
Asin: 3844531971
Title: "Kurze Antworten auf große Fragen"
Title Short: "Kurze Antworten auf große Fragen"
Narrators: Frank Arnold, Anja Stadlober, Herbert Schäfer, Björn Schalla
Blurb: "Stephen Hawkings Vermächtnis. Brillanter Physiker, revolutionärer Kosmologe, unerschütterlicher Optimist: Stephen Hawking beantwortet in..."
Cover: https://m.media-amazon.com/images/I/41ACci5EB1L._SL500_.jpg
Parent Category: Wissenschaft & Technik
Store Page Url: https://audible.de/pd/3844531971?ipRedirectOverride=true&overrideBaseCountry=true
Length: 6h 14m 
Summary: "<p>Stephen Hawkings Vermächtnis.</p> <p>Brillanter Physiker, revolutionärer Kosmologe, unerschütterlicher Optimist: Stephen Hawking beantwortet in seinem letzten Werk die drängendsten Fragen unserer Zeit und nimmt uns mit auf eine persönliche Reise durch das Universum seiner Weltanschauung. Seine Gedanken zu Ursprung und Zukunft der Menschheit sind zugleich eine Mahnung, unseren Heimatplaneten besser vor den Gefahren unserer Gegenwart zu schützen.</p> <p>In der Lesung von Frank Arnold hören wir, warum es uns Menschen gibt, woher wir kommen, ob wir den Weltraum bevölkern können, ob es Hawkings Meinung nach einen Gott im Multiversum gibt - und vieles mehr.</p>"
Child Category: Wissenschaft
Web Player: https://www.audible.com/webplayer?asin=3844531971
Release Date: 2018-10-22
---
# Kurze Antworten auf große Fragen

## Kurze Antworten auf große Fragen

[https://m.media-amazon.com/images/I/41ACci5EB1L._SL500_.jpg](https://m.media-amazon.com/images/I/41ACci5EB1L._SL500_.jpg)
