---
Search In Goodreads: https://www.goodreads.com/search?q=Ray%20Kroc%20-%20Die%20wahre%20Geschichte%20von%20McDonald's
Authors: Ray Kroc, Robert Anderson
Ratings: 227
Isbn 10: 3960920946
Format: Ungekürztes Hörbuch
Language: German
Publishers: FinanzBuch Verlag
Isbn 13: 9783960920946
Tags: 
    - Wirtschaft
    - Wirtschaft & Karriere
Added: 150
Progress: Beendet
Categories: 
    - Biografien & Erinnerungen 
    - Akademiker & Spezialisten
Sample: https://samples.audible.de/bk/riva/000698/bk_riva_000698_sample.mp3
Asin: 3960927819
Title: "Die wahre Geschichte von McDonald's: Erzählt von Gründer Ray Kroc"
Title Short: "Die wahre Geschichte von McDonald's"
Narrators: Markus Böker
Blurb: "Er ist der Mann hinter dem goldenen \"M\" und einer \"Vom Tellerwäscher zum Millionär\"-Geschichte, die ihresgleichen sucht: Ray Kroc, der..."
Cover: https://m.media-amazon.com/images/I/51Z7Zi9xCPL._SL500_.jpg
Parent Category: Biografien & Erinnerungen
Store Page Url: https://audible.de/pd/3960927819?ipRedirectOverride=true&overrideBaseCountry=true
Length: 8h 42m 
Summary: "<p>Er ist der Mann hinter dem goldenen \"M\" und einer \"Vom Tellerwäscher zum Millionär\"-Geschichte, die ihresgleichen sucht: Ray Kroc, der Gründer von McDonald’s. Nur wenige Unternehmer können wirklich von sich behaupten, dass sie unsere Art zu leben für immer verändert haben. Ray Kroc ist einer von ihnen.</p> <p>Doch noch viel interessanter als Ray Kroc, die Businesslegende, ist Ray Kroc, der einfache Mann. Ganz im Gegensatz zum typischen Start-up-Gründer oder Internetmillionär war er bereits 52 Jahre alt, als er auf die McDonald-Brüder traf und sein erstes Franchise eröffnete. Was folgte, ist legendär, doch kaum einer kennt die Anfänge.</p> <p>In seiner offiziellen Autobiografie meldet sich der Mann hinter der Legende selbst zu Wort. Ray Kroc ist ein begnadeter Geschichtenerzähler und unverwüstlicher Enthusiast - er wird Sie mit seiner McDonald's-Story mitreißen und inspirieren. Sie werden ihn danach nie mehr vergessen.</p>"
Child Category: Akademiker & Spezialisten
Web Player: https://www.audible.com/webplayer?asin=3960927819
Release Date: 2020-09-03
---
# Die wahre Geschichte von McDonald's: Erzählt von Gründer Ray Kroc

## Die wahre Geschichte von McDonald's

[https://m.media-amazon.com/images/I/51Z7Zi9xCPL._SL500_.jpg](https://m.media-amazon.com/images/I/51Z7Zi9xCPL._SL500_.jpg)
