---
Search In Goodreads: https://www.goodreads.com/search?q=Stephen%20R.%20Covey%20-%20The%207%20Habits%20of%20Highly%20Effective%20People
Authors: Stephen R. Covey
Ratings: 941
Isbn 10: 1476740054
Format: Ungekürztes Hörbuch
My Rating: 5
Language: English
Publishers: Simon & Schuster Audio
Isbn 13: 9781476740058
Tags: 
    - Management & Leadership
    - Erfolg im Beruf
    - Arbeitsplatz- & Organisationsverhalten
    - Persönlicher Erfolg
Added: 90
Progress: 8 Std. 39 Min. verbleibend
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/cove/000013/bk_cove_000013_sample.mp3
Asin: B004V03KUW
Title: "The 7 Habits of Highly Effective People: Powerful Lessons in Personal Change"
Title Short: "The 7 Habits of Highly Effective People"
Narrators: Stephen R. Covey
Blurb: "Stephen R. Covey's book, The 7 Habits of Highly Effective People, has been a top seller for the simple reason that it ignores trends and pop psychology...."
Cover: https://m.media-amazon.com/images/I/51OuvCFwyZL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B004V03KUW?ipRedirectOverride=true&overrideBaseCountry=true
Length: 13h 4m 
Summary: "<p>Stephen R. Covey's book, <i>The 7 Habits of Highly Effective People</i>, has been a top seller for the simple reason that it ignores trends and pop psychology for proven principles of fairness, integrity, honesty, and human dignity. Celebrating its 15th year of helping people solve personal and professional problems, this special anniversary edition includes a new foreword and afterword written by Covey that explore whether the 7 Habits are still relevant and answer some of the most common questions he has received over the past 15 years. </p> <p>This audio edition is the first ever unabridged recording of <i>The 7 Habits of Highly Effective People</i>. </p> <p><b>PLEASE NOTE: When you purchase this title, the accompanying reference material will be available in your Library section along with the audio.</b></p>"
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=B004V03KUW
Release Date: 2003-12-28
---
# The 7 Habits of Highly Effective People: Powerful Lessons in Personal Change

## The 7 Habits of Highly Effective People

[https://m.media-amazon.com/images/I/51OuvCFwyZL._SL500_.jpg](https://m.media-amazon.com/images/I/51OuvCFwyZL._SL500_.jpg)
