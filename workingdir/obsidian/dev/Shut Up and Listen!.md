---
Search In Goodreads: https://www.goodreads.com/search?q=Tilman%20Fertitta%20-%20Shut%20Up%20and%20Listen!
Authors: Tilman Fertitta, Jim Gray
Ratings: 29
Isbn 10: 1400213738
Format: Ungekürztes Hörbuch
My Rating: 5
Language: English
Publishers: HarperCollins Leadership
Isbn 13: 9781400213733
Tags: 
    - Management & Leadership
    - Erfolg im Beruf
    - Geschäftsentwicklung & Unternehmertum
Added: 67
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/tnwd/001622/bk_tnwd_001622_sample.mp3
Asin: 1400213754
Title: "Shut Up and Listen!: Hard Business Truths that Will Help You Succeed"
Title Short: "Shut Up and Listen!"
Narrators: Tilman Fertitta
Blurb: "One of the few true leadership road maps to the summit of career success and satisfaction, featuring concise learn-and-repeat principles for entrepreneurs, leaders of large companies, managers, and executives at any level...."
Cover: https://m.media-amazon.com/images/I/51HaXWWpiRL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/1400213754?ipRedirectOverride=true&overrideBaseCountry=true
Length: 3h 49m 
Summary: "<p><b>One of the few true leadership road maps to the summit of career success and satisfaction, featuring concise learn-and-repeat principles for entrepreneurs, leaders of large companies, managers, and executives at any level. </b></p> <p><b>\"Accomplished businessman Tilman Fertitta...employs his staccato baritone in this blunt primer on entrepreneurial leadership skills. Liberally dosed with braggadocio, his audio performance is consistently engaging. Clearly, Fertitta is a no-nonsense person, and this audiobook reflects his temperament.\" (</b><b><i>Audiofile </i></b><b>Magazine)</b></p> <p>For aspiring entrepreneurs or people in business, this audiobook will help you take your company to the next level. When you finish listening, you’ll know what you’re doing right and what you’re doing wrong to operate your business, and if you’re just getting started, it will help set you up for success.</p> <p>Tilman Fertitta, also known as the Billion Dollar Buyer, started his hospitality empire 30 years ago with just one restaurant. So he knows the challenges that business owners face, as well as the common pitfalls that cause them to go under. Over the years, he’s stayed true to the principles that helped him scale his business to what is believed to be the largest single-shareholder company in America, with more than four billion dollars in revenue, including hundreds of restaurants (Landry’s Seafood, Bubba Gump Shrimp Company, Morton’s Steakhouse, Mastro’s, The Chart House, Rainforest Café, and more than 40 more restaurant concepts) and five Golden Nugget Casinos. He’s also sole owner of the NBA’s Houston Rockets. In <i>Shut Up and Listen!</i>, he shares the key insights that made it all possible.</p> <p>When entrepreneurs appear on <i>Billion Dollar Buyer</i>, the biggest obstacles they often face are ones they don’t suspect: not knowing your numbers, not knowing your strengths and weaknesses, or not being willing to go that extra mile with your customers. Fertitta has seen it all. He knows that what you aren’t paying attention to can either sink your business or become the very things that launch you to the top. As Fertitta says: <b>\"You might think you know what you’re doing, but I’m going to show you what you don’t know.\"</b></p> <p>Fertitta shares straight-talk \"Tilmanisms\" around six key action items that any entrepreneur can adopt today:</p> <p></p> <ul> <li>Be the Bull</li> <li>No Spare Customers</li> <li>Change, Change, Change</li> <li>Know Your Numbers</li> <li>Follow the 95/5 Rule</li> <li>Take No Out of Your Vocabulary</li> </ul> <p>A groundbreaking, no-holds-barred audiobook, <i>Shut Up and Listen! </i>offers practical, hard-earned wisdom from one of the most successful business owners in the world.</p>"
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=1400213754
Release Date: 2019-09-17
---
# Shut Up and Listen!: Hard Business Truths that Will Help You Succeed

## Shut Up and Listen!

[https://m.media-amazon.com/images/I/51HaXWWpiRL._SL500_.jpg](https://m.media-amazon.com/images/I/51HaXWWpiRL._SL500_.jpg)
