---
Search In Goodreads: https://www.goodreads.com/search?q=Reed%20Hastings%20-%20Keine%20Regeln
Authors: Reed Hastings, Erin Meyer
Ratings: 744
Isbn 10: 3843722684
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: HörbucHHamburg HHV GmbH
Isbn 13: 9783843722681
Tags: 
    - Management & Leadership
    - Arbeitsplatz- & Organisationsverhalten
Added: 134
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/hamb/003239/bk_hamb_003239_sample.mp3
Asin: 3844923543
Title: "Keine Regeln: Warum Netflix so erfolgreich ist"
Title Short: "Keine Regeln"
Narrators: Simone Kabst, Wolfgang Wagner
Blurb: "Die ungekürzte Lesung zur Netflix-Unternehmenstrategie. Netflix ist eines der erfolgreichsten Unternehmen der Welt und wird für seine..."
Cover: https://m.media-amazon.com/images/I/41Y0o7HW2tL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/3844923543?ipRedirectOverride=true&overrideBaseCountry=true
Length: 10h 20m 
Summary: "<p>Die ungekürzte Lesung zur Netflix-Unternehmenstrategie.</p> <p>Netflix ist eines der erfolgreichsten Unternehmen der Welt und wird für seine Innovationskraft, Flexibilität, Geschwindigkeit und seinen unternehmerischen Mut bewundert. Gibt es dahinter ein Geheimnis? Außergewöhnlich sind vor allem die Unternehmensleitlinien, die für alle Mitarbeiter der Maßstab ihrer Arbeit sind. Hier einige Beispiele:</p> <ul> <li>Harte Arbeit ist irrelevant.</li> <li>Niemand soll seinem Chef nach dem Mund reden.</li> <li>Jeder Mitarbeiter kann so viele Urlaubstage nehmen, wie er möchte.</li> <li>Netflix zahlt die besten Gehälter.</li> <li>Netflix will keine Angeber und Selbstdarsteller.</li> <li>Niemand kommt nackt zur Arbeit.</li> </ul> <p>Allen ist klar, dass ein gut klingendes Konzept vollkommen wertlos ist, wenn die Ideen nicht von der Leitungsebene vorgelebt werden. Aufrichtigkeit und exzellente Mitarbeiter sind für Netflix-Chef Reed Hastings die wesentlichen Schlüssel zu überdurchschnittlichen Ergebnissen. Wenn diese Voraussetzungen erfüllt sind, kann ein Unternehmen auf starre Regeln wie Arbeiten auszuführen sind und sogar auf Entscheidungshierarchien verzichten.</p>"
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=3844923543
Release Date: 2020-09-15
---
# Keine Regeln: Warum Netflix so erfolgreich ist

## Keine Regeln: Warum Netflix so erfolgreich ist

[https://m.media-amazon.com/images/I/41Y0o7HW2tL._SL500_.jpg](https://m.media-amazon.com/images/I/41Y0o7HW2tL._SL500_.jpg)
