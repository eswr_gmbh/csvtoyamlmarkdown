---
Search In Goodreads: https://www.goodreads.com/search?q=Frank%20Behrendt%20-%20Liebe%20dein%20Leben%20und%20nicht%20deinen%20Job
Authors: Frank Behrendt
Ratings: 291
Isbn 10: 3328102817
Format: Ungekürztes Hörbuch
My Rating: 3
Language: German
Publishers: Audible Studios
Isbn 13: 9783328102816
Tags: 
    - Erfolg im Beruf
Added: 120
Progress: 12 Min. verbleibend
Categories: 
    - Wirtschaft & Karriere 
    - Erfolg im Beruf
Sample: https://samples.audible.de/bk/adko/003336/bk_adko_003336_sample.mp3
Asin: B07CHKR99W
Title: "Liebe dein Leben und nicht deinen Job: 10 Ratschläge für eine entspannte Haltung"
Title Short: "Liebe dein Leben und nicht deinen Job"
Narrators: Hans Jürgen Stockerl
Blurb: "Frank Behrendt ist der \"Guru der Gelassenheit\" oder auch der \"Lord des Loslassens\". So jedenfalls nennen ihn die Medien, seit seine..."
Cover: https://m.media-amazon.com/images/I/51RkEKzLX5L._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B07CHKR99W?ipRedirectOverride=true&overrideBaseCountry=true
Length: 5h 59m 
Summary: "<p>Nimm dich ernst, aber nicht wichtig! Gelassen und entspannt durch Job und Alltag.</p> <p>Frank Behrendt ist der \"Guru der Gelassenheit\" oder auch der \"Lord des Loslassens\". So jedenfalls nennen ihn die Medien, seit seine erfrischenden Thesen zur Bewältigung eines anstrengenden Berufslebens im Internet für Furore gesorgt haben. Welchen Nerv treffen diese Thesen? Warum haben so viele Menschen so positiv darauf reagiert?</p> <p>Trotz 60-Stunden-Woche und engagiertem Familienleben ist der erfolgreiche Agenturmanager tatsächlich ein tiefenentspannter, grundfröhlicher Mensch. Wie er das schafft? Mut, Verrücktheit, Spontaneität, Spaß, Humor, ein hohes Maß an Disziplin und Konsequenz, ein verlässliches Bauchgefühl und eine gute Portion Selbstironie - das sind nur einige der Eigenschaften, die ihn auszeichnen.</p> <p>In diesem Hörbuch erzählt Frank Behrendt, was ihn geprägt hat, mit welcher Haltung er Dinge angeht, wie er so angstfrei geworden ist, was ihm Kraft und Energie gibt. Kurzweilig, informativ und selbstironisch lässt er Höhepunkte und Niederlagen seines Lebens Revue passieren, immer mit Blick darauf, was ihm geholfen hat, die Herausforderungen zu meistern. So füllt er die 10 Thesen mit \"gelebtem Leben\", legt ihren tieferen Sinn frei - eine Inspiration für Jeden!</p> <p>\"Im Job bist du oft der Zampano, aber Zuhause bringst du den Müll raus\" - wie wichtig es ist, sich zu erden, immer wieder die Perspektive zu ändern.</p> <p>\"Ein hohes Maß an Disziplin ist kein Widerspruch zu Entspanntheit\" - wie das geht, erzählt Frank Behrendt kurzweilig, witzig und überzeugend.</p> <p>\"Kein Mitarbeiter hat verdient, dass ich schlechte Laune habe\" - auf die Haltung kommt es an!</p> <p>\"Vor einem Chef im goldenen Glitzeranzug hat niemand Angst\" - wie befreiend es ist, über sich selbst zu lachen.</p> <p></p> <ul> <li>10 überraschende Thesen für die optimale Balance zwischen Arbeit und Privatleben;</li> <li>Kurzweilige und witzige Geschichten und Einblicke;</li> <li>Work-Life-Balance ganz anders: Leidenschaft, Leichtigkeit und Spielfreude.<br>  </li> </ul> <p>>> Diese ungekürzte Hörbuch-Fassung genießt du exklusiv nur bei Audible.</p>"
Child Category: Erfolg im Beruf
Web Player: https://www.audible.com/webplayer?asin=B07CHKR99W
Release Date: 2018-05-03
---
# Liebe dein Leben und nicht deinen Job: 10 Ratschläge für eine entspannte Haltung

## Liebe dein Leben und nicht deinen Job

[https://m.media-amazon.com/images/I/51RkEKzLX5L._SL500_.jpg](https://m.media-amazon.com/images/I/51RkEKzLX5L._SL500_.jpg)
