---
Search In Goodreads: https://www.goodreads.com/search?q=Jon%20Kabat-Zinn%20-%20Wherever%20You%20Go%20There%20You%20Are
Authors: Jon Kabat-Zinn
Ratings: 29
Isbn 10: 0349414238
Format: Gekürztes Hörbuch
Language: English
Publishers: Macmillan Audio
Isbn 13: 9780349414232
Tags: 
    - Alternative & Komplementäre Medizin
    - Persönlicher Erfolg
    - Stressmanagement
    - Buddhismus
    - Spiritualität
Added: 158
Categories: 
    - Gesundheit & Wellness 
    - Alternative & Komplementäre Medizin
Sample: https://samples.audible.de/bk/aren/000061/bk_aren_000061_sample.mp3
Asin: B004V550Y6
Title: "Wherever You Go There You Are"
Title Short: "Wherever You Go There You Are"
Narrators: Jon Kabat-Zinn
Blurb: "Warmth, humor, anecdotes, and poems make up this inspirational guide to a revolutionary new way of being, seeing, and living..."
Cover: https://m.media-amazon.com/images/I/511lprkhs9L._SL500_.jpg
Parent Category: Gesundheit & Wellness
Store Page Url: https://audible.de/pd/B004V550Y6?ipRedirectOverride=true&overrideBaseCountry=true
Length: 3 Std. 9 Min.
Summary: "Warmth, humor, anecdotes, and poems make up this inspirational guide to a revolutionary new way of being, seeing, and living. Exploring principles and practices of mindfulness, Dr. Kabat Zinn has taught this two thousand year old Buddhist method of relaxation to thousands. Learn how to capture the present, to live fully in the moment and reduce anxiety, achieve inner peace, and enrich the quality of life. Let this be your guide to mindfulness meditation in everyday life."
Child Category: Alternative & Komplementäre Medizin
Web Player: https://www.audible.com/webplayer?asin=B004V550Y6
Release Date: 2000-02-24
---
# Wherever You Go There You Are

## Wherever You Go There You Are

[https://m.media-amazon.com/images/I/511lprkhs9L._SL500_.jpg](https://m.media-amazon.com/images/I/511lprkhs9L._SL500_.jpg)
