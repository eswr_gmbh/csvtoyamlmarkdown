---
Search In Goodreads: https://www.goodreads.com/search?q=Yuval%20Noah%20Harari%20-%20Homo%20Deus%3A%20Eine%20Geschichte%20von%20Morgen
Authors: Yuval Noah Harari
Ratings: 8963
Isbn 10: 342104595X
Format: Ungekürztes Hörbuch
My Rating: 4
Language: German
Publishers: Der Hörverlag
Isbn 13: 9783421045959
Tags: 
    - Politik & Regierungen
    - Zukunftsstudien
Added: 29
Progress: Beendet
Categories: 
    - Politik & Sozialwissenschaften 
    - Politik & Regierungen
Sample: https://samples.audible.de/bk/hoer/001856/bk_hoer_001856_sample.mp3
Asin: B06X3RQW9P
Title: "Homo Deus: Eine Geschichte von Morgen"
Title Short: "Homo Deus: Eine Geschichte von Morgen"
Narrators: Jürgen Holdorf
Blurb: "Über das letzte Jahrhundert hinweg hat sich die Menschheit Hunger, Seuchen und Kriegen erfolgreich gestellt. Aus Erfolg wird Ehrgeiz..."
Cover: https://m.media-amazon.com/images/I/41bJmo4dOeL._SL500_.jpg
Parent Category: Politik & Sozialwissenschaften
Store Page Url: https://audible.de/pd/B06X3RQW9P?ipRedirectOverride=true&overrideBaseCountry=true
Length: 17h 13m 
Summary: "Die nächste Stufe der Evolution. <br> <br>Über das letzte Jahrhundert hinweg hat sich die Menschheit Hunger, Seuchen und Kriegen erfolgreich gestellt. Aus Erfolg wird Ehrgeiz: als nächstes stehen Unsterblichkeit, grenzenloses Glück und gottgleiche Schöpfungskräfte auf der To-Do-Liste. Die Verfolgung dieser Ziele wird die meisten Menschen allerdings überflüssig machen. Also bleiben Fragen: Wohin führt unser Weg? Wie können wir unsere Zukunft bestmöglich beeinflussen? Schließlich kann man den Lauf der Dinge nicht aufhalten, aber die Richtung bestimmen."
Child Category: Politik & Regierungen
Web Player: https://www.audible.com/webplayer?asin=B06X3RQW9P
Release Date: 2017-02-24
---
# Homo Deus: Eine Geschichte von Morgen

## Homo Deus: Eine Geschichte von Morgen

[https://m.media-amazon.com/images/I/41bJmo4dOeL._SL500_.jpg](https://m.media-amazon.com/images/I/41bJmo4dOeL._SL500_.jpg)
