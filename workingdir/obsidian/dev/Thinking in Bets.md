---
Search In Goodreads: https://www.goodreads.com/search?q=Annie%20Duke%20-%20Thinking%20in%20Bets
Authors: Annie Duke
Ratings: 146
Isbn 10: 0735216363
Format: Ungekürztes Hörbuch
Language: English
Publishers: Penguin Audio
Isbn 13: 9780735216365
Tags: 
    - Management & Leadership
    - Erfolg im Beruf
    - Seelische & Geistige Gesundheit
Added: 186
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/peng/003379/bk_peng_003379_sample.mp3
Asin: B07954FBKF
Title: "Thinking in Bets: Making Smarter Decisions When You Don't Have All the Facts"
Title Short: "Thinking in Bets"
Narrators: Annie Duke
Blurb: "Poker champion turned business consultant Annie Duke teaches you how to get comfortable with uncertainty and make better decisions as a result...."
Cover: https://m.media-amazon.com/images/I/41CueL9WfHL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B07954FBKF?ipRedirectOverride=true&overrideBaseCountry=true
Length: 6h 50m 
Summary: "<p><b>Poker champion turned business consultant Annie Duke teaches you how to get comfortable with uncertainty and make better decisions as a result.</b> </p> <p>In Super Bowl XLIX, Seahawks coach Pete Carroll made one of the most controversial calls in football history: With 26 seconds remaining, and trailing by four at the Patriots' one-yard line, he called for a pass instead of a handing off to his star running back. The pass was intercepted, and the Seahawks lost. Critics called it the dumbest play in history. But was the call really that bad? Or did Carroll actually make a great move that was ruined by bad luck? </p> <p>Even the best decision doesn't yield the best outcome every time. There's always an element of luck that you can't control, and there is always information that is hidden from view. So the key to long-term success (and avoiding worrying yourself to death) is to think in bets: How sure am I? What are the possible ways things could turn out? What decision has the highest odds of success? Did I land in the unlucky 10 percent on the strategy that works 90 percent of the time? Or is my success attributable to dumb luck rather than great decision making? </p> <p>Annie Duke, a former World Series of Poker champion turned business consultant, draws on examples from business, sports, politics, and (of course) poker to share tools anyone can use to embrace uncertainty and make better decisions. For most people, it's difficult to say \"I'm not sure\" in a world that values and even rewards the appearance of certainty. But professional poker players are comfortable with the fact that great decisions don't always lead to great outcomes and bad decisions don't always lead to bad outcomes. </p> <p>By shifting your thinking from a need for certainty to a goal of accurately assessing what you know and what you don't, you'll be less vulnerable to reactive emotions, knee-jerk biases, and destructive habits in your decision making. You'll become more confident, calm, compassionate, and successful in the long run. </p> <p><i>Includes a bonus PDF of charts and graphs. </i></p> <p><b>PLEASE NOTE: When you purchase this title, the accompanying PDF will be available in your Audible Library along with the audio.</b> </p>"
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=B07954FBKF
Release Date: 2018-02-06
---
[# Thinking in Bets: Making Smarter Decisions When You Don't Have All the Facts

## Thinking in Bets

![altText|200](https://m.media-amazon.com/images/I/41CueL9WfHL._SL500_.jpg)

## Thinking in Bets




## Notes
#### Resulting.
Resulting is when you are judging your decisions on the outcome and not based. What you know at the time. 
for example, you have the power to change the moment. But maybe not really the outcome.

If you ask people, what was a great or a bead decision they did. They Probably would measure this from the outcome. 
[[Hindsight bias]]

Nach dem Motto:
- hätte ich dies doch früher gewusst.
- Ich hab es dir/mir doch gesagt.

Der Punkt ist der, wir sollten eher lernen mit den Grenzen des Gehirns klar kommen, denken etc. An stelle es versuchen zu überlasten
z.b Wissen wir genau, dass wir auf eine Optische Illusion schauen. Sehen sie aber dennoch weiter hin.
