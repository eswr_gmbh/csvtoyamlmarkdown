---
Search In Goodreads: https://www.goodreads.com/search?q=John%20E.%20Siers%20-%20Someday%20the%20Stars
Authors: John E. Siers
Format: Ungekürztes Hörbuch
Language: English
Publishers: Theogony Books
Book Numbers: 2
Tags: 
    - Erstkontakt
    - Space Opera
Added: 191
Progress: Beendet
Categories: 
    - Science Fiction & Fantasy 
    - Science Fiction
Sample: https://samples.audible.de/bk/acx0/293275/bk_acx0_293275_sample.mp3
Asin: B09QD4H4LC
Title: "Someday the Stars: The Lunar Free State, Book 2"
Title Short: "Someday the Stars"
Narrators: Jimmy Moreland
Blurb: "It’s been 10 years since the Lunar Free State established itself on Earth’s moon. Now the Lunar Free State must face two undeniable facts: The humans of Earth are not the only intelligent life in the universe, and travel to other star systems is possible...."
Series: The Lunar Free State (book 2)
Cover: https://m.media-amazon.com/images/I/51CDFLdbqZL._SL500_.jpg
Parent Category: Science Fiction & Fantasy
Store Page Url: https://audible.de/pd/B09QD4H4LC?ipRedirectOverride=true&overrideBaseCountry=true
Length: 15h 9m 
Summary: "<p>Someday...is now! </p> <p>It’s been 10 years since the Lunar Free State established itself as an independent nation on Earth’s moon. Since then, other Earth nations have pushed into space, competing with the LFS for the untapped resources of the solar system, but - thanks to the advantages provided by its gravity-driven spacecraft - the LFS has continued to grow and prosper, and relations with the Earth nations remain...strained, at best. </p> <p>When the research ship LFS <i>Stephen Hawking</i> encounters an alien probe near Saturn, though, the craft leads <i>Hawking </i>on a merry chase before vanishing into...well, for want of a better term, hyperspace. Now the Lunar Free State must face two undeniable facts: The humans of Earth are not the only intelligent life in the universe, and travel to other star systems is possible. </p> <p>The LFS has to figure out how to get to hyperspace, too, and the time they have to do so is limited. Someone else out there now knows humanity exists, and the countdown clock is ticking until whoever sent the alien craft decides to come calling.</p>"
Child Category: Science Fiction
Web Player: https://www.audible.com/webplayer?asin=B09QD4H4LC
Release Date: 2022-01-13
---
