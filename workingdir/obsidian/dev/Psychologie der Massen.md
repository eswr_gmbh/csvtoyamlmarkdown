---
Search In Goodreads: https://www.goodreads.com/search?q=Gustav%20Le%20Bon%20-%20Psychologie%20der%20Massen
Authors: Gustav Le Bon
Ratings: 722
Isbn 10: 3752931523
Format: Ungekürztes Hörbuch
My Rating: 3
Language: German
Publishers: Aureon Verlag
Isbn 13: 9783752931525
Tags: 
    - Seelische & Geistige Gesundheit
Added: 84
Progress: Beendet
Categories: 
    - Gesundheit & Wellness 
    - Seelische & Geistige Gesundheit
Sample: https://samples.audible.de/bk/aure/000014/bk_aure_000014_sample.mp3
Asin: B00KYUVV22
Title: "Psychologie der Massen"
Title Short: "Psychologie der Massen"
Narrators: Jan Peter Richter
Blurb: "\"Machen wir uns den Sachverhalt nochmals klar: Wenn die Psychologie, welche die Anlagen, Triebregungen, Motive, Absichten..."
Cover: https://m.media-amazon.com/images/I/51IiZMW+jOL._SL500_.jpg
Parent Category: Gesundheit & Wellness
Store Page Url: https://audible.de/pd/B00KYUVV22?ipRedirectOverride=true&overrideBaseCountry=true
Length: 5h 30m 
Summary: "\"Machen wir uns den Sachverhalt nochmals klar: Wenn die Psychologie, welche die Anlagen, Triebregungen, Motive, Absichten eines einzelnen Menschen bis zu seinen Handlungen und in die Beziehungen zu seinen Nächsten verfolgt, ihre Aufgabe restlos gelöst und alle diese Zusammenhänge durchsichtig gemacht hätte, dann fände sie sich plötzlich vor einer neuen Aufgabe, die sich ungelöst vor ihr erhebt. Sie müßte die überraschende Tatsache erklären, daß dies ihr verständlich gewordene Individuum unter einer bestimmten Bedingung ganz anders fühlt, denkt und handelt, als von ihm zu erwarten stand, und diese Bedingung ist die Einreihung in eine Menschenmenge, welche die Eigenschaft einer \"psychologischen Masse\" erworben hat. Was ist nun eine \"Masse\", wodurch erwirbt sie die Fähigkeit, das Seelenleben des Einzelnen so entscheidend zu beeinflussen, und worin besteht die seelische Veränderung, die sie dem Einzelnen aufnötigt?\" <br> <br>Dieser Frage, niedergeschrieben von Sigmund Freud in seinem Buch \"Massenpsychologie und Ich-Analyse\", widmete sich der französische Gelehrte Gustav Le Bon erstmals umfassend in seinem Hauptwerk \"Psychologie der Massen\" aus dem Jahr 1895. Er beschreibt hier intensiv die Verhaltensveränderung des Individuums, die sich aus ihrer Zugehörigkeit zu einem größeren Personenkreis, einer Masse, ergibt. Mit diesem Werk wurde Le Bon zum Begründer und Vordenker der Massenpsychologie. Seine Ideen und Ansichten entwickelte er auf zahlreichen Reisen zu verschiedenen Völkern Afrikas und Asiens. Dort betrieb Le Bon intensive völkerkundliche Studien und veröffentliche daraufhin zwei umfangreiche Beschreibungen der Kulturen des Orients und der Araber. <br> <br>Sein erfolgreichstes Buch, die Buchvorlage zum Hörbuch \"Psychologie der Massen\" wurde in 10 Sprachen übersetzt und beeinflusste nachweislich andere renommierte Wissenschaftler wie Max Weber oder Sigmund Freud."
Child Category: Seelische & Geistige Gesundheit
Web Player: https://www.audible.com/webplayer?asin=B00KYUVV22
Release Date: 2014-07-01
---
# Psychologie der Massen

## Psychologie der Massen

[https://m.media-amazon.com/images/I/51IiZMW+jOL._SL500_.jpg](https://m.media-amazon.com/images/I/51IiZMW+jOL._SL500_.jpg)
