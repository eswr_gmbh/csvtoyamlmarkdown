---
Search In Goodreads: https://www.goodreads.com/search?q=Seth%20Godin%20-%20The%20Dip
Authors: Seth Godin
Ratings: 247
Isbn 10: 0748129278
Format: Ungekürztes Hörbuch
My Rating: 5
Language: English
Publishers: Audible Studios
Isbn 13: 9780748129270
Tags: 
    - Erfolg im Beruf
    - Persönlicher Erfolg
    - Zeitmanagement
Added: 61
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Erfolg im Beruf
Sample: https://samples.audible.de/bk/adbl/000068/bk_adbl_000068_sample.mp3
Asin: B004V1BCTM
Title: "The Dip"
Title Short: "The Dip"
Narrators: Seth Godin
Blurb: "Every new project (or job, or hobby, or company) starts out exciting and fun. Then it gets harder and less fun, until it hits a low point...."
Cover: https://m.media-amazon.com/images/I/41q9ys6E3BL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B004V1BCTM?ipRedirectOverride=true&overrideBaseCountry=true
Length: 1h 32m 
Summary: "Every new project (or job, or hobby, or company) starts out exciting and fun. Then it gets harder and less fun, until it hits a low point: really hard, and not much fun at all. <p>And then you find yourself asking if the goal is even worth the hassle. Maybe you're in a Dip: a temporary setback that you will overcome if you keep pushing. But maybe it's really a Cul-de-Sac, which will <i>never</i> get better, no matter how hard you try.</p> <p>According to best-selling author Seth Godin, what really sets superstars apart from everyone else is the ability to escape dead ends quickly while staying focused and motivated when it really counts.</p> <p>Winners quit fast, quit often, and quit without guilt: until they commit to beating the <i>right</i> Dip for the <i>right</i> reasons. In fact, winners <i>seek out</i> the Dip. They realize that the bigger the barrier, the bigger the reward for getting past it. If you can become number one in your niche, you'll get more than your fair share of profits, glory, and long-term security.</p> <p>Losers, on the other hand, fall into two basic traps. Either they fail to stick out the Dip - they get to the moment of truth and then give up - or they never even find the right Dip to conquer.</p> <p>Whether you're a graphic designer, a sales rep, an athlete, or an aspiring CEO, this fun little book will help you figure out if you're in a Dip that's worthy of your time, effort, and talents. If you are, <i>The Dip</i> will inspire you to hang tough. If not, it will help you find the courage to quit so you can be number one at something else.</p> <p>Seth Godin doesn't claim to have all the answers. But he will teach you how to ask the right questions.</p>"
Child Category: Erfolg im Beruf
Web Player: https://www.audible.com/webplayer?asin=B004V1BCTM
Release Date: 2007-04-27
---
# The Dip

## The Dip

[https://m.media-amazon.com/images/I/41q9ys6E3BL._SL500_.jpg](https://m.media-amazon.com/images/I/41q9ys6E3BL._SL500_.jpg)
