---
Search In Goodreads: https://www.goodreads.com/search?q=Encyclopaedia%20Britannica%20-%20Benjamin%20Franklin
Authors: Encyclopaedia Britannica
Format: Ungekürztes Hörbuch
Language: English
Publishers: Encyclopaedia Britannica
Book Numbers: ∞
Tags: 
    - Biografien & Erinnerungen
    - Nord-
    - Mittel- & Südamerika
Added: 173
Progress: 6 Min. verbleibend
Categories: 
    - Geschichte 
    - Nord-
    - Mittel- & Südamerika
Sample: https://samples.audible.de/bk/brit/000011/bk_brit_000011_sample.mp3
Asin: B004UVLI24
Title: "Benjamin Franklin: The Founding Fathers Series"
Title Short: "Benjamin Franklin"
Narrators: Richard M. Davidson
Blurb: "In this edition, the editors of Encyclopaedia Britannica examine the life and multiple talents of Benjamin Franklin...."
Series: The Founding Fathers
Cover: https://m.media-amazon.com/images/I/51S8ydF6o3L._SL500_.jpg
Parent Category: Geschichte
Store Page Url: https://audible.de/pd/B004UVLI24?ipRedirectOverride=true&overrideBaseCountry=true
Length: 26h 0m 
Summary: "The Founding Fathers Series sheds new light on the revolutionary leaders who planned, fought for, and created the United States of America. This series is about the people who led America from monarchy to republic as well as the tools they used. Perhaps more importantly, it is about the principles upon which the American republic was founded. The men who initiated \"the great experiment\" of American freedom and democracy are examined here, as are the events, ideas, and profound documents that changed the course of world history. <p>Though they had different backgrounds, skills, and ideas, the Founding Fathers came together to forge a new nation and persevered against the odds to achieve America's independence. In this edition, the editors of Encyclopaedia Britannica examine the life and multiple talents of Benjamin Franklin.</p>"
Child Category: Nord-, Mittel- & Südamerika
Web Player: https://www.audible.com/webplayer?asin=B004UVLI24
Release Date: 2007-11-15
---
# Benjamin Franklin: The Founding Fathers Series

## Benjamin Franklin

### The Founding Fathers

[https://m.media-amazon.com/images/I/51S8ydF6o3L._SL500_.jpg](https://m.media-amazon.com/images/I/51S8ydF6o3L._SL500_.jpg)
