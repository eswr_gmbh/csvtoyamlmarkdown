---
Search In Goodreads: https://www.goodreads.com/search?q=John%20Carreyrou%20-%20Bad%20Blood
Authors: John Carreyrou
Ratings: 499
Isbn 10: 3421048231
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Verlag Michael John Media
Isbn 13: 9783421048233
Tags: 
    - Politik & Regierungen
Added: 73
Progress: Beendet
Categories: 
    - Politik & Sozialwissenschaften 
    - Politik & Regierungen
Sample: https://samples.audible.de/bk/jome/000508/bk_jome_000508_sample.mp3
Asin: 3963840137
Title: "Bad Blood: Die wahre Geschichte des größten Betrugs im Silicon Valley"
Title Short: "Bad Blood"
Narrators: Mark Bremer
Blurb: "Elizabeth Holmes, die Gründerin von Theranos, galt lange als der weibliche Steve Jobs. Das 19-jährige Start-up-Wunderkind versprach, mit..."
Cover: https://m.media-amazon.com/images/I/41dFltxYCeL._SL500_.jpg
Parent Category: Politik & Sozialwissenschaften
Store Page Url: https://audible.de/pd/3963840137?ipRedirectOverride=true&overrideBaseCountry=true
Length: 12h 21m 
Summary: "<p>Der New-York-Times-Bestseller jetzt auf Deutsch. Financial Times and McKinsey & Company Business Book of the Year Award 2018. Time Magazine Platz 1 der Best Non-fiction Books of 2018.</p> <p>Elizabeth Holmes, die Gründerin von Theranos, galt lange als der weibliche Steve Jobs. Das 19-jährige Start-up-Wunderkind versprach, mit ihrer Firma die Medizinindustrie zu revolutionieren. Ein einziger Tropfen Blut sollte reichen, um Blutbilder zu erstellen und Therapien zu steuern - eine Riesenhoffnung für Millionen Menschen und ein extrem lukratives Geschäft.</p> <p>Namhafte Investoren steckten Unsummen in das junge Unternehmen, bis es mit neun Milliarden Dollar am Markt kapitalisiert war. Es gab nur ein einziges Problem: Die Technologie hinter den schicken Apparaturen hat nie funktioniert. Pulitzer-Preisträger John Carreyrou kam diesem gigantischen Betrug auf die Spur und erzählt in seinem preisgekrönten Buch die packende Geschichte seiner Enthüllung.</p>"
Child Category: Politik & Regierungen
Web Player: https://www.audible.com/webplayer?asin=3963840137
Release Date: 2019-05-03
---
# Bad Blood: Die wahre Geschichte des größten Betrugs im Silicon Valley

## Bad Blood

[https://m.media-amazon.com/images/I/41dFltxYCeL._SL500_.jpg](https://m.media-amazon.com/images/I/41dFltxYCeL._SL500_.jpg)
