---
Search In Goodreads: https://www.goodreads.com/search?q=Alexander%20Kennedy%20-%20Edison%3A%20A%20Life%20of%20Invention
Authors: Alexander Kennedy
My Rating: 5
Tags: 
    - 
Added: 71
Progress: Beendet
Categories: 
    - 
Asin: B01FKVWWZY
Title: "Edison: A Life of Invention"
Title Short: "Edison: A Life of Invention"
Narrators: Jack Nolan
Blurb: "Thomas Edison was named \"The Wizard of Menlo Park.\" He invented the motion picture camera, founded General Electric, and registered over 1000 patents in his lifetime...."
Cover: https://m.media-amazon.com/images/I/41loxs7Kq5L._SL500_.jpg
Store Page Url: https://audible.de/pd/B01FKVWWZY?ipRedirectOverride=true&overrideBaseCountry=true
Summary: ""
Store Page Missing: true
Web Player: https://www.audible.com/webplayer?asin=B01FKVWWZY
---
# Edison: A Life of Invention

## Edison: A Life of Invention

[https://m.media-amazon.com/images/I/41loxs7Kq5L._SL500_.jpg](https://m.media-amazon.com/images/I/41loxs7Kq5L._SL500_.jpg)
