---
Search In Goodreads: https://www.goodreads.com/search?q=David%20Epstein%20-%20Es%20lebe%20der%20Generalist!
Authors: David Epstein
Ratings: 89
Isbn 10: 3868817743
Format: Ungekürztes Hörbuch
Language: German
Publishers: Redline Verlag
Isbn 13: 9783868817744
Tags: 
    - Management & Leadership
    - Erfolg im Beruf
Added: 152
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/riva/000668/bk_riva_000668_sample.mp3
Asin: 3962672699
Title: "Es lebe der Generalist!: Warum sie gerade in einer spezialisierten Welt erfolgreicher sind"
Title Short: "Es lebe der Generalist!"
Narrators: Markus Böker
Blurb: "Spezialisierung sei der Schlüssel zum Erfolg, sagen viele Experten. Um Fähigkeiten, Instrumente oder Themengebiete zu beherrschen..."
Cover: https://m.media-amazon.com/images/I/41-5Ze9BvlL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/3962672699?ipRedirectOverride=true&overrideBaseCountry=true
Length: 13h 17m 
Summary: "<p>Spezialisierung sei der Schlüssel zum Erfolg, sagen viele Experten. Um Fähigkeiten, Instrumente oder Themengebiete zu beherrschen, müsse man früh anfangen und lange üben. David J. Epstein analysiert in seinem Bestseller Top-Performer in Wirtschaft und Wissenschaft, Ausnahmekünstler wie Vincent van Gogh und Profisportler wie Roger Federer oder Tiger Woods und belegt: Das ist eher die Ausnahme, denn die Regel!</p> <p>Generalisten legen vielleicht später los, dafür aber meist kreativer, agiler und mit Blick über den Tellerrand. Und haben letztlich Erfolg. Das Hörbuch ist ein eindrucksvolles Plädoyer, wieder mehr Überblick zu wagen - und zu fördern!</p>"
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=3962672699
Release Date: 2020-07-07
---
# Es lebe der Generalist!: Warum sie gerade in einer spezialisierten Welt erfolgreicher sind

## Es lebe der Generalist!

[https://m.media-amazon.com/images/I/41-5Ze9BvlL._SL500_.jpg](https://m.media-amazon.com/images/I/41-5Ze9BvlL._SL500_.jpg)
