---
Search In Goodreads: https://www.goodreads.com/search?q=Franz%20Kafka%20-%20Die%20Franz%20Kafka%20Box
Authors: Franz Kafka
Ratings: 67
Isbn 10: 3866103174
Format: Ungekürztes Hörbuch
Language: German
Publishers: Argon Verlag
Isbn 13: 9783866103177
Tags: 
    - Klassiker
Added: 147
Progress: 5 Std. 9 Min. verbleibend
Categories: 
    - Literatur & Belletristik 
    - Klassiker
Sample: https://samples.audible.de/bk/argo/000405/bk_argo_000405_sample.mp3
Asin: B004V1O2Z8
Title: "Die Franz Kafka Box"
Title Short: "Die Franz Kafka Box"
Narrators: Hans Peter Hallwachs, Wolfgang Schiffer, Oliver Nitsche
Blurb: "Aus Anlass des 125. Geburtstages im Juli 2008 sind die wichtigsten Erzählungen Franz Kafkas in diesem Download zusammengefasst."
Cover: https://m.media-amazon.com/images/I/512JT2jZB7L._SL500_.jpg
Parent Category: Literatur & Belletristik
Store Page Url: https://audible.de/pd/B004V1O2Z8?ipRedirectOverride=true&overrideBaseCountry=true
Length: 5h 42m 
Summary: "Franz Kafka wurde am 3. Juli 1883 als Sohn jüdischer Eltern in Prag geboren, der Stadt, in der er nahezu sein ganzes Leben verbrachte. Nach dem Jurastudium, das er 1906 mit der Promotion abschloss, schlug er eine Beamtenlaufbahn ein. 1912 begann seine Arbeit an dem Roman \"Der Verschollene\", der jedoch, ebenso wie \"Der Proceß\" und \"Das Schloß\" unvollendet blieb und erst nach Kafkas Tod gedruckt wurde. Das Erscheinen der Erzählungen \"Das Urteil\" im Jahre 1913 und \"Die Verwandlung\" zwei Jahre später verhalfen ihm zu literarischem Erfolg. Im Sommer 1917 erlitt Kafka einen Blutsturz; es war der Ausbruch der Tuberkulose, an deren Folgen er am 3. Juni 1924, noch nicht 41 Jahre alt, starb. <p> Die Erzählungen nehmen in Kafkas Werk eine bedeutende Stellung ein. Von ihm selbst als \"Stückchen\", bestenfalls als \"Geschichten\" bezeichnet, haben sie sich längst einen festen Platz in der Weltliteratur erobert. Aus Anlass des 125. Geburtstages im Juli 2008 sind die wichtigsten Erzählungen Franz Kafkas in dieser Box.</p>"
Child Category: Klassiker
Web Player: https://www.audible.com/webplayer?asin=B004V1O2Z8
Release Date: 2009-05-13
---
# Die Franz Kafka Box

## Die Franz Kafka Box

[https://m.media-amazon.com/images/I/512JT2jZB7L._SL500_.jpg](https://m.media-amazon.com/images/I/512JT2jZB7L._SL500_.jpg)
