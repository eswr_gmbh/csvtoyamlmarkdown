---
Search In Goodreads: https://www.goodreads.com/search?q=Yuval%20Noah%20Harari%20-%2021%20Lektionen%20f%26uuml%3Br%20das%2021.%20Jahrhundert
Authors: Yuval Noah Harari
Ratings: 7516
Isbn 10: 3406727786
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Der Hörverlag
Isbn 13: 9783406727788
Tags: 
    - Politik & Regierungen
    - Zukunftsstudien
Added: 36
Progress: Beendet
Categories: 
    - Politik & Sozialwissenschaften 
    - Politik & Regierungen
Sample: https://samples.audible.de/bk/hoer/002592/bk_hoer_002592_sample.mp3
Asin: 3844532870
Title: "21 Lektionen für das 21. Jahrhundert"
Title Short: "21 Lektionen für das 21. Jahrhundert"
Narrators: Jürgen Holdorf
Blurb: "Was sind die großen Herausforderungen, vor denen die Menschheit steht? Wer sind wir und was sollen wir mit unserem Leben anfangen? Seit Jahrtausenden..."
Cover: https://m.media-amazon.com/images/I/41GT81ltmtL._SL500_.jpg
Parent Category: Politik & Sozialwissenschaften
Store Page Url: https://audible.de/pd/3844532870?ipRedirectOverride=true&overrideBaseCountry=true
Length: 14h 1m 
Summary: "<p>Was sollten wir unseren Kindern heute beibringen, damit sie für die Welt von morgen gerüstet sind? - Der Bestseller von Yuval Noah Harari</p> <p>Was sind die großen Herausforderungen, vor denen die Menschheit steht? Wer sind wir und was sollen wir mit unserem Leben anfangen? Seit Jahrtausenden fragt die Menschheit nach dem Sinn des Lebens. Doch jetzt setzen uns die ökologische Krise, die wachsende Bedrohung durch Massenvernichtungswaffen und der Aufstieg neuer disruptiver Technologien unter Zeitdruck. Irgendjemand wird darüber entscheiden müssen, wie wir die Macht nutzen, die künstliche Intelligenz und Biotechnologie bereit halten. Yuval Noah Harari will Menschen dazu anregen, sich an den großen Debatten unserer Zeit zu beteiligen.</p>"
Child Category: Politik & Regierungen
Web Player: https://www.audible.com/webplayer?asin=3844532870
Release Date: 2018-09-18
---
# 21 Lektionen für das 21. Jahrhundert

## 21 Lektionen für das 21. Jahrhundert

![https://m.media-amazon.com/images/I/41GT81ltmtL._SL500_.jpg](https://m.media-amazon.com/images/I/41GT81ltmtL._SL500_.jpg)
