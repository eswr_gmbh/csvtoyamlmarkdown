---
Search In Goodreads: https://www.goodreads.com/search?q=David%20Einhorn%20-%20Fooling%20Some%20of%20the%20People%20All%20of%20the%20Time
Authors: David Einhorn
Ratings: 29
Isbn 10: 0470073942
Format: Ungekürztes Hörbuch
Language: English
Publishers: Audible Studios
Isbn 13: 9780470073940
Tags: 
    - Ökonomie
    - Geld & Finanzen
Added: 92
Progress: 10 Std. verbleibend
Categories: 
    - Geld & Finanzen 
    - Ökonomie
Sample: https://samples.audible.de/bk/adbl/000974/bk_adbl_000974_sample.mp3
Asin: B004PD8M1M
Title: "Fooling Some of the People All of the Time: A Long Short Story"
Title Short: "Fooling Some of the People All of the Time"
Narrators: L. J. Ganser
Blurb: "A rare look inside the world of activist hedge funds from one of this country's top investors...."
Cover: https://m.media-amazon.com/images/I/412+bjzFudL._SL500_.jpg
Parent Category: Geld & Finanzen
Store Page Url: https://audible.de/pd/B004PD8M1M?ipRedirectOverride=true&overrideBaseCountry=true
Length: 13h 20m 
Summary: "<p>A rare look inside the world of activist hedge funds from one of this country's top investors.</p> <p>In 2002, David Einhorn, the president of Greenlight Capital, gave a speech at a charity investment conference and was asked to share his best investment idea. He described his reasons why Greenlight had sold short the shares of Allied Capital, a leader in the private finance industry. What followed was a firestorm of controversy. </p> <p>Allied responded with a Washington, D.C. style spin-job - attacking Einhorn and disseminating half-truths and outright lies. Undeterred by the spin-job and lies, Greenlight continued its research after the speech and discovered Allied's behavior was far worse than Einhorn ever suspected. <i>Fooling Some of the People All of the Time</i> is the gripping chronicle of this saga, and this edition contains all new updates from the author.</p> <p> Minute by minute, it delves deep inside Wall Street, showing how the $6-billion hedge fund Greenlight Capital conducts its investment research and detailing the maneuvers of an unscrupulous company. Along the way, you'll witness feckless regulators, compromised politicians, and the barricades our capital markets have erected against exposing misconduct from important Wall Street customers. </p> <ul> <li>Goes behind the scenes to detail the truth about investing, short selling, and the politics of business</li> <li>Shows the failings of Wall Street: its investment banks, analysts, journalists, and especially our government regulators</li> <li>Offers insights into the battles surrounding hedge funds</li> <li>Reveals the immense difficulties that prevent the government from sanctioning politically connected companies</li> </ul> <p>At its most basic level, Allied Capital is the story of Wall Street at its worst. But the story is much bigger than one little-known company. <i>Fooling Some of the People All of the Time</i> is an important call for effective law enforcement, free speech, and fair play. </p>"
Child Category: Ökonomie
Web Player: https://www.audible.com/webplayer?asin=B004PD8M1M
Release Date: 2010-02-01
---
# Fooling Some of the People All of the Time: A Long Short Story

## Fooling Some of the People All of the Time

[https://m.media-amazon.com/images/I/412+bjzFudL._SL500_.jpg](https://m.media-amazon.com/images/I/412+bjzFudL._SL500_.jpg)
