---
Search In Goodreads: https://www.goodreads.com/search?q=Viktor%20E.%20Frankl%20-%20Wer%20ein%20Warum%20zu%20leben%20hat
Authors: Viktor E. Frankl
Ratings: 50
Isbn 10: 3407864922
Format: Ungekürztes Hörbuch
Language: German
Publishers: ABP Verlag
Isbn 13: 9783407864925
Tags: 
    - Sozialwissenschaftler & Psychologen
Added: 156
Progress: 11 Std. 25 Min. verbleibend
Categories: 
    - Biografien & Erinnerungen 
    - Akademiker & Spezialisten
Sample: https://samples.audible.de/bk/abpu/000056/bk_abpu_000056_sample.mp3
Asin: 1628612428
Title: "Wer ein Warum zu leben hat: Lebenssinn und Resilienz"
Title Short: "Wer ein Warum zu leben hat"
Narrators: Dominic Kolb
Blurb: "Vom Autor des Bestsellers \"... trotzdem Ja zum Leben sagen\" Viktor E. Frankl. \"Wer ein Warum zu leben hat, erträgt fast jedes Wie\" - nach..."
Cover: https://m.media-amazon.com/images/I/51mwWZIObgL._SL500_.jpg
Parent Category: Biografien & Erinnerungen
Store Page Url: https://audible.de/pd/1628612428?ipRedirectOverride=true&overrideBaseCountry=true
Length: 11h 42m 
Summary: "<p>Vom Autor des Bestsellers \"... trotzdem Ja zum Leben sagen\" Viktor E. Frankl.</p> <p>\"Wer ein Warum zu leben hat, erträgt fast jedes Wie\" - nach diesem Motto erforschte Viktor Frankl, wie Sinnerfüllung auch angesichts schwerer Schicksalsschläge möglich ist und Menschen in die Lage versetzt, in Krisenzeiten seelisch heil zu bleiben.</p> <p>Viktor Frankl war ein österreichischer Neurologe und Psychiater. Während des Zweiten Weltkriegs verbrachte er drei Jahre in Auschwitz, Dachau und anderen Konzentrationslagern.</p> <p>Er begründete die Logotherapie und Existenzanalyse, die vielfach auch als die \"Dritte Wiener Schule der Psychotherapie\" bezeichnet wird.</p> <p>Lebenssinn und Resilienz stehen im Mittelpunkt der von Frankl begründeten Logotherapie, seiner Auseinandersetzung mit Holocaustüberlebenden und seiner kritischen Haltung gegenüber der inneren Leere der modernen Konsumgesellschaft.</p> <p>Dieses Hörbuch lädt ein, den großen Humanisten neu zu entdecken. Seine Texte aus sechs Jahrzehnten zeigen den Brückenschlag, den Frankl zwischen Psychologie und philosophischer Lebenskunst vollzogen hat.</p> <p>In diesem Hörbuch erfahren wir unter anderem über:</p> <p></p> <ul> <li>Leidensfähigkeit und Leidbewältigung,</li> <li>Sinnsuche und Sinnfindung,</li> <li>Die Bedeutung der menschlichen Freiheit,</li> <li>Verborgene innere Kräfte des Menschens.</li> </ul>"
Child Category: Akademiker & Spezialisten
Web Player: https://www.audible.com/webplayer?asin=1628612428
Release Date: 2020-06-12
---
# Wer ein Warum zu leben hat: Lebenssinn und Resilienz

## Wer ein Warum zu leben hat

[https://m.media-amazon.com/images/I/51mwWZIObgL._SL500_.jpg](https://m.media-amazon.com/images/I/51mwWZIObgL._SL500_.jpg)
