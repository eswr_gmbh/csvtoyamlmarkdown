---
Search In Goodreads: https://www.goodreads.com/search?q=Kevin%20Simler%20-%20The%20Elephant%20in%20the%20Brain
Authors: Kevin Simler, Robin Hanson
Ratings: 63
Isbn 10: 0190495995
Format: Ungekürztes Hörbuch
Language: English
Publishers: Tantor Audio
Isbn 13: 9780190495992
Tags: 
    - Seelische & Geistige Gesundheit
    - Sozialwissenschaften
    - Persönlicher Erfolg
Added: 182
Progress: 9 Std. 9 Min. verbleibend
Categories: 
    - Gesundheit & Wellness 
    - Seelische & Geistige Gesundheit
Sample: https://samples.audible.de/bk/tant/012928/bk_tant_012928_sample.mp3
Asin: 1541446216
Title: "The Elephant in the Brain: Hidden Motives in Everyday Life"
Title Short: "The Elephant in the Brain"
Narrators: Jeffrey Kafer
Blurb: "Human beings are primates, and primates are political animals. Our brains, therefore, are designed not just to hunt and gather but also to help us get ahead socially, often via deception and self-deception. But while we may be self-interested schemers, we benefit by pretending otherwise...."
Cover: https://m.media-amazon.com/images/I/41fEeQG1lXL._SL500_.jpg
Parent Category: Gesundheit & Wellness
Store Page Url: https://audible.de/pd/1541446216?ipRedirectOverride=true&overrideBaseCountry=true
Length: 10h 26m 
Summary: "<p>Human beings are primates, and primates are political animals. Our brains, therefore, are designed not just to hunt and gather but also to help us get ahead socially, often via deception and self-deception. But while we may be self-interested schemers, we benefit by pretending otherwise. The less we know about our own ugly motives, the better - and thus, we don't like to talk, or even think, about the extent of our selfishness. This is \"the elephant in the brain\". </p> <p>Such an introspective taboo makes it hard for us to think clearly about our nature and the explanations for our behavior. The aim of this book, then, is to confront our hidden motives directly - to track down the darker, unexamined corners of our psyches and blast them with floodlights. Then, once everything is clearly visible, we can work to better understand ourselves: Why do we laugh? Why are artists sexy? Why do we brag about travel? Why do we prefer to speak rather than listen?</p> <p>Our unconscious motives drive more than just our private behavior; they also infect our venerated social institutions such as art, school, charity, medicine, politics, and religion. In fact, these institutions are in many ways designed to accommodate our hidden motives, to serve covert agendas alongside their \"official\" ones. The existence of big hidden motives can upend the usual political debates, leading one to question the legitimacy of these social institutions, and of standard policies designed to favor or discourage them. You won't see yourself - or the world - the same after confronting the elephant in the brain.</p>"
Child Category: Seelische & Geistige Gesundheit
Web Player: https://www.audible.com/webplayer?asin=1541446216
Release Date: 2018-09-25
---
# The Elephant in the Brain: Hidden Motives in Everyday Life

## The Elephant in the Brain

![https://m.media-amazon.com/images/I/41fEeQG1lXL._SL500_.jpg](https://m.media-amazon.com/images/I/41fEeQG1lXL._SL500_.jpg)



Coholischens
habe verschiedene Namen bei uns, z,B  
	- Intrests group
	- Polistischen Party
	- Guilds, Teams, Traid parnter
	- Friends or Clicks
	- Gangs 
	- or Frakschions


Makaveli, Machiavellisch

Er bespricht die phase, das wir Politische Wesen sind. 
wir suchen also bei Partner nach gewissen skills oder gute DNA
wir suchen auch bei Firma Partner oder Politische freunde mit gleichen Meinungen

wir müssen aber auch unsere werte zeigen um unseren wert zu steigern. 

Also für Höheren status bei Freunde etc, um bei diesen Coholischens besseren erfolg zu haben.
Also endlich wie denn Kampf von den Pflanzen, die um das licht kämpfen
kämpfen wir um die liebe und Aufmerksamkeit unsere freunde / Mitmenschen.
Also einfach höher stehen, als die anderen Pflanzen, aber dann werden wir von allen gesehen.

Judge freely but expect you also get been jugged.
...

Signals 
Display informations 
z.b Schöne haut, das wir Gesund und gute DNA haben

Die wahren Singale sind die teuern.. Handycap konzept
z.B Krasse Farben bei den Tieren zum herausstechen, heisst auch das man eher zum Opfer wird.
Jeans or Anzug Hosen. Jeans sind Stabil und müssen nicht jeden Tag gewaschen werden. Dagegen die Anzug Hosen sind super extensiv und einfach zerbrechlich, sprich sie zeigen Disziplin oder mehr. 
Actions > Word

Word sind to Cheap!

Ware Freunde können dich zum Beispiel im Spital besuchen und zeigen damit ihre Loyalität.
Gute DNA Partner zeigen dies z.B in dem sie in das Gym gehen
in Gangs zeigen sie dies über die Tattoo's.

Nicht immer sind wir die Singale bewusst, die wir ausstossen oder ansehen. z.B Kunst ist ein Zeichen von Reichtum und Freiheit nicht jeden tag Jagen zu gehen.


Value as Singnal
Counter Signaling
z.B Bekannt oder eher distanzierete Freunde und nahe freunde. Nahe freunde Pisaken einem vielleicht, bisschen unfreundlich zu sein. Um zu zeigen, wie nahe wir sind. Wären entfernte freunde eher versuchen lieb zu sein und kleine Sachen versuchen zu merken.










