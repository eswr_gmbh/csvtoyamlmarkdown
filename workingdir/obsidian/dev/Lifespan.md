---
Search In Goodreads: https://www.goodreads.com/search?q=David%20A.%20Sinclair%20PhD%20-%20Lifespan
Authors: David A. Sinclair PhD, Matthew D. LaPlante
Ratings: 488
Isbn 10: 1501191977
Format: Ungekürztes Hörbuch
Language: English
Publishers: Simon & Schuster Audio
Isbn 13: 9781501191978
Tags: 
    - Gesundheit & Wellness
    - Persönliche Entwicklung
    - Wissenschaft
Added: 181
Categories: 
    - Beziehungen
    - Elternschaft & persönliche Entwicklung 
    - Persönliche Entwicklung
Sample: https://samples.audible.de/bk/sans/009479/bk_sans_009479_sample.mp3
Asin: 1508296057
Title: "Lifespan: Why We Age - and Why We Don't Have To"
Title Short: "Lifespan"
Narrators: David A. Sinclair PhD
Blurb: "This paradigm-shifting audiobook shows how almost everything we think we know about aging is wrong, offers a front-row seat to the amazing global effort to slow, stop, and reverse aging, and calls listeners to consider a future where aging can be treated...."
Cover: https://m.media-amazon.com/images/I/51bb7yJqDNL._SL500_.jpg
Parent Category: Beziehungen, Elternschaft & persönliche Entwicklung
Store Page Url: https://audible.de/pd/1508296057?ipRedirectOverride=true&overrideBaseCountry=true
Length: 11 Std. 55 Min.
Summary: "<p><b>Audio bonus! Includes exclusive conversations with the authors!</b></p> <p><b>From an acclaimed Harvard professor and one of </b><b><i>Time</i></b><b>’s most influential people, this paradigm-shifting audiobook shows how almost everything we think we know about aging is wrong, offers a front-row seat to the amazing global effort to slow, stop, and reverse aging, and calls listeners to consider a future where aging can be treated.</b></p> <p>For decades, experts have believed that we are at the mercy of our genes and that natural damage to our genes - the kind that inevitably happens as we get older - makes us become sick and grow old.</p> <p>But what if everything you think you know about aging is wrong? What if aging is a disease - and that disease is treatable?</p> <p>In <i>Lifespan</i>, one of the world’s foremost experts on aging and genetics reveals a groundbreaking new theory that will forever change the way we think about why we age and what we can do about it. Aging isn’t immutable; we can have far more control over it than we realize. This eye-opening and provocative work takes us to the front lines of research that is pushing the boundaries on our perceived scientific limitations, revealing incredible breakthroughs - many from Dr. David Sinclair’s own lab - that demonstrate how we can slow down, or even reverse<i>, </i>the genetic clock. The key is activating newly discovered vitality genes - the decedents of an ancient survival circuit that is both the cause of aging and the key to reversing it. Dr. Sinclair shares the emerging technologies and simple lifestyle changes - such as intermittent fasting, cold exposure, and exercising with the right intensity - that have been shown to help lead to longer lives.</p> <p><i>Lifespan </i>provides a road map for taking charge of our own health destiny and a bold new vision for the future when humankind is able to live to be 100 years young.</p> <p><b>PLEASE NOTE: When you purchase this title, the accompanying PDF will be available in your Audible Library along with the audio.</b></p>"
Child Category: Persönliche Entwicklung
Web Player: https://www.audible.com/webplayer?asin=1508296057
Release Date: 2019-09-10
---
# Lifespan: Why We Age - and Why We Don't Have To

## Lifespan

[https://m.media-amazon.com/images/I/51bb7yJqDNL._SL500_.jpg](https://m.media-amazon.com/images/I/51bb7yJqDNL._SL500_.jpg)
