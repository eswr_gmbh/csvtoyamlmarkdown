---
Search In Goodreads: https://www.goodreads.com/search?q=Yuval%20Noah%20Harari%20-%20Eine%20kurze%20Geschichte%20der%20Menschheit
Authors: Yuval Noah Harari
Ratings: 15117
Isbn 10: 3570552691
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Der Hörverlag
Isbn 13: 9783570552698
Tags: 
    - Wissenschaft
Added: 37
Progress: Beendet
Categories: 
    - Wissenschaft & Technik 
    - Wissenschaft
Sample: https://samples.audible.de/bk/hoer/001177/bk_hoer_001177_sample.mp3
Asin: B00EY81Z80
Title: "Eine kurze Geschichte der Menschheit"
Title Short: "Eine kurze Geschichte der Menschheit"
Narrators: Jürgen Holdorf
Blurb: "Vor 100.000 Jahren war der Homo sapiens noch ein unbedeutendes Tier, das unauffällig in einem abgelegenen Winkel des afrikanischen Kontinents..."
Cover: https://m.media-amazon.com/images/I/51zgvACKy+L._SL500_.jpg
Parent Category: Wissenschaft & Technik
Store Page Url: https://audible.de/pd/B00EY81Z80?ipRedirectOverride=true&overrideBaseCountry=true
Length: 17h 2m 
Summary: "<p>Vor 100.000 Jahren war der Homo sapiens noch ein unbedeutendes Tier, das unauffällig in einem abgelegenen Winkel des afrikanischen Kontinents lebte. Unsere Vorfahren teilten sich den Planeten mit mindestens fünf weiteren menschlichen Spezies und die Rolle, die sie im Ökosystem spielten, war nicht größer als die von Gorillas, Libellen oder Quallen.  </p> <p>Vor 70.000 Jahren dann vollzog sich ein mysteriöser und rascher Wandel mit dem Homo sapiens, und es war vor allem die Beschaffenheit seines Gehirns, die ihn zum Herrn des Planeten und zum Schrecken des Ökosystems werden ließ. </p>"
Child Category: Wissenschaft
Web Player: https://www.audible.com/webplayer?asin=B00EY81Z80
Release Date: 2013-09-09
---
# Eine kurze Geschichte der Menschheit

## Eine kurze Geschichte der Menschheit

[https://m.media-amazon.com/images/I/51zgvACKy+L._SL500_.jpg](https://m.media-amazon.com/images/I/51zgvACKy+L._SL500_.jpg)
