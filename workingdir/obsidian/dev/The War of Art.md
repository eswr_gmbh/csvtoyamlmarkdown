---
Search In Goodreads: https://www.goodreads.com/search?q=Steven%20Pressfield%20-%20The%20War%20of%20Art
Authors: Steven Pressfield
Ratings: 567
Isbn 10: 1936891026
Format: Ungekürztes Hörbuch
My Rating: 5
Language: English
Publishers: Black Irish Entertainment LLC
Isbn 13: 9781936891023
Tags: 
    - Kreativität
    - Persönlicher Erfolg
Added: 68
Progress: Beendet
Categories: 
    - Beziehungen
    - Elternschaft & persönliche Entwicklung 
    - Persönliche Entwicklung
Sample: https://samples.audible.de/bk/acx0/145714/bk_acx0_145714_sample.mp3
Asin: B07PQ9G9ST
Title: "The War of Art"
Title Short: "The War of Art"
Narrators: Steven Pressfield
Blurb: "Think of The War of Art as tough love...for yourself. Since 2002, The War of Art has inspired people around the world to defeat \"resistance\"; to recognize and knock down dream-blocking barriers and to silence the naysayers within us...."
Cover: https://m.media-amazon.com/images/I/51HxOQqzriL._SL500_.jpg
Parent Category: Beziehungen, Elternschaft & persönliche Entwicklung
Store Page Url: https://audible.de/pd/B07PQ9G9ST?ipRedirectOverride=true&overrideBaseCountry=true
Length: 2h 29m 
Summary: "<p>Think of <i>The War of Art </i>as tough love...for yourself.</p> <p>Since 2002, <i>The War of Art</i> has inspired people around the world to defeat \"resistance\"; to recognize and knock down dream-blocking barriers and to silence the naysayers within us. Resistance kicks everyone's butt, and the desire to defeat it is equally as universal. <i>The War of Art</i> identifies the enemy that every one of us must face, outlines a battle plan to conquer this internal foe, then pinpoints just how to achieve the greatest success. </p> <p>Though it was written for writers, it has been embraced by business entrepreneurs, actors, dancers, painters, photographers, filmmakers, military service members, and thousands of others around the world.</p>"
Child Category: Persönliche Entwicklung
Web Player: https://www.audible.com/webplayer?asin=B07PQ9G9ST
Release Date: 2019-04-04
---
# The War of Art

## The War of Art

[https://m.media-amazon.com/images/I/51HxOQqzriL._SL500_.jpg](https://m.media-amazon.com/images/I/51HxOQqzriL._SL500_.jpg)
