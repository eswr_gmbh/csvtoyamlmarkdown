---
Search In Goodreads: https://www.goodreads.com/search?q=John%20E.%20Siers%20-%20The%20Moon%20and%20Beyond
Authors: John E. Siers
Ratings: 3
Format: Ungekürztes Hörbuch
My Rating: 4
Language: English
Publishers: Theogony Books
Book Numbers: 1
Tags: 
    - Abenteuer
    - Space Opera
Added: 190
Progress: Beendet
Categories: 
    - Science Fiction & Fantasy 
    - Science Fiction
Sample: https://samples.audible.de/bk/acx0/287585/bk_acx0_287585_sample.mp3
Asin: B09ML51HSH
Title: "The Moon and Beyond: The Lunar Free State, Book 1"
Title Short: "The Moon and Beyond"
Narrators: Jimmy Moreland
Blurb: "Ian Stevens, director of the Deep Space Research Institute, isn’t interested in conducting research just for the sake of research. While the rest of the world - including the US Government - thinks that’s the DSRI’s sole mission, Stevens is actually interested in more...."
Series: The Lunar Free State (book 1)
Cover: https://m.media-amazon.com/images/I/51rRnrELX5L._SL500_.jpg
Parent Category: Science Fiction & Fantasy
Store Page Url: https://audible.de/pd/B09ML51HSH?ipRedirectOverride=true&overrideBaseCountry=true
Length: 14h 28m 
Summary: "<p>Ian Stevens, director of the Deep Space Research Institute, isn’t interested in conducting research just for the sake of research. While the rest of the world - including the US Government - thinks that’s the DSRI’s sole mission, Stevens is actually interested in more - he wants manned space travel and the colonization of the moon. With NASA defunct, few scientific probes are being launched, and no one even talks about human spaceflight anymore. Stevens is out to change that. A billionaire in his own right, Stevens has assembled a crew of the best scientists and engineers he can find, and the DSRI is secretly building and testing gravity-powered spacecraft.</p> <p>There’s just one problem - the project would be widely condemned worldwide, especially since there’s no government supervision. Everything they do has to be kept out of sight and away from the prying eyes of the NSA, Homeland Security, the IRS, OSHA, and a half-dozen other government agencies. They’ve got the world's smartest computer on their side, but will it be enough?</p> <p>As the government gets ever closer to figuring out what the DSRI is doing, Stevens’ plan to establish a permanent settlement on the Moon nears fruition. The race is on - will the government find out and shut down the project, or will Stevens be able to implement...the Lunar Free State?</p>"
Child Category: Science Fiction
Web Player: https://www.audible.com/webplayer?asin=B09ML51HSH
Release Date: 2021-11-29
---
