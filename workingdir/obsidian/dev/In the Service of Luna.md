---
Search In Goodreads: https://www.goodreads.com/search?q=John%20E.%20Siers%20-%20In%20the%20Service%20of%20Luna
Authors: John E. Siers
Ratings: 1
Format: Ungekürztes Hörbuch
Language: English
Publishers: Theogony Books
Book Numbers: 4
Tags: 
    - Belletristik
    - Abenteuer
    - Erstkontakt
Added: 195
Progress: 1 Std. 36 Min. verbleibend
Categories: 
    - Literatur & Belletristik 
    - Belletristik
Sample: https://samples.audible.de/bk/acx0/307498/bk_acx0_307498_sample.mp3
Asin: B09ZKLVQHB
Title: "In the Service of Luna: The Lunar Free State, Book 4"
Title Short: "In the Service of Luna"
Narrators: Jimmy Moreland
Blurb: "On-the-job training can be hazardous to your health...Lorna Greenwood hopes the Lunar Free State’s Fleet Academy will prepare her to follow in her grandmother’s legendary footsteps...."
Series: The Lunar Free State (book 4)
Cover: https://m.media-amazon.com/images/I/51CK8vPPkzL._SL500_.jpg
Parent Category: Literatur & Belletristik
Store Page Url: https://audible.de/pd/B09ZKLVQHB?ipRedirectOverride=true&overrideBaseCountry=true
Length: 15h 2m 
Summary: "<p><b>On-the-job training can be hazardous to your health...</b></p> <p>Lorna Greenwood hopes the Lunar Free State’s Fleet Academy will prepare her to follow in her grandmother’s legendary footsteps. Her roommate Nova Sakura has already spent time in the LFS Marines and wants to move up to the officer ranks. First, though, they have to survive four years of training. Military service gets real for Lorna when the assault carrier Omaha Beach comes under fire during her first year training cruise. Nova’s first year cruise aboard the light cruiser Hydra seems pretty routine, until she’s called upon to take charge of a squad of Marines boarding a ship full of murderous pirates.</p> <p>And nothing prepares them for the challenges they face after graduation, when they’re both assigned to the same ship and find themselves facing an alien enemy in a distant star system, in what could be the start of another interstellar war. Two young women put their lives on the line in the service of Luna...even if that means leaving family, friends, and their hearts behind.</p>"
Child Category: Belletristik
Web Player: https://www.audible.com/webplayer?asin=B09ZKLVQHB
Release Date: 2022-05-05
---
