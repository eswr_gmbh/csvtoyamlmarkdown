---
Search In Goodreads: https://www.goodreads.com/search?q=W.%20Chan%20Kim%20-%20Der%20Blaue%20Ozean%20als%20Strategie
Authors: W. Chan Kim, Renee Mauborgne
Ratings: 100
Isbn 10: 3446450475
Format: Ungekürztes Hörbuch
Language: German
Publishers: ABP Verlag
Isbn 13: 9783446450479
Tags: 
    - Management & Leadership
Added: 104
Progress: 57 Min. verbleibend
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/abpu/000013/bk_abpu_000013_sample.mp3
Asin: 1628610956
Title: "Der Blaue Ozean als Strategie: Wie man neue Märkte schafft wo es keine Konkurrenz gibt"
Title Short: "Der Blaue Ozean als Strategie"
Narrators: Olaf Renoldi
Blurb: "Der Wall-Street-Journal-Bestseller! Über 3,5 Millionen verkaufte Exemplare weltweit. cDieses Buch ist ein globales Phänomen. Es wurde in 43..."
Cover: https://m.media-amazon.com/images/I/61ifmFOSp0L._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/1628610956?ipRedirectOverride=true&overrideBaseCountry=true
Length: 9h 16m 
Summary: "<p>Der Wall-Street-Journal-Bestseller! Über 3,5 Millionen verkaufte Exemplare weltweit. cDieses Buch ist ein globales Phänomen. Es wurde in 43 Sprachen publiziert und ist auf 5 Kontinenten zum Bestseller geworden.</p> <p>Die meisten Unternehmen liefern sich erbitterte Kämpfe mit ihren Konkurrenten - um Kunden, Wettbewerbsvorteile und Marktanteile. Und trotzdem wird es für sie immer schwieriger, nachhaltig profitables Wachstum zu erreichen.</p> <p>Der bahnbrechende Bestseller von W. Chan Kim und Renée Mauborgne stellt das bisherige strategische Denken auf den Kopf: Wer sich auf den Kampf mit seinen Konkurrenten einlässt, hat schon verloren. Wer dagegen ganz neue Märkte schafft, wo es keine Konkurrenz gibt, kann satte Gewinne einfahren und seine Wettbewerber getrost vergessen.</p> <p>\"Der Blaue Ozean als Strategie\" präsentiert einen systematischen Ansatz, wie man Konkurrenz irrelevant macht, und legt Prinzipien und Methoden vor, mit der jede Organisation ihre eigenen \"Blauen Ozeane\" erobern kann.</p> <p>Dieses Hörbuch zeigt:</p> <p></p> <ul> <li>wie man die Grenzen bestehender Märkte kraftvoll erweitert;</li> <li>wie man ganz neue Märkte erschließt;  </li> <li>wie man bisher unerfüllte Kundenbedürfnisse erkennt und befriedigt. </li> </ul> <p>Wer das kann, bestimmt selbst die Regeln des Spiels, statt sich ihnen zu unterwerfen und entwirft einen kühnen neuen Weg in die Zukunft.</p> <p>W. Chan Kim und Renée Mauborgne sind Professoren für Strategie am INSEAD sowie Co-Direktoren des INSEAD Blue Ocean Strategy Institute. Sie sind Mitglieder des Weltwirtschaftsforums und Gründer des Blue Ocean Global Network.</p> <p>Im englischen Original erschien unter dem Titel \"Blue Ocean Strategy, Expanded Edition. How to Create Uncontested Market Space and Make the Competition Irrelevant\".</p>"
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=1628610956
Release Date: 2019-08-22
---
# Der Blaue Ozean als Strategie: Wie man neue Märkte schafft wo es keine Konkurrenz gibt

## Der Blaue Ozean als Strategie

[https://m.media-amazon.com/images/I/61ifmFOSp0L._SL500_.jpg](https://m.media-amazon.com/images/I/61ifmFOSp0L._SL500_.jpg)
