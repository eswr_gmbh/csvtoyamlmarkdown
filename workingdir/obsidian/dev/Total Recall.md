---
Search In Goodreads: https://www.goodreads.com/search?q=Arnold%20Schwarzenegger%20-%20Total%20Recall
Authors: Arnold Schwarzenegger
Ratings: 1336
Isbn 10: 1451662440
Format: Ungekürztes Hörbuch
My Rating: 4
Language: English
Publishers: Simon & Schuster Audio
Isbn 13: 9781451662443
Tags: 
    - Unterhaltung & Stars
    - Biografien & Erinnerungen
    - Sport
Added: 25
Progress: Beendet
Categories: 
    - Biografien & Erinnerungen 
    - Unterhaltung & Stars
Sample: https://samples.audible.de/bk/sans/006163/bk_sans_006163_sample.mp3
Asin: B009GL5NHY
Title: "Total Recall: My Unbelievably True Life Story"
Title Short: "Total Recall"
Narrators: Stephen Lang, Arnold Schwarzenegger
Blurb: "One of the most anticipated autobiographies of this generation, Arnold Schwarzenegger's Total Recall is the candid story of one of the world's most remarkable world leaders...."
Cover: https://m.media-amazon.com/images/I/51qvq8tJUuL._SL500_.jpg
Parent Category: Biografien & Erinnerungen
Store Page Url: https://audible.de/pd/B009GL5NHY?ipRedirectOverride=true&overrideBaseCountry=true
Length: 23h 21m 
Summary: "<p><b>One of the most anticipated autobiographies of this generation, Arnold Schwarzenegger's <i>Total Recall</i> is the candid story of one of the world's most remarkable actors, businessmen, and world leaders. </b> </p> <p>Born in the small city of Thal, Austria, in 1947, Arnold Schwarzenegger moved to Los Angeles at the age of 21. Within 10 years, he was a millionaire businessman. After 20 years, he was the world's biggest movie star. In 2003, he was elected governor of California and became a household name around the world. </p> <p>Chronicling his embodiment of the American Dream, <i>Total Recall</i> covers Schwarzenegger's high-stakes journey to the United States, from creating the international bodybuilding industry out of the sands of Venice Beach, to breathing life into cinema's most iconic characters, and becoming one of the leading political figures of our time. Proud of his accomplishments and honest about his regrets, Schwarzenegger spares nothing in sharing his amazing story.</p>"
Child Category: Unterhaltung & Stars
Web Player: https://www.audible.com/webplayer?asin=B009GL5NHY
Release Date: 2012-10-01
---
# Total Recall: My Unbelievably True Life Story

## Total Recall

[https://m.media-amazon.com/images/I/51qvq8tJUuL._SL500_.jpg](https://m.media-amazon.com/images/I/51qvq8tJUuL._SL500_.jpg)
