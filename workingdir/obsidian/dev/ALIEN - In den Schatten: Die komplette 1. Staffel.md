---
Search In Goodreads: https://www.goodreads.com/search?q=Tim%20Lebbon%20-%20ALIEN%20-%20In%20den%20Schatten%3A%20Die%20komplette%201.%20Staffel
Authors: Tim Lebbon, Dirk Maggs
Ratings: 8530
Format: Hörspiel
My Rating: 4
Language: German
Publishers: Audible Originals
Book Numbers: 1.1 - 1.10
Tags: 
    - Erstkontakt
Added: 87
Progress: Beendet
Categories: 
    - Science Fiction & Fantasy 
    - Science Fiction
Sample: https://samples.audible.de/bk/adop/300061/bk_adop_300061_sample.mp3
Asin: B01L21WTBI
Title: "ALIEN - In den Schatten: Die komplette 1. Staffel"
Title Short: "ALIEN - In den Schatten: Die komplette 1. Staffel"
Narrators: Karin Buchholz, Dietmar Wunder, Michael Iwannek, Ann Vielhaben, Bernd Vollbrecht, David Nathan
Blurb: "Der Raumfrachter MARION soll das wertvolle Mineral Trimonit vom Planeten LV178 zur Erde bringen. Doch bei den Schürfungen unter Tage stößt die Crew..."
Series: ALIEN (book 1.1 - 1.10)
Cover: https://m.media-amazon.com/images/I/41XuEwBkytL._SL500_.jpg
Parent Category: Science Fiction & Fantasy
Store Page Url: https://audible.de/pd/B01L21WTBI?ipRedirectOverride=true&overrideBaseCountry=true
Length: 4h 29m 
Summary: "Der Raumfrachter MARION soll das wertvolle Mineral Trimonit vom Planeten LV178 zur Erde bringen. Doch bei den Schürfungen unter Tage stößt die Crew auf unbekannte Wesen, die tief in den Minen geschlummert haben - und die Hölle bricht los. <br> <br>Als die Arbeiter panisch die Flucht ergreifen, kommt es zur Katastrophe: Beim Anflug auf das Mutterschiff rast eine der Transportfähren ungebremst in die MARION und das Minenraumschiff kann nicht mehr gesteuert werden. Aber damit nicht genug: Zwar kann die zweite Transportkapsel an der MARION andocken, doch auch für ihre Besatzung kommt jede Hilfe zu spät, denn die gefräßigen Monster haben es mit an Bord geschafft. <br> <br>Chefmechaniker Chris Hooper und die restlichen Überlebenden der MARION sind der Verzweiflung nahe, als sie der Notruf eines Rettungsshuttles erreicht: An Bord befindet sich Ellen Ripley - die einzige Überlebende der NOSTROMO, und somit die einzige Person, die den Klauen der Aliens jemals entkommen ist... <br> <br>In deiner Audible-Bibliothek findest du für dieses Hörerlebnis eine PDF-Datei mit zusätzlichem Material.<p>>> Diese Hörspiel-Serie genießt du exklusiv nur bei Audible.</p>"
Child Category: Science Fiction
Web Player: https://www.audible.com/webplayer?asin=B01L21WTBI
Release Date: 2016-09-22
---
# ALIEN - In den Schatten: Die komplette 1. Staffel

## ALIEN - In den Schatten: Die komplette 1. Staffel

### ALIEN (book 1.1 - 1.10)

[https://m.media-amazon.com/images/I/41XuEwBkytL._SL500_.jpg](https://m.media-amazon.com/images/I/41XuEwBkytL._SL500_.jpg)
