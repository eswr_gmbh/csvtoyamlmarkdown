---
Search In Goodreads: https://www.goodreads.com/search?q=Andy%20Weir%20-%20Project%20Hail%20Mary
Authors: Andy Weir
Ratings: 3717
Isbn 10: 0593135210
Format: Ungekürztes Hörbuch
My Rating: 5
Language: English
Publishers: Audible Studios
Isbn 13: 9780593135211
Tags: 
    - Abenteuer
    - Naturwissenschaften
    - Space Opera
Added: 184
Categories: 
    - Science Fiction & Fantasy 
    - Science Fiction
Sample: https://samples.audible.de/bk/adbl/051843/bk_adbl_051843_sample.mp3
Asin: B08G9RZBTT
Title: "Project Hail Mary"
Title Short: "Project Hail Mary"
Narrators: Ray Porter
Blurb: "Ryland Grace is the sole survivor on a desperate, last-chance mission - and if he fails, humanity and the Earth itself will perish. Except that right now, he doesn't know that. He can't even remember his own name, let alone the nature of his assignment or how to complete it...."
Cover: https://m.media-amazon.com/images/I/51b6fvQr1-L._SL500_.jpg
Parent Category: Science Fiction & Fantasy
Store Page Url: https://audible.de/pd/B08G9RZBTT?ipRedirectOverride=true&overrideBaseCountry=true
Length: 16 Std. 10 Min.
Summary: "<p><b><i>Winner of the 2022 Audie Awards' Audiobook of the Year</i></b></p> <p><b><i>Number-One Audible and </i></b><b>New York Times</b><b><i> Audio Best Seller</i></b></p> <p><b><i>More than one million audiobooks sold</i></b></p> <p><b>A lone astronaut must save the earth from disaster in this incredible new science-based thriller from the number-one </b><b><i>New York Times</i></b><b> best-selling author of </b><b><i>The Martian</i></b><b>.</b></p> <p>Ryland Grace is the sole survivor on a desperate, last-chance mission - and if he fails, humanity and the Earth itself will perish.</p> <p>Except that right now, he doesn't know that. He can't even remember his own name, let alone the nature of his assignment or how to complete it.</p> <p>All he knows is that he's been asleep for a very, very long time. And he's just been awakened to find himself millions of miles from home, with nothing but two corpses for company.</p> <p>His crewmates dead, his memories fuzzily returning, he realizes that an impossible task now confronts him. Alone on this tiny ship that's been cobbled together by every government and space agency on the planet and hurled into the depths of space, it's up to him to conquer an extinction-level threat to our species.</p> <p>And thanks to an unexpected ally, he just might have a chance.</p> <p>Part scientific mystery, part dazzling interstellar journey, <i>Project Hail Mary</i> is a tale of discovery, speculation, and survival to rival <i>The Martian</i> - while taking us to places it never dreamed of going.</p> <p>PLEASE NOTE: To accommodate this audio edition, some changes to the original text have been made with the approval of author Andy Weir.</p>"
Child Category: Science Fiction
Web Player: https://www.audible.com/webplayer?asin=B08G9RZBTT
Release Date: 2021-05-04
---
# Project Hail Mary

## Project Hail Mary

![https://m.media-amazon.com/images/I/51b6fvQr1-L._SL500_.jpg|200](https://m.media-amazon.com/images/I/51b6fvQr1-L._SL500_.jpg)


## Handlung
in diesem Buch geht es um eine Person. Ryland Grace. Wie er als Lehrer zu einem Weltretter wird. 


![Rocky|500](https://pbs.twimg.com/media/FAiLUkIVUAcB00a.jpg:large)

Personen
-   **Ryland Grace** – The novel's protagonist, a disillusioned molecular biologist who becomes a junior high school science teacher before being recruited to study Astrophage by Eva Stratt.
-   **Eva Stratt** – A [Dutch](https://en.wikipedia.org/wiki/Netherlands "Netherlands") woman who works for the [UN](https://en.wikipedia.org/wiki/United_Nations "United Nations"), who is subsequently given absolute authority to stop Astrophage, leading to the _Hail Mary_ mission.
-   **Rocky** – An alien engineer from the [40 Eridani](https://en.wikipedia.org/wiki/40_Eridani "40 Eridani") system whose planet is simultaneously threatened by Astrophage. Due to his proximity to radiation-shielding Astrophage fuel, he survives space radiation while his crew does not. Rocky's vessel eventually encounters the _Hail Mary_ and works together with Grace.
-   **Yáo Li-Jie** – The intended commander of _Hail Mary_'s crew. He dies en route to Tau Ceti.
-   **Olesya Ilyukhina** – The intended engineer and EVA specialist of _Hail Mary_’s crew, ribald yet cheerful. She dies en route to Tau Ceti.
-   **Dr. Lokken** – A [Norwegian](https://en.wikipedia.org/wiki/Norway "Norway") scientist who assists in the design of the _Hail Mary_ and has a short rivalry with Grace over a paper he wrote.
-   **Dimitri Komorov** – A Russian scientist who develops the Astrophage-based propulsion system for the _Hail Mary_ and discovered its mass-conversion properties.
-   **Steve Hatch** – A researcher from the [University of British Columbia](https://en.wikipedia.org/wiki/University_of_British_Columbia "University of British Columbia"). He develops the "Beetle" probes and is an avid Beatle fan. He is described as very talkative and optimistic.
-   **Martin DuBois** – An American man and original science advisor on the _Hail Mary_ mission. He dies in an explosion nine days before launch. He is described as honest and gregarious.
-   **Annie Shapiro** – The original backup science advisor on the _Hail Mary_ mission. She dies with Martin in the same explosion.
-   **Robert Redell** – A solar energy expert from [New Zealand](https://en.wikipedia.org/wiki/New_Zealand "New Zealand"). Arrested for embezzlement and the death of seven technicians in a testing accident, he develops a method to breed Astrophage rapidly.
-   **François Leclerc** – A French climatologist who helps to slow down the climate changes caused by the Astrophage stealing the sun's energy through the development of a method releasing [trapped methane embedded in Antarctic ice sheets](https://en.wikipedia.org/wiki/Arctic_methane_emissions#Ice_sheets "Arctic methane emissions") into the atmosphere through the use of [fusion bombs](https://en.wikipedia.org/wiki/Fusion_bomb "Fusion bomb").

