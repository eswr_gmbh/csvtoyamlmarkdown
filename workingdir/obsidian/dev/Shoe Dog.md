---
Search In Goodreads: https://www.goodreads.com/search?q=Phil%20Knight%20-%20Shoe%20Dog
Authors: Phil Knight
Ratings: 2039
Isbn 10: 3862489272
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: FinanzBuch Verlag
Isbn 13: 9783862489275
Tags: 
    - Wirtschaft
Added: 69
Progress: Beendet
Categories: 
    - Biografien & Erinnerungen 
    - Akademiker & Spezialisten
Sample: https://samples.audible.de/bk/riva/000570/bk_riva_000570_sample.mp3
Asin: 3960923945
Title: "Shoe Dog: Die offizielle Biografie des NIKE-Gründers"
Title Short: "Shoe Dog"
Narrators: Stefan Lehnen
Blurb: "Als junger, abenteuerlustiger Business-School-Absolvent auf der Suche nach einer Herausforderung lieh Phil Knight sich von seinem..."
Cover: https://m.media-amazon.com/images/I/412IPF8hewL._SL500_.jpg
Parent Category: Biografien & Erinnerungen
Store Page Url: https://audible.de/pd/3960923945?ipRedirectOverride=true&overrideBaseCountry=true
Length: 15h 56m 
Summary: "<p>Als junger, abenteuerlustiger Business-School-Absolvent auf der Suche nach einer Herausforderung lieh Phil Knight sich von seinem Vater 50 Dollar und gründete eine Firma mit einer klaren Mission: qualitativ hochwertige, aber preiswerte Laufschuhe aus Japan importieren. In jenem ersten Jahr, 1963, verkaufte Knight Laufschuhe aus dem Kofferraum seines Plymouth Valiant heraus und erzielte einen Umsatz von 8000 Dollar. Heute liegen die Jahresumsätze von Nike bei über 30 Milliarden Dollar. In unserem Zeitalter der Start-ups hat sich Knights Firma Nike als Maßstab aller Dinge etabliert und sein \"Swoosh\" ist längst mehr als nur ein Logo. Es ist ein Symbol von Geschmeidigkeit und Größe, eines der wenigen Icons, die in jedem Winkel unseres Erdballs sofort wiedererkannt werden.</p> <p>Aber Knight selbst, der Mann hinter dem Swoosh, ist immer ein Geheimnis geblieben. Jetzt erzählt er endlich seine Geschichte. Seine Memoiren sind überraschend, bescheiden, ungeschönt, humorvoll und handwerklich meisterhaft.</p> <p>Den Anfang markiert eine klassische Situation am Scheideweg. Der 24-jährige Knight bereist als Rucksacktourist Asien, Europa und Afrika, ihn bewegen die ganz großen philosophischen Fragen des Lebens. Und er entscheidet sich für einen unkonventionellen Lebensweg. Anstatt für ein großes etabliertes Unternehmen zu arbeiten, beschließt er, etwas ganz Eigenes zu schaffen – etwas, das neu, dynamisch und anders ist.</p> <p>En détail beschreibt Knight die vielen unberechenbaren Risiken, mit denen er sich auf seinem Weg konfrontiert sah, die niederschmetternden Rückschläge, die skrupellosen Konkurrenten, die zahllosen Zweifler und Widersacher, die abweisenden Banker, die etlichen Male, wo er knapp einer Katastrophe entging, ebenso wie seine vielen triumphalen Erfolge.</p>"
Child Category: Akademiker & Spezialisten
Web Player: https://www.audible.com/webplayer?asin=3960923945
Release Date: 2019-02-28
---
# Shoe Dog: Die offizielle Biografie des NIKE-Gründers

## Shoe Dog

[https://m.media-amazon.com/images/I/412IPF8hewL._SL500_.jpg](https://m.media-amazon.com/images/I/412IPF8hewL._SL500_.jpg)
