---
Search In Goodreads: https://www.goodreads.com/search?q=Garry%20Kasparov%20-%20How%20Life%20Imitates%20Chess
Authors: Garry Kasparov
Ratings: 21
Isbn 10: 1596918276
Format: Ungekürztes Hörbuch
My Rating: 5
Language: English
Publishers: Macmillan Audio
Isbn 13: 9781596918276
Tags: 
    - Management & Leadership
    - Erfolg im Beruf
Added: 107
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/aren/000741/bk_aren_000741_sample.mp3
Asin: B004UVZQ0O
Title: "How Life Imitates Chess: Making the Right Moves, from the Board to the Boardroom"
Title Short: "How Life Imitates Chess"
Narrators: Garry Kasparov, Adam Grupper
Blurb: "How Life Imitates Chess is a primer on how to think, make decisions, prepare strategies, and anticipate the future...."
Cover: https://m.media-amazon.com/images/I/51eDvYENupL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B004UVZQ0O?ipRedirectOverride=true&overrideBaseCountry=true
Length: 6h 41m 
Summary: "<i>How Life Imitates Chess</i> is a primer on how to think, make decisions, prepare strategies, and anticipate the future. Kasparov has distilled the lessons he learned over a lifetime as a chess grandmaster to cover the practical side - tactics, strategy, preparation, as well as the subtler, more human arts of using memory, intuition, and imagination. It's a remarkably honest audiobook in which Kasparov, one of the world's most celebrated and successful competitors, details both his blunders and his victories, always with the intent to enable readers to absorb his lessons and do better for themselves."
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=B004UVZQ0O
Release Date: 2007-10-16
---
# How Life Imitates Chess: Making the Right Moves, from the Board to the Boardroom

## How Life Imitates Chess

[https://m.media-amazon.com/images/I/51eDvYENupL._SL500_.jpg](https://m.media-amazon.com/images/I/51eDvYENupL._SL500_.jpg)
