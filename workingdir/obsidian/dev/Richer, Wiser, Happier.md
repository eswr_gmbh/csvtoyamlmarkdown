---
Search In Goodreads: https://www.goodreads.com/search?q=William%20Green%20-%20Richer%2C%20Wiser%2C%20Happier
Authors: William Green
Ratings: 110
Isbn 10: 1501164856
Format: Ungekürztes Hörbuch
Language: English
Publishers: Simon & Schuster Audio
Isbn 13: 9781501164859
Tags: 
    - Erfolg im Beruf
    - Geld & Finanzen
    - Persönliche Finanzen
Added: 153
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Erfolg im Beruf
Sample: https://samples.audible.de/bk/sans/010357/bk_sans_010357_sample.mp3
Asin: 179710232X
Title: "Richer, Wiser, Happier: How the World's Greatest Investors Win in Markets and Life"
Title Short: "Richer, Wiser, Happier"
Narrators: Raphael Corkhill
Blurb: "From a renowned financial journalist comes a fresh and unexpectedly profound book that draws on hundreds of hours of exclusive interviews with many of the world’s super-investors to demonstrate that the keys for building wealth hold other life lessons as well...."
Cover: https://m.media-amazon.com/images/I/41+ivDI2weS._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/179710232X?ipRedirectOverride=true&overrideBaseCountry=true
Length: 11h 33m 
Summary: "<p><b>From a renowned financial journalist who has written for </b><b><i>Time</i></b><b>, </b><b><i>Fortune</i></b><b>, </b><b><i>Forbes</i></b><b>, and </b><b><i>The New Yorker</i></b><b>, a fresh and unexpectedly profound book that draws on hundreds of hours of exclusive interviews with many of the world’s super-investors to demonstrate that the keys for building wealth hold other life lessons as well.</b></p> <p>Billionaire investors. If we think of them, it’s with a mixture of awe and suspicion. Clearly, they possess a kind of genius - the proverbial Midas Touch. But are the skills they possess transferable? And do they have anything to teach us <i>besides</i> making money?</p> <p>In <i>Richer, Wiser, Happier</i>, William Green draws on interviews that he’s conducted over 25 years with many of the world’s greatest investors. As he discovered, their talents extend well beyond the financial realm. The most successful investors are mavericks and iconoclasts who question conventional wisdom and profit vastly from their ability to think more rationally, rigorously, and objectively. They are master game players who consciously maximize their odds of long-term success in markets and life, while also minimizing any risk of catastrophe. They draw powerful insights from many different fields, are remarkably intuitive about trends, practice fanatical discipline, and have developed a high tolerance for pain. As Green explains, the best investors can teach us not only how to become rich, but how to improve the way we think, reach decisions, assess risk, avoid costly errors, build resilience, and turn uncertainty to our advantage.</p> <p>Green ushers us into the lives of more than 40 super-investors, visiting them in their offices, homes, and even their places of worship - all to share what they have to teach. From Sir John Templeton to Charlie Munger, Jack Bogle to Ed Thorp, Will Danoff to Mohnish Pabrai, Bill Miller to Laura Geritz, Joel Greenblatt to Howard Marks. In explaining how they think and why they win, this “unexpectedly illuminating” (Peter Diamandis) book provides “many nuggets of wisdom” (<i>The Washington Post</i>) that will enrich you both financially and personally. </p>"
Child Category: Erfolg im Beruf
Web Player: https://www.audible.com/webplayer?asin=179710232X
Release Date: 2021-04-20
---
# Richer, Wiser, Happier: How the World's Greatest Investors Win in Markets and Life

## Richer, Wiser, Happier

[https://m.media-amazon.com/images/I/41+ivDI2weS._SL500_.jpg](https://m.media-amazon.com/images/I/41+ivDI2weS._SL500_.jpg)

https://www.youtube.com/watch?v=Xsp0_IupS0U
