---
Search In Goodreads: https://www.goodreads.com/search?q=Dennis%20E.%20Taylor%20-%20Wir%20sind%20G%26ouml%3Btter
Authors: Dennis E. Taylor
Ratings: 7376
Isbn 10: 3453319214
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Random House Audio, Deutschland
Isbn 13: 9783453319219
Book Numbers: 2
Tags: 
    - Gentechnik
    - Naturwissenschaften
    - Weltraumerkundung
Added: 78
Progress: Beendet
Categories: 
    - Science Fiction & Fantasy 
    - Science Fiction
Sample: https://samples.audible.de/bk/rhde/003649/bk_rhde_003649_sample.mp3
Asin: B07KW263H8
Title: "Wir sind Götter: Bobiverse 2"
Title Short: "Wir sind Götter"
Narrators: Simon Jäger
Blurb: "Eigentlich hat Bob Johansson nie an ein Leben nach dem Tod geglaubt. Als er nach einem tödlichen Autounfall als Künstliche Intelligenz..."
Series: Bobiverse (book 2)
Cover: https://m.media-amazon.com/images/I/51e2P2KBMFL._SL500_.jpg
Parent Category: Science Fiction & Fantasy
Store Page Url: https://audible.de/pd/B07KW263H8?ipRedirectOverride=true&overrideBaseCountry=true
Length: 10h 35m 
Summary: "<p>Eigentlich hat Bob Johansson nie an ein Leben nach dem Tod geglaubt. Als er nach einem tödlichen Autounfall als Künstliche Intelligenz eines Raumschiffes wieder erwacht, ist er natürlich geschockt. Doch damit nicht genug - er ist der intelligente Computer von Neuman Probe, das heißt er wurde tausendfach repliziert. Bob und seine Kopien werden ausgeschickt, um in den Tiefen des Weltalls nach neuen, bewohnbaren Planeten zu suchen. Dabei stoßen sie nicht nur auf ein primitives Alien-Volk, das sie als Götter verehrt, sondern auch auf eine feindliche Spezies, die droht, die Erde anzugreifen - und die Bobs sind die Einzigen, die sie noch aufhalten können...</p>"
Child Category: Science Fiction
Web Player: https://www.audible.com/webplayer?asin=B07KW263H8
Release Date: 2018-12-07
---
# Wir sind Götter: Bobiverse 2

## Wir sind Götter

### Bobiverse (book 2)

[https://m.media-amazon.com/images/I/51e2P2KBMFL._SL500_.jpg](https://m.media-amazon.com/images/I/51e2P2KBMFL._SL500_.jpg)
