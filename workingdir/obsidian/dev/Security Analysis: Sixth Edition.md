---
Search In Goodreads: https://www.goodreads.com/search?q=Benjamin%20Graham%20-%20Security%20Analysis%3A%20Sixth%20Edition
Authors: Benjamin Graham, David Dodd
Ratings: 15
Isbn 10: 0071592539
Format: Ungekürztes Hörbuch
Language: English
Publishers: Echo Point Books & Media, LLC
Isbn 13: 9780071592536
Tags: 
    - Geld & Finanzen
    - Persönliche Finanzen
Added: 94
Categories: 
    - Geld & Finanzen 
    - Persönliche Finanzen
Sample: https://samples.audible.de/bk/acx0/178581/bk_acx0_178581_sample.mp3
Asin: B083PV5JMR
Title: "Security Analysis: Sixth Edition: Foreword by Warren Buffett"
Title Short: "Security Analysis: Sixth Edition"
Narrators: Scott R. Pollak
Blurb: "First published in 1934, Security Analysis is one of the most influential financial books ever written. Selling more than one million copies through five editions, it has provided generations of investors with timeless value investing philosophy and techniques...."
Cover: https://m.media-amazon.com/images/I/61VRbZRA4GL._SL500_.jpg
Parent Category: Geld & Finanzen
Store Page Url: https://audible.de/pd/B083PV5JMR?ipRedirectOverride=true&overrideBaseCountry=true
Length: 32 Std. 23 Min.
Summary: "<p><b>\"A road map for investing that I have now been following for 57 years.\" (From the Foreword by Warren E. Buffett) </b></p> <p>First published in 1934, <i>Security Analysis</i> is one of the most influential financial books ever written. Selling more than one million copies through five editions, it has provided generations of investors with the timeless value investing philosophy and techniques of Benjamin Graham and David L. Dodd. </p> <p>As relevant today as when they first appeared nearly 75 years ago, the teachings of Benjamin Graham, “the father of value investing”, have withstood the test of time across a wide diversity of market conditions, countries, and asset classes. </p> <p>This new sixth edition, based on the classic 1940 version, is enhanced with commentary from some of today’s leading Wall Street money managers. These masters of value investing explain why the principles and techniques of Graham and Dodd are still highly relevant even in today’s vastly different markets. The contributor list includes: </p> <p></p> <ul> <li>Seth A. Klarman, president of The Baupost Group, LLC and author of <i>Margin of Safety </i></li> <li>James Grant, founder of <i>Grant's Interest Rate Observer</i>, general partner of Nippon Partners </li> <li>Jeffrey M. Laderman, twenty-five year veteran of <i>BusinessWeek </i></li> <li>Roger Lowenstein, author of <i>Buffett: The Making of an American Capitalist</i> and <i>When America Aged</i> and Outside Director, Sequoia Fund </li> <li>Howard S. Marks, CFA, Chairman and Co-Founder, Oaktree Capital Management LP </li> <li>J. Ezra Merkin, Managing Partner, Gabriel Capital Group </li> <li>Bruce Berkowitz, Founder, Fairholme Capital Management </li> <li>Glenn H. Greenberg, Co-Founder and Managing Director, Chieftain Capital Management </li> <li>Bruce Greenwald, Robert Heilbrunn Professor of Finance and Asset Management, Columbia Business School </li> <li>David Abrams, Managing Member, Abrams Capital</li> </ul> <p>Featuring a foreword by Warren E. Buffett (in which he reveals that he has read the 1940 masterwork “at least four times”), this new edition of <i>Security Analysis</i> will reacquaint you with the foundations of value investing - more relevant than ever in the tumultuous 21st century markets.</p> <p><b>PLEASE NOTE: When you purchase this title, the accompanying PDF will be available in your Audible Library along with the audio.</b></p>"
Child Category: Persönliche Finanzen
Web Player: https://www.audible.com/webplayer?asin=B083PV5JMR
Release Date: 2020-01-10
---
# Security Analysis: Sixth Edition: Foreword by Warren Buffett

## Security Analysis: Sixth Edition

[https://m.media-amazon.com/images/I/61VRbZRA4GL._SL500_.jpg](https://m.media-amazon.com/images/I/61VRbZRA4GL._SL500_.jpg)
