---
Search In Goodreads: https://www.goodreads.com/search?q=Cixin%20Liu%20-%20Die%20drei%20Sonnen
Authors: Cixin Liu
Ratings: 3950
Isbn 10: 3453317165
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Rubikon Audioverlag
Isbn 13: 9783453317161
Book Numbers: 1
Tags: 
    - Erstkontakt
Added: 19
Progress: Beendet
Categories: 
    - Science Fiction & Fantasy 
    - Science Fiction
Sample: https://samples.audible.de/bk/xrub/000019/bk_xrub_000019_sample.mp3
Asin: B01MTAD5Q1
Title: "Die drei Sonnen: Die Trisolaris-Trilogie 1"
Title Short: "Die drei Sonnen"
Narrators: Mark Bremer
Blurb: "China, Ende der 1960er-Jahre: Während im ganzen Land die Kulturrevolution tobt, beginnt eine kleine Gruppe von Astrophysikern, Politkommissaren und..."
Series: Die Trisolaris-Trilogie (book 1)
Cover: https://m.media-amazon.com/images/I/619K9z1slOL._SL500_.jpg
Parent Category: Science Fiction & Fantasy
Store Page Url: https://audible.de/pd/B01MTAD5Q1?ipRedirectOverride=true&overrideBaseCountry=true
Length: 14h 53m 
Summary: "China, Ende der 1960er-Jahre: Während im ganzen Land die Kulturrevolution tobt, beginnt eine kleine Gruppe von Astrophysikern, Politkommissaren und Ingenieuren ein streng geheimes Forschungsprojekt. Ihre Aufgabe: Signale ins All zu senden und noch vor allen anderen Nationen Kontakt mit Außerirdischen aufzunehmen. Fünfzig Jahre später wird diese Vision Wirklichkeit - auf eine so erschreckende, umwälzende und globale Weise, dass dieser Kontakt das Schicksal der Menschheit für immer verändern wird."
Child Category: Science Fiction
Web Player: https://www.audible.com/webplayer?asin=B01MTAD5Q1
Release Date: 2017-01-20
---
# Die drei Sonnen: Die Trisolaris-Trilogie 1

## Die drei Sonnen

### Die Trisolaris-Trilogie (book 1)

[https://m.media-amazon.com/images/I/619K9z1slOL._SL500_.jpg](https://m.media-amazon.com/images/I/619K9z1slOL._SL500_.jpg)
