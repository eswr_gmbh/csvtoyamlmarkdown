---
Search In Goodreads: https://www.goodreads.com/search?q=Christine%20S.%20Richard%20-%20Confidence%20Game%3A%20How%20Hedge%20Fund%20Manager%20Bill%20Ackman%20Called%20Wall%20Street's%20Bluff
Authors: Christine S. Richard
My Rating: 5
Tags: 
    - 
Added: 70
Progress: Beendet
Categories: 
    - 
Asin: B008GWJCHQ
Title: "Confidence Game: How Hedge Fund Manager Bill Ackman Called Wall Street's Bluff"
Title Short: "Confidence Game: How Hedge Fund Manager Bill Ackman Called Wall Street's Bluff"
Narrators: Caroline Shaffer
Blurb: "Confidence Game: How a Hedge Fund Manager Called Wall Street's Bluff is the story of Bill Ackman's six-year campaign to warn that the $2.5 trillion bond insurance business was a catastrophe waiting to happen...."
Cover: https://m.media-amazon.com/images/I/51XwXpW9OFL._SL500_.jpg
Store Page Url: https://audible.de/pd/B008GWJCHQ?ipRedirectOverride=true&overrideBaseCountry=true
Summary: ""
Store Page Missing: true
Web Player: https://www.audible.com/webplayer?asin=B008GWJCHQ
---
# Confidence Game: How Hedge Fund Manager Bill Ackman Called Wall Street's Bluff

## Confidence Game: How Hedge Fund Manager Bill Ackman Called Wall Street's Bluff

[https://m.media-amazon.com/images/I/51XwXpW9OFL._SL500_.jpg](https://m.media-amazon.com/images/I/51XwXpW9OFL._SL500_.jpg)
