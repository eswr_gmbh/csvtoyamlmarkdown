---
Search In Goodreads: https://www.goodreads.com/search?q=Daniel%20Kahneman%20-%20Schnelles%20Denken%2C%20langsames%20Denken
Authors: Daniel Kahneman
Ratings: 4182
Isbn 10: 3641093740
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Der Hörverlag
Isbn 13: 9783641093747
Tags: 
    - Seelische & Geistige Gesundheit
Added: 6
Progress: Beendet
Categories: 
    - Gesundheit & Wellness 
    - Seelische & Geistige Gesundheit
Sample: https://samples.audible.de/bk/hoer/001043/bk_hoer_001043_sample.mp3
Asin: B00AIMNQTM
Title: "Schnelles Denken, langsames Denken"
Title Short: "Schnelles Denken, langsames Denken"
Narrators: Jürgen Holdorf
Blurb: "Wie treffen wir unsere Entscheidungen? Warum ist Zögern ein überlebensnotwendiger Reflex, und was passiert in unserem Gehirn..."
Cover: https://m.media-amazon.com/images/I/5145VAs6-eL._SL500_.jpg
Parent Category: Gesundheit & Wellness
Store Page Url: https://audible.de/pd/B00AIMNQTM?ipRedirectOverride=true&overrideBaseCountry=true
Length: 20h 47m 
Summary: "Wie treffen wir unsere Entscheidungen? Warum ist Zögern ein überlebensnotwendiger Reflex, und was passiert in unserem Gehirn, wenn wir andere Menschen oder Dinge beurteilen? Daniel Kahneman, Nobelpreisträger und einer der einflussreichsten Wissenschaftler unserer Zeit, zeigt anhand ebenso nachvollziehbarer wie verblüffender Beispiele, welchen mentalen Mustern wir folgen und wie wir uns gegen verhängnisvolle Fehlentscheidungen wappnen können. <p>Geldhändler, die ganze Bankenimperien ruinieren; Finanzmärkte, die außer Rand und Band sind; Kleinanleger, die ihr Erspartes in Aktien anlegen, ohne je den Wirtschaftsteil einer Zeitung gelesen zu haben: Wer in diesen Zeiten noch an den Homo oeconomicus als rational agierendes Wesen glaubt, dem ist nicht zu helfen. </p> <p>Daniel Kahneman liefert eine völlig andere Sichtweise, die nah am wirklichen menschlichen Verhalten orientiert ist und die Wirtschaftsakteure nicht als berechenbare Roboter betrachtet. Sein Fazit: Wir werden niemals immer und überall optimal handeln, wichtige Entscheidungen bleiben unsicher und fehleranfällig. Doch gibt es viele alltägliche Situationen, in denen wir die Qualität und die Folgen unseres Urteils entscheidend verbessern können. Ein Hörbuch, das unser Denken verändern wird. </p><p>>> Diese ungekürzte Hörbuch-Fassung genießt du exklusiv nur bei Audible.</p>"
Child Category: Seelische & Geistige Gesundheit
Web Player: https://www.audible.com/webplayer?asin=B00AIMNQTM
Release Date: 2012-12-10
---
# Schnelles Denken, langsames Denken

## Schnelles Denken, langsames Denken

[https://m.media-amazon.com/images/I/5145VAs6-eL._SL500_.jpg](https://m.media-amazon.com/images/I/5145VAs6-eL._SL500_.jpg)
