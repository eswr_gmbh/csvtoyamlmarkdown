---
Search In Goodreads: https://www.goodreads.com/search?q=Randall%20Munroe%20-%20What%20if%3F%20Was%20w%26auml%3Bre%20wenn%3F%20Wirklich%20wissenschaftliche%20Antworten%20auf%20absurde%20hypothetische%20Fragen
Authors: Randall Munroe
Ratings: 1320
Isbn 10: 3328106901
Format: Ungekürztes Hörbuch
My Rating: 4
Language: German
Publishers: Der Hörverlag
Isbn 13: 9783328106906
Book Numbers: 1
Tags: 
    - Wissenschaft
Added: 115
Progress: Beendet
Categories: 
    - Wissenschaft & Technik 
    - Wissenschaft
Sample: https://samples.audible.de/bk/hoer/001487/bk_hoer_001487_sample.mp3
Asin: B00V6PXZXW
Title: "What if? Was wäre wenn? Wirklich wissenschaftliche Antworten auf absurde hypothetische Fragen"
Title Short: "What if? Was wäre wenn? Wirklich wissenschaftliche Antworten auf absurde hypothetische Fragen"
Narrators: Norman Matt
Blurb: "Antworten auf Fragen, die Sie sich wahrscheinlich noch nie gestellt haben..."
Series: What if? (book 1)
Cover: https://m.media-amazon.com/images/I/51oz8oVYo8L._SL500_.jpg
Parent Category: Wissenschaft & Technik
Store Page Url: https://audible.de/pd/B00V6PXZXW?ipRedirectOverride=true&overrideBaseCountry=true
Length: 7h 33m 
Summary: "Antworten auf Fragen, die Sie sich wahrscheinlich noch nie gestellt haben. <br> <br>Wenn xkcd.com einen neuen Science Cartoon postet, vibriert das Internet. Sein Blog \"what if\", auf dem der Physiker Randall Munroe jede Woche bizarre Fragen mit exakter Wissenschaft und genialen Strichmännchen beantwortet, ist Kult. Wie lange würde es dauern, bis wir merken würden, dass sich der Erdumfang verändert? Hätten wir genug Energie, um die ganze Weltbevölkerung von der Erde wegzubefördern? Wann (wenn überhaupt) wird Facebook mehr Profile von Toten als von Lebenden enthalten? Wenn man eine zufällige Nummer wählt und \"Gesundheit!\" sagt, wie hoch ist die Wahrscheinlichkeit, dass der Angerufene tatsächlich gerade geniest hat?"
Child Category: Wissenschaft
Web Player: https://www.audible.com/webplayer?asin=B00V6PXZXW
Release Date: 2015-04-06
---
# What if? Was wäre wenn? Wirklich wissenschaftliche Antworten auf absurde hypothetische Fragen

## What if? Was wäre wenn? Wirklich wissenschaftliche Antworten auf absurde hypothetische Fragen

[https://m.media-amazon.com/images/I/51oz8oVYo8L._SL500_.jpg](https://m.media-amazon.com/images/I/51oz8oVYo8L._SL500_.jpg)
