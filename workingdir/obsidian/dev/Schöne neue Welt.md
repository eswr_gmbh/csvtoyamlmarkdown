---
Search In Goodreads: https://www.goodreads.com/search?q=Aldous%20Huxley%20-%20Sch%26ouml%3Bne%20neue%20Welt
Authors: Aldous Huxley
Ratings: 1868
Isbn 10: 3104027730
Format: Ungekürztes Hörbuch
My Rating: 4
Language: German
Publishers: Der Hörverlag
Isbn 13: 9783104027739
Tags: 
    - Klassiker
    - Dystopien
Added: 126
Progress: Beendet
Categories: 
    - Literatur & Belletristik 
    - Klassiker
Sample: https://samples.audible.de/bk/hoer/001206/bk_hoer_001206_sample.mp3
Asin: B00FGUD19G
Title: "Schöne neue Welt: Ein Roman der Zukunft"
Title Short: "Schöne neue Welt"
Narrators: Matthias Brandt
Blurb: "Eine Welt ohne Gewalt? In ferner Zukunft ist die ideale Gesellschaft geformt..."
Cover: https://m.media-amazon.com/images/I/61173ZO0vlL._SL500_.jpg
Parent Category: Literatur & Belletristik
Store Page Url: https://audible.de/pd/B00FGUD19G?ipRedirectOverride=true&overrideBaseCountry=true
Length: 8h 4m 
Summary: "Eine Welt ohne Gewalt? In ferner Zukunft ist die ideale Gesellschaft geformt: Künstlich reproduzierte, genmanipulierte Menschen sind mit Hilfe von Gehirnwäsche und Drogen glückliche Konsumenten. Nur der Alpha Bernard Marx scheint von einer unbestimmten Sehnsucht nach Freiheit getrieben zu sein. Ein Besuch in einem der wenigen verbliebenen Reservate, in denen noch das alte, unperfekte Leben geführt wird, könnte ihn von seinem Unbehagen heilen - oder etwa nicht?"
Child Category: Klassiker
Web Player: https://www.audible.com/webplayer?asin=B00FGUD19G
Release Date: 2013-09-30
---
# Schöne neue Welt: Ein Roman der Zukunft

## Schöne neue Welt

[https://m.media-amazon.com/images/I/61173ZO0vlL._SL500_.jpg](https://m.media-amazon.com/images/I/61173ZO0vlL._SL500_.jpg)
