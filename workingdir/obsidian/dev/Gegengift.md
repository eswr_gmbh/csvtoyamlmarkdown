---
Search In Goodreads: https://www.goodreads.com/search?q=Gerald%20H%C3%B6rhan%20-%20Gegengift
Authors: Gerald Hörhan
Ratings: 661
Isbn 10: 3990010298
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: ABOD Verlag
Isbn 13: 9783990010297
Tags: 
    - Politik & Regierungen
Added: 26
Progress: Beendet
Categories: 
    - Politik & Sozialwissenschaften 
    - Politik & Regierungen
Sample: https://samples.audible.de/bk/xaod/000057/bk_xaod_000057_sample.mp3
Asin: B00CKYETP6
Title: "Gegengift: Wie euch die Zukunft gestohlen wird. Was ihr dagegen tun könnt."
Title Short: "Gegengift"
Narrators: Matthias Lühn
Blurb: "Sparen bei der Bildung, schwieriger Arbeitsmarkt und keine Chance auf staatliche Altersversorgung mehr. Jetzt halst Europa den jungen Menschen auch..."
Cover: https://m.media-amazon.com/images/I/41reJh2oE1L._SL500_.jpg
Parent Category: Politik & Sozialwissenschaften
Store Page Url: https://audible.de/pd/B00CKYETP6?ipRedirectOverride=true&overrideBaseCountry=true
Length: 3h 44m 
Summary: "Sparen bei der Bildung, schwieriger Arbeitsmarkt und keine Chance auf staatliche Altersversorgung mehr. Jetzt halst Europa den jungen Menschen auch noch die Kosten für den Reformstau auf. Denn die Rechnung für die Krisen in Griechenland, Irland und Portugal bezahlen am Ende sie. Nach seinem Bestseller \"Investment Punk. Warum ihr schuftet und wir reich werden\" rüttelt der Investment-Banker und Punk Gerald Hörhan die jungen Generationen auf und sagt, wie sie sich wehren können. Eine gewohnt provokante Ansage des Harvard-Absolventen, der gerne mit Irokesenfrisur und Lederkluft auftritt und dabei im Aston Martin vorfährt."
Child Category: Politik & Regierungen
Web Player: https://www.audible.com/webplayer?asin=B00CKYETP6
Release Date: 2013-04-30
---
# Gegengift: Wie euch die Zukunft gestohlen wird. Was ihr dagegen tun könnt.

## Gegengift

[https://m.media-amazon.com/images/I/41reJh2oE1L._SL500_.jpg](https://m.media-amazon.com/images/I/41reJh2oE1L._SL500_.jpg)
