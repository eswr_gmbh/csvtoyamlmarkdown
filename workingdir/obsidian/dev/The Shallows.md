---
Search In Goodreads: https://www.goodreads.com/search?q=Nicholas%20Carr%20-%20The%20Shallows
Authors: Nicholas Carr
Ratings: 49
Isbn 10: 1838952586
Format: Ungekürztes Hörbuch
Language: English
Publishers: Blackstone Audio, Inc.
Isbn 13: 9781838952587
Tags: 
    - Geschichte & Kultur
    - Seelische & Geistige Gesundheit
    - Wissenschaft
Added: 105
Categories: 
    - Computer & Technologie 
    - Geschichte & Kultur
Sample: https://samples.audible.de/bk/blak/003822/bk_blak_003822_sample.mp3
Asin: B004V0DK5W
Title: "The Shallows: What the Internet Is Doing to Our Brains"
Title Short: "The Shallows"
Narrators: Richard Powers
Blurb: "A gripping story of human transformation played out against a backdrop of technological upheaval, The Shallows will forever alter the way we think about media and our minds...."
Cover: https://m.media-amazon.com/images/I/51pj3V0wxdL._SL500_.jpg
Parent Category: Computer & Technologie
Store Page Url: https://audible.de/pd/B004V0DK5W?ipRedirectOverride=true&overrideBaseCountry=true
Length: 10 Std. 6 Min.
Summary: "<p>The best-selling author of <i>The Big Switch</i> returns with an explosive look at technology’s effect on the mind.</p> <p>“Is Google making us stupid?” When Nicholas Carr posed that question in an <i>Atlantic Monthly</i> cover story, he tapped into a well of anxiety about how the internet is changing us. He also crystallized one of the most important debates of our time: As we enjoy the internet’s bounties, are we sacrificing our ability to read and think deeply?</p> <p>Now, Carr expands his argument into the most compelling exploration yet published of the internet’s intellectual and cultural consequences. Weaving insights from philosophy, neuroscience, and history into a rich narrative, <i>The Shallows</i> explains how the internet is rerouting our neural pathways, replacing the subtle mind of the book reader with the distracted mind of the screen watcher. A gripping story of human transformation played out against a backdrop of technological upheaval, <i>The Shallows</i> will forever alter the way we think about media and our minds.</p>"
Child Category: Geschichte & Kultur
Web Player: https://www.audible.com/webplayer?asin=B004V0DK5W
Release Date: 2010-06-10
---
# The Shallows: What the Internet Is Doing to Our Brains

## The Shallows

[https://m.media-amazon.com/images/I/51pj3V0wxdL._SL500_.jpg](https://m.media-amazon.com/images/I/51pj3V0wxdL._SL500_.jpg)
