---
Search In Goodreads: https://www.goodreads.com/search?q=Walter%20Isaacson%20-%20Steve%20Jobs
Authors: Walter Isaacson
Ratings: 7783
Isbn 10: 3641074622
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Random House Audio, Deutschland
Isbn 13: 9783641074623
Tags: 
    - Wirtschaft
    - Wissenschaft & Technik
Added: 39
Progress: Beendet
Categories: 
    - Biografien & Erinnerungen 
    - Akademiker & Spezialisten
Sample: https://samples.audible.de/bk/rhde/001518/bk_rhde_001518_sample.mp3
Asin: B006FZ8X8U
Title: "Steve Jobs: Die autorisierte Biografie des Apple-Gründers"
Title Short: "Steve Jobs"
Narrators: Frank Arnold
Blurb: "Macintosh, iMac, iPod, iTunes, iPhone, iPad - Steve Jobs hat der digitalen Welt mit der Kultmarke Apple Ästhetik und Aura gegeben..."
Cover: https://m.media-amazon.com/images/I/41HE5dLu2ZL._SL500_.jpg
Parent Category: Biografien & Erinnerungen
Store Page Url: https://audible.de/pd/B006FZ8X8U?ipRedirectOverride=true&overrideBaseCountry=true
Length: 26h 1m 
Summary: "Macintosh, iMac, iPod, iTunes, iPhone, iPad - Steve Jobs hat der digitalen Welt mit der Kultmarke Apple Ästhetik und Aura gegeben. Wo Bill Gates für solide Alltagsarbeit steht, ist der Mann aus San Francisco die Stilikone des IT-Zeitalters, ein begnadeter Vordenker, der kompromisslos seiner Idee folgt. Genial und selbstbewusst hat er trotz ökonomischer und persönlicher Krisen den Apfel mit Biss (Bite) zum Synonym für Innovation und Vision gemacht. Doch wer ist dieser Meister der Inszenierung, was treibt ihn? <p>Walter Isaacson gewann das Vertrauen des Apple-Chefs und konnte als erster Biograf während der langjährigen Recherchen auf seine uneingeschränkte Unterstützung ebenso bauen wie auf die seiner Familie, seiner Weggefährten und auch der Kontrahenten. Entstanden ist das Buch über Steve Jobs und sein Unternehmen - nicht nur für Apple-Fans. </p> <p>In deiner Audible-Bibliothek findest du für dieses Hörerlebnis eine PDF-Datei mit zusätzlichem Material.</p>"
Child Category: Akademiker & Spezialisten
Web Player: https://www.audible.com/webplayer?asin=B006FZ8X8U
Release Date: 2011-11-30
---
# Steve Jobs: Die autorisierte Biografie des Apple-Gründers

## Steve Jobs

[https://m.media-amazon.com/images/I/41HE5dLu2ZL._SL500_.jpg](https://m.media-amazon.com/images/I/41HE5dLu2ZL._SL500_.jpg)
