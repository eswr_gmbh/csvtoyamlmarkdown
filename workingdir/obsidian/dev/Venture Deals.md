---
Search In Goodreads: https://www.goodreads.com/search?q=Jason%20Mendelson%20-%20Venture%20Deals
Authors: Jason Mendelson, Brad Feld
Ratings: 32
Isbn 10: 1118538676
Format: Ungekürztes Hörbuch
My Rating: 4
Language: English
Publishers: Gildan Media, LLC
Isbn 13: 9781118538678
Tags: 
    - Geschäftsentwicklung & Unternehmertum
    - Geld & Finanzen
    - Persönliche Finanzen
Added: 59
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Geschäftsentwicklung & Unternehmertum
Sample: https://samples.audible.de/bk/gdan/000604/bk_gdan_000604_sample.mp3
Asin: B005UQ0X60
Title: "Venture Deals: Be Smarter Than Your Lawyer and Venture Capitalist"
Title Short: "Venture Deals"
Narrators: Sean Pratt
Blurb: "As each new generation of entrepreneurs emerges, there is a renewed interest in how venture capital deals come together...."
Cover: https://m.media-amazon.com/images/I/511EImcAEWL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B005UQ0X60?ipRedirectOverride=true&overrideBaseCountry=true
Length: 7h 19m 
Summary: "<p>As each new generation of entrepreneurs emerges, there is a renewed interest in how venture capital deals come together. Yet there really is no definitive guide to venture capital deals. Nobody understands this better than authors Brad Feld and Jason Mendelson. For more than seventeen years, they've been involved in hundreds of venture capital financings, and now, with <i>Venture Deals</i>, they share their experiences in this field with you. </p> <p>Inspired by a series of blog posts - created by the authors after a particularly challenging deal - this reliable resource demystifies the venture capital financing process and helps you gain a practical perspective of this dynamic discipline. </p> <p>Whether you're an experienced or aspiring entrepreneur, venture capitalist, or lawyer who partakes in these particular types of deals, you can benefit from the insights found throughout this book. Engaging and informative, Venture Deals skillfully outlines the essential elements of the venture capital term sheet - from terms related to economics to terms related to control. Feld and Mendelson strive to give a balanced view of the particular terms along with the strategies to getting to a fair deal. In addition to examining the nuts and bolts of the term sheet, <i>Venture Deals</i> also introduces you to the various participants in the process, discusses how fundraising works, reveals how VC firms operate, and describes how to apply different negotiating tactics to your deals. You'll also gain valuable insights into several common legal issues most startups face and, as a bonus, discover what a typical letter of intent to acquire your company looks like. </p>"
Child Category: Geschäftsentwicklung & Unternehmertum
Web Player: https://www.audible.com/webplayer?asin=B005UQ0X60
Release Date: 2011-10-12
---
# Venture Deals: Be Smarter Than Your Lawyer and Venture Capitalist

## Venture Deals

[https://m.media-amazon.com/images/I/511EImcAEWL._SL500_.jpg](https://m.media-amazon.com/images/I/511EImcAEWL._SL500_.jpg)
