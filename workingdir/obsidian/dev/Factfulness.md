---
Search In Goodreads: https://www.goodreads.com/search?q=Hans%20Rosling%20-%20Factfulness
Authors: Hans Rosling, Anna Rosling Rönnlund, Ola Rosling
Ratings: 3804
Isbn 10: 3843717451
Format: Ungekürztes Hörbuch
Language: German
Publishers: Argon Verlag
Isbn 13: 9783843717458
Tags: 
    - Politik & Regierungen
    - Medienwissenschaften
Added: 160
Progress: Beendet
Categories: 
    - Politik & Sozialwissenschaften 
    - Politik & Regierungen
Sample: https://samples.audible.de/bk/argo/002033/bk_argo_002033_sample.mp3
Asin: B07DDLMC1F
Title: "Factfulness: Wie wir lernen, die Welt so zu sehen, wie sie wirklich ist"
Title Short: "Factfulness"
Narrators: Uve Teschner
Blurb: "Es wird alles immer schlimmer, eine schreckliche Nachricht jagt die andere: Die Reichen werden reicher, die Armen ärmer. Es gibt immer mehr Kriege..."
Cover: https://m.media-amazon.com/images/I/51q0Oh-YZXL._SL500_.jpg
Parent Category: Politik & Sozialwissenschaften
Store Page Url: https://audible.de/pd/B07DDLMC1F?ipRedirectOverride=true&overrideBaseCountry=true
Length: 8h 13m 
Summary: "<p>Es wird alles immer schlimmer, eine schreckliche Nachricht jagt die andere: Die Reichen werden reicher, die Armen ärmer. Es gibt immer mehr Kriege, Gewaltverbrechen, Naturkatastrophen. Viele Menschen tragen solche beängstigenden Bilder im Kopf. Doch sie liegen damit grundfalsch. Unser Gehirn verführt uns zu einer dramatisierenden Weltsicht, die mitnichten der Realität entspricht, wie der geniale Statistiker und Wissenschaftler Hans Rosling erklärt. Wer das Hörbuch gehört hat, wird</p> <p></p> <ul> <li>ein sicheres, auf Fakten basierendes Gerüst besitzen, um die Welt so zu sehen, wie sie wirklich ist,</li> <li>die zehn gängigsten Arten von aufgebauschten Geschichten erkennen,</li> <li>bessere Entscheidungen treffen können,</li> <li>wahre Factfulness erreichen - jene offene, neugierige und entspannte Geisteshaltung, in der Sie nur noch Ansichten teilen und Urteile fällen, die auf soliden Fakten basieren.</li> </ul> <p>In deiner Audible-Bibliothek findest du für dieses Hörerlebnis eine PDF-Datei mit zusätzlichem Material.</p>"
Child Category: Politik & Regierungen
Web Player: https://www.audible.com/webplayer?asin=B07DDLMC1F
Release Date: 2018-07-06
---
# Factfulness: Wie wir lernen, die Welt so zu sehen, wie sie wirklich ist

## Factfulness

[https://m.media-amazon.com/images/I/51q0Oh-YZXL._SL500_.jpg](https://m.media-amazon.com/images/I/51q0Oh-YZXL._SL500_.jpg)
