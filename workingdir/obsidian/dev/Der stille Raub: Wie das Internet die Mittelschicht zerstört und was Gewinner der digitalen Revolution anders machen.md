---
Search In Goodreads: https://www.goodreads.com/search?q=Gerald%20H%C3%B6rhan%20-%20Der%20stille%20Raub%3A%20Wie%20das%20Internet%20die%20Mittelschicht%20zerst%26ouml%3Brt%20und%20was%20Gewinner%20der%20digitalen%20Revolution%20anders%20machen
Authors: Gerald Hörhan
Ratings: 1842
Isbn 10: 3990012258
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: ABOD Verlag
Isbn 13: 9783990012253
Tags: 
    - Zukunftsstudien
    - Politik & Sozialwissenschaften
Added: 22
Progress: Beendet
Categories: 
    - Politik & Sozialwissenschaften 
    - Sozialwissenschaften
Sample: https://samples.audible.de/bk/xaod/000453/bk_xaod_000453_sample.mp3
Asin: B06WWRHP4J
Title: "Der stille Raub: Wie das Internet die Mittelschicht zerstört und was Gewinner der digitalen Revolution anders machen"
Title Short: "Der stille Raub: Wie das Internet die Mittelschicht zerstört und was Gewinner der digitalen Revolution anders machen"
Narrators: Matthias Lühn
Blurb: "Binnen weniger Jahre wird die digitale Revolution die Gesellschaft komplett verändern. Wenige werden reich, viele arm, und die Mittelschicht wird es nur..."
Cover: https://m.media-amazon.com/images/I/51f0tdr946L._SL500_.jpg
Parent Category: Politik & Sozialwissenschaften
Store Page Url: https://audible.de/pd/B06WWRHP4J?ipRedirectOverride=true&overrideBaseCountry=true
Length: 4h 26m 
Summary: "Binnen weniger Jahre wird die digitale Revolution die Gesellschaft komplett verändern. Wenige werden reich, viele arm, und die Mittelschicht wird es nur noch in den Geschichtsbüchern geben. Gerald Hörhan, Harvard-Absolvent, Investmentbanker und Internet-Unternehmer, zeigt, was die künftigen Gewinner der digitalen Revolution jetzt tun müssen und warum alle anderen untergehen. In provokantem Ton lässt Hörhan, der an Wirtschaftsuniversitäten lehrt und mit seiner Online-Akademie einen MBA (Master of Business Administration) anbietet, hinter die Kulissen der digitalen Wirtschaft blicken. <br> <br>Ein Hörbuch, das erschreckt, und zugleich die neuen Chancen zeigt."
Child Category: Sozialwissenschaften
Web Player: https://www.audible.com/webplayer?asin=B06WWRHP4J
Release Date: 2017-02-25
---
# Der stille Raub: Wie das Internet die Mittelschicht zerstört und was Gewinner der digitalen Revolution anders machen

## Der stille Raub: Wie das Internet die Mittelschicht zerstört und was Gewinner der digitalen Revolution anders machen

[https://m.media-amazon.com/images/I/51f0tdr946L._SL500_.jpg](https://m.media-amazon.com/images/I/51f0tdr946L._SL500_.jpg)
