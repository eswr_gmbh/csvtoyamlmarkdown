---
Search In Goodreads: https://www.goodreads.com/search?q=Ayn%20Rand%20-%20Atlas%20Shrugged
Authors: Ayn Rand
Ratings: 275
Isbn 10: 0141188936
Format: Ungekürztes Hörbuch
Language: English
Publishers: Blackstone Audio, Inc.
Isbn 13: 9780141188935
Tags: 
    - Klassiker
    - Politik
Added: 188
Progress: Beendet
Categories: 
    - Literatur & Belletristik 
    - Klassiker
Sample: https://samples.audible.de/bk/blak/002897/bk_blak_002897_sample.mp3
Asin: B009M8VLT0
Title: "Atlas Shrugged"
Title Short: "Atlas Shrugged"
Narrators: Scott Brick
Blurb: "Tremendous in scope and breathtaking in its suspense, Atlas Shrugged is Ayn Rand's magnum opus, an electrifying moral defense of capitalism and free enterprise...."
Cover: https://m.media-amazon.com/images/I/51vWAJneFBL._SL500_.jpg
Parent Category: Literatur & Belletristik
Store Page Url: https://audible.de/pd/B009M8VLT0?ipRedirectOverride=true&overrideBaseCountry=true
Length: 62h 56m 
Summary: "In a scrap heap within an abandoned factory, the greatest invention in history lies dormant and unused. By what fatal error of judgment has its value gone unrecognized, its brilliant inventor punished rather than rewarded for his efforts? <p>In defense of those greatest of human qualities that have made civilization possible, one man sets out to show what would happen to the world if all the heroes of innovation and industry went on strike. Is he a destroyer or a liberator? And why does he fight his hardest battle not against his enemies but against the woman he loves?</p> <p>Tremendous in scope and breathtaking in its suspense, <i>Atlas Shrugged</i> is Ayn Rand's magnum opus, an electrifying moral defense of capitalism and free enterprise which launched an ideological movement and gained millions of loyal fans around the world.</p>"
Child Category: Klassiker
Web Player: https://www.audible.com/webplayer?asin=B009M8VLT0
Release Date: 2008-12-02
---
