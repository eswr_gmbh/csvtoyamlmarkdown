---
Search In Goodreads: https://www.goodreads.com/search?q=Dr.%20Anna%20Lembke%20-%20Dopamine%20Nation
Authors: Dr. Anna Lembke
Ratings: 258
Isbn 10: 1524746738
Format: Ungekürztes Hörbuch
Language: English
Publishers: Penguin Audio
Isbn 13: 9781524746735
Tags: 
    - Seelische & Geistige Gesundheit
    - Wissenschaft
Added: 177
Progress: 1 Min. verbleibend
Categories: 
    - Gesundheit & Wellness 
    - Seelische & Geistige Gesundheit
Sample: https://samples.audible.de/bk/peng/005497/bk_peng_005497_sample.mp3
Asin: 0593409221
Title: "Dopamine Nation: Finding Balance in the Age of Indulgence"
Title Short: "Dopamine Nation"
Narrators: Dr. Anna Lembke
Blurb: "This book is about pleasure. It’s also about pain. Most important, it’s about how to find the delicate balance between the two, and why now more than ever finding balance is essential...."
Cover: https://m.media-amazon.com/images/I/51bhG2GW-QL._SL500_.jpg
Parent Category: Gesundheit & Wellness
Store Page Url: https://audible.de/pd/0593409221?ipRedirectOverride=true&overrideBaseCountry=true
Length: 6h 11m 
Summary: "<p><b>Instant </b><b><i>New York Times</i></b><b> and </b><b><i>Los Angeles Times</i></b><b> Best Seller</b></p> <p><b>“Brilliant…riveting, scary, cogent, and cleverly argued.” (Beth Macy, author of </b><b><i>Dopesick</i></b><br> <b>As heard on Fresh Air)</b></p> <p>This book is about pleasure. It’s also about pain. Most important, it’s about how to find the delicate balance between the two, and why now more than ever finding balance is essential. We’re living in a time of unprecedented access to high-reward, high-dopamine stimuli: drugs, food, news, gambling, shopping, gaming, texting, sexting, Facebooking, Instagramming, YouTubing, tweeting....</p> <p>The increased numbers, variety, and potency is staggering. The smartphone is the modern-day hypodermic needle, delivering digital dopamine 24/7 for a wired generation. As such we’ve all become vulnerable to compulsive overconsumption.</p> <p>In <i>Dopamine Nation</i>, Dr. Anna Lembke, psychiatrist and author, explores the exciting new scientific discoveries that explain why the relentless pursuit of pleasure leads to pain…and what to do about it. Condensing complex neuroscience into easy-to-understand metaphors, Lembke illustrates how finding contentment and connectedness means keeping dopamine in check. The lived experiences of her patients are the gripping fabric of her narrative. Their riveting stories of suffering and redemption give us all hope for managing our consumption and transforming our lives. In essence, <i>Dopamine Nation </i>shows that the secret to finding balance is combining the science of desire with the wisdom of recovery.</p>"
Child Category: Seelische & Geistige Gesundheit
Web Player: https://www.audible.com/webplayer?asin=0593409221
Release Date: 2021-08-24
---
# Dopamine Nation: Finding Balance in the Age of Indulgence

## Dopamine Nation

[https://m.media-amazon.com/images/I/51bhG2GW-QL._SL500_.jpg](https://m.media-amazon.com/images/I/51bhG2GW-QL._SL500_.jpg)
