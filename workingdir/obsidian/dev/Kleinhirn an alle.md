---
Search In Goodreads: https://www.goodreads.com/search?q=Otto%20Waalkes%20-%20Kleinhirn%20an%20alle
Authors: Otto Waalkes
Ratings: 1782
Isbn 10: 3453201167
Format: Gekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Random House Audio, Deutschland
Isbn 13: 9783453201163
Tags: 
    - Biografien & Erinnerungen
Added: 116
Progress: <1 min verbleibend
Categories: 
    - Biografien & Erinnerungen
Sample: https://samples.audible.de/bk/rhde/003510/bk_rhde_003510_sample.mp3
Asin: B07CQ2XXWW
Title: "Kleinhirn an alle: Die große Ottobiografie - Nach einer wahren Geschichte"
Title Short: "Kleinhirn an alle"
Narrators: Otto Waalkes
Blurb: "Otto wird rund! 70. Geburtstag am 22. Juli 2018. Darauf haben Generationen von Fans gerade noch gewartet: Otto erzählt aus den ersten 70 Jahren seines Lebens..."
Cover: https://m.media-amazon.com/images/I/51lldtQRNZL._SL500_.jpg
Parent Category: Biografien & Erinnerungen
Store Page Url: https://audible.de/pd/B07CQ2XXWW?ipRedirectOverride=true&overrideBaseCountry=true
Length: 6h 12m 
Summary: "<p>Otto wird rund! 70. Geburtstag am 22. Juli 2018</p> <p>Darauf haben Generationen von Fans gerade noch gewartet: Otto erzählt aus den ersten 70 Jahren seines Lebens - einem märchenhaften Aufstieg vom Deichkind zum Alleinunterhalter der Nation. Seine Sketche und Figuren haben unser kollektives Gedächtnis und unseren Witzwortschatz bereichert: Harry Hirsch (übergibt sich ins Funkhaus), Robin Hood (der Stecher der Entnervten), Susi Sorglos (föhnt ihr goldenes Haar), Louis Flambée (kocht Pommes de Bordell) Peter, Paul and Mary (are planning a bank robbery), Hänsel und Gretel (verirren sich im Wald) und der \"Schniedelwutz\" (hat's bis in den Duden gebracht).</p> <p>Was Otto-Fans seit Generationen wissen wollten und bisher nicht zu fragen wagten: Wer waren Ottos Vorbilder? Wo kommt er her? Was treibt ihn an? Wie entsteht seine eigene Art von Komik? Und wozu eigentlich? Gibt es ein Geheimnis?</p> <p>In seiner großen Ottobiografie erzählt Otto freiwillig von Höhe- und Tiefpunkten, von den glücklichsten und den glanzvollsten Momenten, ohne die peinlichsten und traurigsten auszulassen.</p> <p>Waalkes liest Waalkes <br>  </p>"
Web Player: https://www.audible.com/webplayer?asin=B07CQ2XXWW
Release Date: 2018-05-14
---
# Kleinhirn an alle: Die große Ottobiografie - Nach einer wahren Geschichte

## Kleinhirn an alle

[https://m.media-amazon.com/images/I/51lldtQRNZL._SL500_.jpg](https://m.media-amazon.com/images/I/51lldtQRNZL._SL500_.jpg)
