---
Search In Goodreads: https://www.goodreads.com/search?q=John%20Doerr%20-%20Measure%20What%20Matters
Authors: John Doerr, Larry Page - foreword
Ratings: 315
Isbn 10: 052553623X
Format: Ungekürztes Hörbuch
Language: English
Publishers: Penguin Audio
Isbn 13: 9780525536239
Tags: 
    - Management & Leadership
    - Geschäftsentwicklung & Unternehmertum
    - Arbeitsplatz- & Organisationsverhalten
Added: 95
Progress: 3 Std. 1 Min. verbleibend
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/peng/003954/bk_peng_003954_sample.mp3
Asin: B07BMDKB6T
Title: "Measure What Matters: How Google, Bono, and the Gates Foundation Rock the World with OKRs"
Title Short: "Measure What Matters"
Narrators: John Doerr, full cast, Julia Collins, Jini Kim, Mike Lee, Atticus Tysen, Patti Stonesifer, Susan Wojcicki, Cristos Goodrow, Alex Garden, Joseph Suzuki, Various
Blurb: "Legendary venture capitalist John Doerr reveals how the goal-setting system of Objectives and Key Results (OKRs) has helped tech giants from Intel to Google achieve explosive growth - and how it can help any organization thrive...."
Cover: https://m.media-amazon.com/images/I/51HNGEmZEnL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B07BMDKB6T?ipRedirectOverride=true&overrideBaseCountry=true
Length: 7h 56m 
Summary: "<p><b>Legendary venture capitalist John Doerr reveals how the goal-setting system of Objectives and Key Results (OKRs) has helped tech giants from Intel to Google achieve explosive growth - and how it can help any organization thrive.</b></p> <p>In the fall of 1999, John Doerr met with the founders of a start-up whom he'd just given $12.5 million, the biggest investment of his career. Larry Page and Sergey Brin had amazing technology, entrepreneurial energy, and sky-high ambitions, but no real business plan. For Google to change the world (or even to survive), Page and Brin had to learn how to make tough choices on priorities while keeping their team on track. They'd have to know when to pull the plug on losing propositions, to fail fast. And they needed timely, relevant data to track their progress - to measure what mattered.</p> <p>Doerr taught them about a proven approach to operating excellence: Objectives and Key Results. He had first discovered OKRs in the 1970s as an engineer at Intel, where the legendary Andy Grove (\"the greatest manager of his or any era\") drove the best-run company Doerr had ever seen. Later, as a venture capitalist, Doerr shared Grove's brainchild with more than 50 companies. Wherever the process was faithfully practiced, it worked.</p> <p>In this goal-setting system, objectives define what we seek to achieve; key results are how those top-priority goals will be attained with specific, measurable actions within a set time frame. Everyone's goals, from entry level to CEO, are transparent to the entire organization.</p> <p>The benefits are profound. OKRs surface an organization's most important work. They focus effort and foster coordination. They keep employees on track. They link objectives across silos to unify and strengthen the entire company. Along the way, OKRs enhance workplace satisfaction and boost retention.</p> <p>In <i>Measure What Matters</i>, Doerr shares a broad range of first-person, behind-the-scenes case studies, with narrators including Bono and Bill Gates, to demonstrate the focus, agility, and explosive growth that OKRs have spurred at so many great organizations. This book will help a new generation of leaders capture the same magic.</p> <p><i>Read by John Doerr, William Davidow, Brett Kopf, Jini Kim, Mike Lee, Atticus Tysen, Patti Stonesifer, Susan Wojcicki, Cristos Goodrow, Julia Collins, Alex Garden, Joseph Suzuki, Andrew Cole, Bono, and others </i></p>"
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=B07BMDKB6T
Release Date: 2018-04-24
---
# Measure What Matters: How Google, Bono, and the Gates Foundation Rock the World with OKRs

## Measure What Matters

[https://m.media-amazon.com/images/I/51HNGEmZEnL._SL500_.jpg](https://m.media-amazon.com/images/I/51HNGEmZEnL._SL500_.jpg)
