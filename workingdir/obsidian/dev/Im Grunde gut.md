---
Search In Goodreads: https://www.goodreads.com/search?q=Rutger%20Bregman%20-%20Im%20Grunde%20gut
Authors: Rutger Bregman
Ratings: 1557
Isbn 10: 3644007632
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Argon Verlag
Isbn 13: 9783644007635
Tags: 
    - Anthropologie
Added: 112
Progress: Beendet
Categories: 
    - Politik & Sozialwissenschaften 
    - Anthropologie
Sample: https://samples.audible.de/bk/argo/002606/bk_argo_002606_sample.mp3
Asin: 3732454320
Title: "Im Grunde gut: Eine neue Geschichte der Menschheit"
Title Short: "Im Grunde gut"
Narrators: Julian Mehne
Blurb: "Der Historiker und Journalist Rutger Bregman setzt sich in seinem neuen Buch mit dem Wesen des Menschen auseinander. Anders als in der..."
Cover: https://m.media-amazon.com/images/I/416rNOYLsoL._SL500_.jpg
Parent Category: Politik & Sozialwissenschaften
Store Page Url: https://audible.de/pd/3732454320?ipRedirectOverride=true&overrideBaseCountry=true
Length: 13h 15m 
Summary: "<p>Der Historiker und Journalist Rutger Bregman setzt sich in seinem neuen Buch mit dem Wesen des Menschen auseinander. Anders als in der westlichen Denktradition angenommen ist der Mensch seinen Thesen nach nicht böse, sondern im Gegenteil: von Grund auf gut. Und geht man von dieser Prämisse aus, ist es möglich, die Welt und den Menschen in ihr komplett neu und grundoptimistisch zu denken. In seinem mitreißend geschriebenen, überzeugenden Buch präsentiert Bregman Ideen für die Verbesserung der Welt. Sie sind innovativ und mutig und stimmen vor allem hoffnungsfroh.</p>"
Child Category: Anthropologie
Web Player: https://www.audible.com/webplayer?asin=3732454320
Release Date: 2020-03-10
---
# Im Grunde gut: Eine neue Geschichte der Menschheit

## Im Grunde gut

[https://m.media-amazon.com/images/I/416rNOYLsoL._SL500_.jpg](https://m.media-amazon.com/images/I/416rNOYLsoL._SL500_.jpg)
