---
Search In Goodreads: https://www.goodreads.com/search?q=Stephen%20R.%20Covey%20-%20Der%208.%20Weg
Authors: Stephen R. Covey
Ratings: 583
Isbn 10: 3869368950
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Gabal
Isbn 13: 9783869368955
Tags: 
    - Management & Leadership
    - Persönlicher Erfolg
Added: 97
Progress: 13 Min. verbleibend
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/gaba/000056/bk_gaba_000056_sample.mp3
Asin: B004V01ZU4
Title: "Der 8. Weg: Mit Effektivität zu wahrer Größe"
Title Short: "Der 8. Weg"
Narrators: Heiko Grauel, Sonngard Dressler
Blurb: "Wir leben im Zeitalter der Wissensarbeit. Doch wir verwenden immer noch das Führungsmodell des Industriezeitalters..."
Cover: https://m.media-amazon.com/images/I/51F+OiGrtxL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B004V01ZU4?ipRedirectOverride=true&overrideBaseCountry=true
Length: 13h 38m 
Summary: "Wir leben im Zeitalter der Wissensarbeit. Doch wir verwenden immer noch das Führungsmodell des Industriezeitalters. Wir managen Menschen wie Dinge, betrachten Mitarbeiter als Kostenfaktor und motivieren mit Zuckerbrot und Peitsche. Und wundern uns, warum unsere Mitarbeiter und Unternehmen in der Mittelmäßigkeit verharren. Dabei ruft unsere Gegenwart nach Sinnhaftigkeit und leidenschaftlicher Umsetzung. Stephen R. Covey zeigt in seinem neuen Werk, dass dafür der Mensch mit all seinen Potenzialen im Mittelpunkt stehen muss. GuteFührungskräfte helfen anderen dabei, ihr Potenzial zu entwickeln. Und wissen auch, dass sie bei sich selbst beginnen müssen, wenn sie Veränderung wollen. Dazu braucht es Vision, Disziplin, Leidenschaft und Gewissen. <p>Der Autor: <br> Das Times Magazin zählte Stephen R. Covey 1996 zu den 25 einflussreichsten Menschen in Amerika. Zu Recht, denn Covey galt und gilt weiterhin als der führende Experte für das Thema \"Leadership\". Er ist als Sprecher auf internationalen Bühnen ebenso gefragt wie als Berater von Unternehmen, Top Managern und Regierungen. Stephen Covey ist Mitbegründer und Vice Chairman des weltweit operierenden FranklinCovey Instituts, das inzwischen in mehr als 130 Ländern vertreten ist. Und vor allem ist er ein Visionär, der an Effektivität als Motor für das Wachstum von Individuen und Unternehmen glaubt. </p>"
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=B004V01ZU4
Release Date: 2007-06-01
---
# Der 8. Weg: Mit Effektivität zu wahrer Größe

## Der 8. Weg

[https://m.media-amazon.com/images/I/51F+OiGrtxL._SL500_.jpg](https://m.media-amazon.com/images/I/51F+OiGrtxL._SL500_.jpg)
