---
Search In Goodreads: https://www.goodreads.com/search?q=Richard%20C.%20Schwartz%20PhD%20-%20No%20Bad%20Parts
Authors: Richard C. Schwartz PhD, Alanis Morissette - foreword introduction
Ratings: 29
Isbn 10: 1683646681
Format: Ungekürztes Hörbuch
Language: English
Publishers: Sounds True
Isbn 13: 9781683646686
Tags: 
    - Seelische & Geistige Gesundheit
    - Persönlicher Erfolg
Added: 185
Progress: <1 min verbleibend
Categories: 
    - Gesundheit & Wellness 
    - Seelische & Geistige Gesundheit
Sample: https://samples.audible.de/sp/true/001648/sp_true_001648_sample.mp3
Asin: B09GKWKT79
Title: "No Bad Parts: Healing Trauma and Restoring Wholeness with the Internal Family Systems Model"
Title Short: "No Bad Parts"
Narrators: Charlie Mechling
Blurb: "Dr. Schwartz’s Internal Family Systems (IFS) model has been transforming psychology for decades. With No Bad Parts, you’ll learn why IFS has been so effective in areas such as trauma recovery, addiction therapy, and depression treatment...."
Cover: https://m.media-amazon.com/images/I/51BFS345dcL._SL500_.jpg
Parent Category: Gesundheit & Wellness
Store Page Url: https://audible.de/pd/B09GKWKT79?ipRedirectOverride=true&overrideBaseCountry=true
Length: 8h 10m 
Summary: "<p><b>Discover an empowering new way of understanding your multifaceted mind - and healing the many parts that make you who you are.</b></p> <p>Is there just one “you”? We’ve been taught to believe we have a single identity, and to feel fear or shame when we can’t control the inner voices that don’t match the ideal of who we think we should be. Yet Dr. Richard Schwartz’s research now challenges this “mono-mind” theory. “All of us are born with many sub-minds - or parts,” says Dr. Schwartz. “These parts are not imaginary or symbolic. They are individuals who exist as an internal family within us - and the key to health and happiness is to honor, understand, and love <i>every </i>part.”</p> <p>Dr. Schwartz’s Internal Family Systems (IFS) model has been transforming psychology for decades. With <i>No Bad Parts</i>, you’ll learn why IFS has been so effective in areas such as trauma recovery, addiction therapy, and depression treatment - and how this new understanding of consciousness has the potential to radically change our lives. Here you’ll explore:</p> <p></p> <ul> <li>The IFS revolution - how honoring and communicating with our parts changes our approach to mental wellness</li> <li>Overturning the cultural, scientific, and spiritual assumptions that reinforce an outdated mono-mind model</li> <li>The ego, the inner critic, the saboteur - making these often-maligned parts into powerful allies</li> <li>Burdens - why our parts become distorted and stuck in childhood traumas and cultural beliefs</li> <li>How IFS demonstrates human goodness by revealing that there are no bad parts</li> <li>The Self - discover your wise, compassionate essence of goodness that is the source of healing and harmony</li> <li>Exercises for mapping your parts, accessing the Self, working with a challenging protector, identifying each part’s triggers, and more</li> </ul> <p>IFS is a paradigm-changing model because it gives us a powerful approach for healing ourselves, our culture, and our planet. As Dr. Schwartz teaches, “Our parts can sometimes be disruptive or harmful, but once they’re unburdened, they return to their essential goodness. When we learn to love all our parts, we can learn to love all people - and that will contribute to healing the world.”</p>"
Child Category: Seelische & Geistige Gesundheit
Web Player: https://www.audible.com/webplayer?asin=B09GKWKT79
Release Date: 2021-10-12
---
# No Bad Parts: Healing Trauma and Restoring Wholeness with the Internal Family Systems Model

## No Bad Parts

[https://m.media-amazon.com/images/I/51BFS345dcL._SL500_.jpg](https://m.media-amazon.com/images/I/51BFS345dcL._SL500_.jpg)
