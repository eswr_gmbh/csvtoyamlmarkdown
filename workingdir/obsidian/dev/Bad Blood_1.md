---
Search In Goodreads: https://www.goodreads.com/search?q=John%20Carreyrou%20-%20Bad%20Blood
Authors: John Carreyrou
Ratings: 148
Isbn 10: 1524731668
Format: Ungekürztes Hörbuch
Language: English
Publishers: Macmillan Digital Audio
Isbn 13: 9781524731663
Tags: 
    - True Crime
Added: 20
Progress: Beendet
Categories: 
    - Biografien & Erinnerungen 
    - True Crime
Sample: https://samples.audible.de/bk/macm/001924/bk_macm_001924_sample.mp3
Asin: B07CY3CC3T
Title: "Bad Blood: Secrets and Lies in a Silicon Valley Startup"
Title Short: "Bad Blood_1"
Narrators: Will Damron
Blurb: "The full inside story of the breathtaking rise and shocking collapse of a multibillion-dollar start-up, by the prize-winning journalist who first broke the story and pursued it to the end...."
Cover: https://m.media-amazon.com/images/I/41O5blDy52L._SL500_.jpg
Parent Category: Biografien & Erinnerungen
Store Page Url: https://audible.de/pd/B07CY3CC3T?ipRedirectOverride=true&overrideBaseCountry=true
Length: 11h 36m 
Summary: "<p>The shocking true story behind <i>The Dropout</i>, starring Amanda Seyfried, Naveen Andrews and Stephen Fry.</p> <p>Winner of the <i>Financial Times</i>/McKinsey Business Book of the Year Award 2018.</p> <p>The full inside story of the breathtaking rise and shocking collapse of Theranos, the multibillion-dollar biotech startup, by the prize-winning journalist who first broke the story and pursued it to the end, despite pressure from its charismatic CEO and threats by her lawyers.</p> <p>In 2014, Theranos founder and CEO Elizabeth Holmes was widely seen as the female Steve Jobs: a brilliant Stanford dropout whose startup 'unicorn' promised to revolutionise the medical industry with a machine that would make blood testing significantly faster and easier. Backed by investors such as Larry Ellison and Tim Draper, Theranos sold shares in a fundraising round that valued the company at more than $9 billion, putting Holmes' worth at an estimated $4.7 billion. There was just one problem: the technology didn't work.</p> <p>In <i>Bad Blood</i>, John Carreyrou tells the riveting story of the biggest corporate fraud since Enron, a tale of ambition and hubris set amid the bold promises of Silicon Valley.</p>"
Child Category: True Crime
Web Player: https://www.audible.com/webplayer?asin=B07CY3CC3T
Release Date: 2018-05-31
---
# Bad Blood: Secrets and Lies in a Silicon Valley Startup

## Bad Blood

[https://m.media-amazon.com/images/I/41O5blDy52L._SL500_.jpg](https://m.media-amazon.com/images/I/41O5blDy52L._SL500_.jpg)
