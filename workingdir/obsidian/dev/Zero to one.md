---
Search In Goodreads: https://www.goodreads.com/search?q=Peter%20Thiel%20-%20Zero%20to%20one
Authors: Peter Thiel, Blake Masters
Ratings: 1406
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: ABOD Verlag
Tags: 
    - Management & Leadership
    - Geschäftsentwicklung & Unternehmertum
Added: 11
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Management & Leadership
Sample: https://samples.audible.de/bk/xaod/000267/bk_xaod_000267_sample.mp3
Asin: B00OGB88US
Title: "Zero to one: Mutig denken. Neues wagen. Zukunft bauen."
Title Short: "Zero to one"
Narrators: Matthias Lühn
Blurb: "Wir leben in einer technologischen Sackgasse. Zwar suggeriert die Globalisierung technischen Fortschritt, doch das vermeintlich Neue, sind vor allem Kopien..."
Cover: https://m.media-amazon.com/images/I/41tTGigH8tL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B00OGB88US?ipRedirectOverride=true&overrideBaseCountry=true
Length: 4h 53m 
Summary: "Wir leben in einer technologischen Sackgasse. Zwar suggeriert die Globalisierung technischen Fortschritt, doch das vermeintlich Neue, sind vor allem Kopien des Bestehenden. Thiel, Peter, Silicon-Valley-Insider und in der Wirtschaftscommunity bestens bekannter Innovationstreiber ist überzeugt: Globalisierung ist kein Fortschritt, Konkurrenz ist schädlich und nur Monopole sind nachhaltig erfolgreich. Er zeigt: Wahre Innovation entsteht nicht horizontal, sondern sprunghaft - from Zero to One. <br> <br>Um die Zukunft zu erobern, reicht es nicht, der Beste zu sein. Gründer müssen aus dem Wettkampf des \"Immergleichen\" heraustreten und völlig neue Märkte erobern. Wie man wirklich Neues erfindet, enthüllt seine beeindruckende Anleitung zum visionären Querdenken. Ein Appell für einen Start-up der ganzen Gesellschaft."
Child Category: Management & Leadership
Web Player: https://www.audible.com/webplayer?asin=B00OGB88US
Release Date: 2014-10-14
---
# Zero to one: Mutig denken. Neues wagen. Zukunft bauen.

## Zero to one

[https://m.media-amazon.com/images/I/41tTGigH8tL._SL500_.jpg](https://m.media-amazon.com/images/I/41tTGigH8tL._SL500_.jpg)
