---
Search In Goodreads: https://www.goodreads.com/search?q=Peter%20F.%20Hamilton%20-%20Befreiung
Authors: Peter F. Hamilton, Wolfgang Thon - Übersetzer
Ratings: 1958
Isbn 10: 3492705057
Format: Ungekürztes Hörbuch
My Rating: 4
Language: German
Publishers: HörbucHHamburg HHV GmbH
Isbn 13: 9783492705059
Book Numbers: 1
Tags: 
    - Weltraumerkundung
Added: 98
Progress: Beendet
Categories: 
    - Science Fiction & Fantasy 
    - Science Fiction
Sample: https://samples.audible.de/bk/hamb/002668/bk_hamb_002668_sample.mp3
Asin: B07L5NBM46
Title: "Befreiung: Die Salvation-Saga 1"
Title Short: "Befreiung"
Narrators: Oliver Siebeck
Blurb: "Das 22. Jahrhundert: Die Menschen haben Raumschiffe zu mehreren Sternen ausgesandt und begonnen, Planeten zu terraformen. Durch Portalsysteme..."
Series: Die Salvation-Saga (book 1)
Cover: https://m.media-amazon.com/images/I/51eTRQaXR6L._SL500_.jpg
Parent Category: Science Fiction & Fantasy
Store Page Url: https://audible.de/pd/B07L5NBM46?ipRedirectOverride=true&overrideBaseCountry=true
Length: 23h 14m 
Summary: "<p>Mit <i>Befreiung</i> beginnt Peter F. Hamilton eine neue, großangelegte Science-Fiction-Saga.</p> <p>Das 22. Jahrhundert: Die Menschen haben Raumschiffe zu mehreren Sternen ausgesandt und begonnen, Planeten zu terraformen. Durch Portalsysteme miteinander verbunden, können Reisende in Nullzeit zwischen den Welten hin- und herspringen. Bei der Erforschung des Alls stoßen die Menschen im Jahr 2150 auf ein gigantisches, außerirdisches Schiff. Es gehört den Kax, einer uralten Rasse, die sich auf einer epischen Reise bis zum Ende der Zeit befindet. Doch die Kax sind nicht so friedlich gesinnt, wie sie vorgeben. Ihre Mission ist geheim, unerbittlich - und brandgefährlich für die gesamte Menschheit...</p>"
Child Category: Science Fiction
Web Player: https://www.audible.com/webplayer?asin=B07L5NBM46
Release Date: 2018-12-06
---
# Befreiung: Die Salvation-Saga 1

## Befreiung

### Die Salvation-Saga (book 1)

[https://m.media-amazon.com/images/I/51eTRQaXR6L._SL500_.jpg](https://m.media-amazon.com/images/I/51eTRQaXR6L._SL500_.jpg)
