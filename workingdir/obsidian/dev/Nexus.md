---
Search In Goodreads: https://www.goodreads.com/search?q=Ramez%20Naam%20-%20Nexus
Authors: Ramez Naam
Ratings: 3698
Isbn 10: 345331560X
Format: Ungekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Ronin Hörverlag
Isbn 13: 9783453315600
Book Numbers: 1
Tags: 
    - Technothriller
    - Fantasy
    - Science Fiction
Added: 109
Progress: Beendet
Categories: 
    - Krimis & Thriller 
    - Thriller
Sample: https://samples.audible.de/bk/klan/000028/bk_klan_000028_sample.mp3
Asin: B00UNL0HVI
Title: "Nexus: Nexus-Trilogie 1"
Title Short: "Nexus"
Narrators: Uve Teschner
Blurb: "Die Nano-Droge ermöglicht es einem, sich mental mit anderen Menschen und mit den Datenströmen des Internets zu verbinden..."
Series: Nexus-Trilogie (book 1)
Cover: https://m.media-amazon.com/images/I/51giLVzXfFL._SL500_.jpg
Parent Category: Krimis & Thriller
Store Page Url: https://audible.de/pd/B00UNL0HVI?ipRedirectOverride=true&overrideBaseCountry=true
Length: 15h 10m 
Summary: "Die Nano-Droge ermöglicht es einem, sich mental mit anderen Menschen und mit den Datenströmen des Internets zu verbinden. Der US-Behörde ERD ist Nexus jedoch ein Dorn im Auge.Sie zwingen den jungen Programmierer Kaden Lane, einen der Miterfinder von Nexus, sich bei einer skrupellosen chinesischen Wissenschaftlerin einzuschleusen. Sie plant, die gesamte Menschheit zu unterwerfen. Doch auch die Behörden gehen für Nexus über Leichen."
Child Category: Thriller
Web Player: https://www.audible.com/webplayer?asin=B00UNL0HVI
Release Date: 2015-03-16
---
# Nexus: Nexus-Trilogie 1

## Nexus

### Nexus-Trilogie (book 1)

[https://m.media-amazon.com/images/I/51giLVzXfFL._SL500_.jpg](https://m.media-amazon.com/images/I/51giLVzXfFL._SL500_.jpg)
