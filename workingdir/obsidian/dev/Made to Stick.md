---
Search In Goodreads: https://www.goodreads.com/search?q=Chip%20Heath%20-%20Made%20to%20Stick
Authors: Chip Heath, Dan Heath
Ratings: 208
Isbn 10: 1400064287
Format: Ungekürztes Hörbuch
My Rating: 5
Language: English
Publishers: Random House Audio
Isbn 13: 9781400064281
Tags: 
    - Erfolg im Beruf
    - Sprache
    - Vokabeln & Grammatik
    - Seelische & Geistige Gesundheit
    - Politik & Sozialwissenschaften
    - Kommunikation & soziale Kompetenz
Added: 93
Progress: Beendet
Categories: 
    - Wirtschaft & Karriere 
    - Erfolg im Beruf
Sample: https://samples.audible.de/bk/rand/000995/bk_rand_000995_sample.mp3
Asin: B004V14R6W
Title: "Made to Stick: Why Some Ideas Survive and Others Die"
Title Short: "Made to Stick"
Narrators: Charles Kahlenberg
Blurb: "Why do some ideas thrive while others die? And how do we improve the chances of worthy ideas...."
Cover: https://m.media-amazon.com/images/I/51fhNaNM87L._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B004V14R6W?ipRedirectOverride=true&overrideBaseCountry=true
Length: 8h 37m 
Summary: "Mark Twain once observed, \"A lie can get halfway around the world before the truth can even get its boots on.\" His observation rings true: Urban legends, conspiracy theories, and bogus public-health scares circulate effortlessly. Meanwhile, people with important ideas (business people, teachers, politicians, journalists, and others) struggle to make their ideas \"stick\". <p> Why do some ideas thrive while others die? And how do we improve the chances of worthy ideas? In <i>Made to Stick</i>, accomplished educators and idea collectors Chip and Dan Heath tackle head-on these vexing questions. Inside, the brothers Heath reveal the anatomy of ideas that stick and explain ways to make ideas stickier, such as applying the \"human scale principle\", using the \"Velcro Theory of Memory\", and creating \"curiosity gaps\".</p> <p> In this indispensable guide, we discover that sticky messages of all kinds (from the infamous \"kidney theft ring\" hoax to a coach's lessons on sportsmanship, to a new-product vision at Sony) draw their power from the same six traits.</p> <p> <i>Made to Stick</i> is a book that will transform the way you communicate ideas. It includes a fast-paced tour of success stories (and failures), such as the Nobel Prize-winning scientist who drank a glass full of bacteria to prove a point about stomach ulcers, the charities who make use of \"the Mother Teresa Effect\", and the elementary school teacher whose simulation actually prevented racial prejudice. Provocative, eye-opening, and often surprisingly funny, <i>Made to Stick</i> shows us the vital principles of winning ideas and tells us how we can apply these rules to making our own messages stick.</p>"
Child Category: Erfolg im Beruf
Web Player: https://www.audible.com/webplayer?asin=B004V14R6W
Release Date: 2006-12-18
---
# Made to Stick: Why Some Ideas Survive and Others Die

## Made to Stick

[https://m.media-amazon.com/images/I/51fhNaNM87L._SL500_.jpg](https://m.media-amazon.com/images/I/51fhNaNM87L._SL500_.jpg)
