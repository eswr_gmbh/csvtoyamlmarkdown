---
Search In Goodreads: https://www.goodreads.com/search?q=Martin%20Wehrle%20-%20Sei%20einzig%2C%20nicht%20artig!
Authors: Martin Wehrle
Ratings: 556
Isbn 10: 3641154596
Format: Gekürztes Hörbuch
My Rating: 5
Language: German
Publishers: Lagato Verlag
Isbn 13: 9783641154592
Tags: 
    - Medienwissenschaften
Added: 127
Progress: Beendet
Categories: 
    - Politik & Sozialwissenschaften 
    - Sozialwissenschaften
Sample: https://samples.audible.de/bk/laga/000112/bk_laga_000112_sample.mp3
Asin: B015Y00Y4M
Title: "Sei einzig, nicht artig!: So sagen Sie nie mehr JA, wenn Sie NEIN sagen wollen"
Title Short: "Sei einzig, nicht artig!"
Narrators: Helge Heynold
Blurb: "Nie war die Gefahr so groß wie heute, die eigenen Wünsche und Träume zu verraten. Der moderne Mensch lebt für die Arbeit..."
Cover: https://m.media-amazon.com/images/I/51E0IXffv-L._SL500_.jpg
Parent Category: Politik & Sozialwissenschaften
Store Page Url: https://audible.de/pd/B015Y00Y4M?ipRedirectOverride=true&overrideBaseCountry=true
Length: 5h 11m 
Summary: "Nie war die Gefahr so groß wie heute, die eigenen Wünsche und Träume zu verraten. Der moderne Mensch lebt für die Arbeit, für die Familie oder für den Facebook-Account, aber nicht mehr für sich selbst. Die Medien sagen uns, was wir denken sollen; die Modedesigner, wie wir uns zu kleiden haben; die Arbeitgeber, womit wir den Tag verbringen müssen. Oft sind Burnout und Depression die Folge dieser Angepasstheit. <br> <br>Erfolgsautor Martin Wehrle fordert uns deshalb dazu auf, nichts mehr nur für andere zu tun, sondern alles für uns selbst. Gemäß dem Motto: Sei einzig, nicht artig!"
Child Category: Sozialwissenschaften
Web Player: https://www.audible.com/webplayer?asin=B015Y00Y4M
Release Date: 2015-09-29
---
# Sei einzig, nicht artig!: So sagen Sie nie mehr JA, wenn Sie NEIN sagen wollen

## Sei einzig, nicht artig!

[https://m.media-amazon.com/images/I/51O9E538tYL._SL500_.jpg](https://m.media-amazon.com/images/I/51O9E538tYL._SL500_.jpg)
