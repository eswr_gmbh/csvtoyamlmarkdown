---
Search In Goodreads: https://www.goodreads.com/search?q=Ayn%20Rand%20-%20Der%20Ursprung
Authors: Ayn Rand
Ratings: 123
Isbn 10: 3932564367
Format: Ungekürztes Hörbuch
Language: German
Publishers: ABP Verlag
Isbn 13: 9783932564369
Tags: 
    - Klassiker
Added: 189
Progress: 11 Std. 4 Min. verbleibend
Categories: 
    - Literatur & Belletristik 
    - Klassiker
Sample: https://samples.audible.de/bk/abpu/000052/bk_abpu_000052_sample.mp3
Asin: 162861238X
Title: "Der Ursprung"
Title Short: "Der Ursprung"
Narrators: Uwe Thoma
Blurb: "Der Ursprung, Ayn Rands zeitloser Klassiker von 1943, beschreibt seinen Helden Howard Roark als einen Menschen, der die Motivation für..."
Cover: https://m.media-amazon.com/images/I/51qeWWZiJIL._SL500_.jpg
Parent Category: Literatur & Belletristik
Store Page Url: https://audible.de/pd/162861238X?ipRedirectOverride=true&overrideBaseCountry=true
Length: 37h 21m 
Summary: "<p>Von der Autorin des Romans \"Atlas wirft die Welt ab\".</p> <p>Weltweiter Bestseller! Mehr als 6,5 Millionen verkaufte Exemplare!</p> <p>Der Ursprung, Ayn Rands zeitloser Klassiker von 1943, beschreibt seinen Helden Howard Roark als einen Menschen, der die Motivation für seine Arbeit aus sich selbst schöpft und nicht um den Applaus anderer bemüht ist. Dieser junge Architekt kämpft nicht nur für die künstlerische Vision seiner Bauten, sondern gegen die Maßstäbe der \"Autoritäten\" und gegen die Herrschaft der Mittelmäßigkeit, der Gleichmacherei und der Beliebigkeit - und gegen eine atemberaubend schöne Frau, die ihn leidenschaftlich liebt, aber seinen schlimmsten Feind heiratet.</p> <p>Roark geht seinen Weg ohne überbordende Emotionen oder Verwunderung. Er geht ihn einfach, weil es für ihn so sein muss.</p> <p>Roarks Gegner besitzen kein Selbst, sie leben in einer anderen Welt, eben aus zweiter Hand. Sie fühlen sich angesichts der Naturgewalten klein und hilflos, während schöpfende Menschen wie Roark von den Naturgewalten an die Größe des menschlichen Gestaltungswillens erinnert werden.</p> <p>Ayn Rand schrieb: \"Bestimmte Schriftsteller, von denen ich einer bin, leben, denken oder schreiben nicht für die Spanne des Augenblicks. Romane, im korrekten Wortsinn, werden nicht geschrieben, um nach einem Monat oder einem Jahr zu verschwinden\". Und sie hatte Recht. Nach mehr als 70 Jahren ist Der Ursprung immer noch eines der beliebtesten Bücher der Weltliteratur.</p>"
Child Category: Klassiker
Web Player: https://www.audible.com/webplayer?asin=162861238X
Release Date: 2020-06-12
---
