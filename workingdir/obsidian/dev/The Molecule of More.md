---
Search In Goodreads: https://www.goodreads.com/search?q=Daniel%20Z.%20Lieberman%20MD%20-%20The%20Molecule%20of%20More
Authors: Daniel Z. Lieberman MD, Michael E. Long
Ratings: 113
Isbn 10: 1948836580
Format: Ungekürztes Hörbuch
My Rating: 5
Language: English
Publishers: Brilliance Audio
Isbn 13: 9781948836586
Tags: 
    - Gesundheit & Wellness
    - Seelische & Geistige Gesundheit
    - Wissenschaft
Added: 178
Categories: 
    - Gesundheit & Wellness 
    - Seelische & Geistige Gesundheit
Sample: https://samples.audible.de/bk/brll/011173/bk_brll_011173_sample.mp3
Asin: B07HRZ5HBT
Title: "The Molecule of More: How a Single Chemical in Your Brain Drives Love, Sex, and Creativity - And Will Determine the Fate of the Human Race"
Title Short: "The Molecule of More"
Narrators: Tom Parks
Blurb: "Why is addiction “perfectly logical” to an addict? The answer is found in a single chemical in your brain: dopamine. Dopamine ensured the survival of early man. Thousands of years later, it is the source of our most basic behaviors and cultural ideas - and progress itself...."
Cover: https://m.media-amazon.com/images/I/41rju8FkUFL._SL500_.jpg
Parent Category: Gesundheit & Wellness
Store Page Url: https://audible.de/pd/B07HRZ5HBT?ipRedirectOverride=true&overrideBaseCountry=true
Length: 8 Std. 13 Min.
Summary: "<p><i>Why are we obsessed with the things we want and bored when we get them?</i></p> <p><i>Why is addiction “perfectly logical” to an addict?</i></p> <p><i>Why does love change so quickly from passion to disinterest?</i></p> <p><i>Why are some people diehard liberals and others hardcore conservatives?</i></p> <p><i>Why are we always hopeful for solutions even in the darkest times - and so good at figuring them out?</i> </p> <p>The answer is found in a single chemical in your brain: <i>dopamine</i>. Dopamine ensured the survival of early man. Thousands of years later, it is the source of our most basic behaviors and cultural ideas - and progress itself. </p> <p>Dopamine is the chemical of desire that always asks for more - more stuff, more stimulation, and more surprises. In pursuit of these things, it is undeterred by emotion, fear, or morality. Dopamine is the source of our every urge, that little bit of biology that makes an ambitious business professional sacrifice everything in pursuit of success, or that drives a satisfied spouse to risk it all for the thrill of someone new. Simply put, it is why we seek and succeed; it is why we discover and prosper. Yet, at the same time, it’s why we gamble and squander. </p> <p>From dopamine’s point of view, it’s not the <i>having </i>that matters. It’s getting something - anything - that’s new. From this understanding - the difference between possessing something versus anticipating it - we can understand <i>in a revolutionary new way</i> why we behave as we do in love, business, addiction, politics, religion - and we can even predict those behaviors in ourselves and others. </p> <p>In <i>The Molecule of More: How a Single Chemical in Your Brain Drives Love, Sex, and Creativity—And will Determine the Fate of the Human Race</i>, George Washington University professor and psychiatrist Daniel Z. Lieberman, MD, and Georgetown University lecturer Michael E. Long present a potentially life-changing proposal: Much of human life has an unconsidered component that explains an array of behaviors previously thought to be unrelated, including why winners cheat, why geniuses often suffer with mental illness, why nearly all diets fail, and why the brains of liberals and conservatives really <i>are </i>different.</p>"
Child Category: Seelische & Geistige Gesundheit
Web Player: https://www.audible.com/webplayer?asin=B07HRZ5HBT
Release Date: 2018-10-01
---
# The Molecule of More: How a Single Chemical in Your Brain Drives Love, Sex, and Creativity - And Will Determine the Fate of the Human Race

## The Molecule of More

![https://m.media-amazon.com/images/I/41rju8FkUFL._SL500_.jpg](https://m.media-amazon.com/images/I/41rju8FkUFL._SL500_.jpg)
