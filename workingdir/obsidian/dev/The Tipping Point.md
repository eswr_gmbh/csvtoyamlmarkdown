---
Search In Goodreads: https://www.goodreads.com/search?q=Malcolm%20Gladwell%20-%20The%20Tipping%20Point
Authors: Malcolm Gladwell
Isbn 10: 1587243938
Format: Ungekürztes Hörbuch
Language: English
Publishers: Hachette Audio UK
Isbn 13: 9781587243936
Tags: 
    - Marketing & Vertrieb
    - Popularkultur
Added: 201
Progress: 7 Std. 23 Min. verbleibend
Categories: 
    - Wirtschaft & Karriere 
    - Marketing & Vertrieb
Sample: https://samples.audible.de/bk/twuk/003671/bk_twuk_003671_sample.mp3
Asin: B0BPTBQGS3
Title: "The Tipping Point: How Little Things Can Make a Big Difference"
Title Short: "The Tipping Point"
Narrators: Malcolm Gladwell
Blurb: "In this brilliant and original audiobook, Malcolm Gladwell explains and analyses the 'tipping point', that magic moment when ideas, trends and social behaviour cross a threshold, tip and spread like wildfire...."
Cover: https://m.media-amazon.com/images/I/41g0t-80XiL._SL500_.jpg
Parent Category: Wirtschaft & Karriere
Store Page Url: https://audible.de/pd/B0BPTBQGS3?ipRedirectOverride=true&overrideBaseCountry=true
Length: 8 Std. 34 Min.
Summary: "<p>In this brilliant and original audiobook, Malcolm Gladwell explains and analyses the 'tipping point', that magic moment when ideas, trends and social behaviour cross a threshold, tip and spread like wildfire. Taking a look behind the surface of many familiar occurrences in our everyday world, Gladwell explains the fascinating social dynamics that cause rapid change.</p>"
Child Category: Marketing & Vertrieb
Web Player: https://www.audible.com/webplayer?asin=B0BPTBQGS3
Release Date: 2023-01-26
---
