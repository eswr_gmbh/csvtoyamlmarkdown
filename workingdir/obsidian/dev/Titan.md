---
Search In Goodreads: https://www.goodreads.com/search?q=Ron%20Chernow%20-%20Titan
Authors: Ron Chernow
Ratings: 83
Isbn 10: 1788541588
Format: Ungekürztes Hörbuch
My Rating: 3
Language: English
Publishers: Blackstone Audio, Inc.
Isbn 13: 9781788541589
Tags: 
    - Historisch
    - Wirtschaft
    - Wirtschaft & Karriere
    - Ökonomie
    - Nord-
    - Mittel- & Südamerika
Added: 96
Progress: 20 Std. 26 Min. verbleibend
Categories: 
    - Biografien & Erinnerungen 
    - Historisch
Sample: https://samples.audible.de/bk/blak/005821/bk_blak_005821_sample.mp3
Asin: B00ECGL1Y2
Title: "Titan: The Life of John D. Rockefeller, Sr."
Title Short: "Titan"
Narrators: Grover Gardner
Blurb: "Titan is the first full-length biography based on unrestricted access to Rockefeller’s exceptionally rich trove of papers. A landmark publication full of startling revelations, the book indelibly alters our image of this most enigmatic capitalist...."
Cover: https://m.media-amazon.com/images/I/518u3PrQSAS._SL500_.jpg
Parent Category: Biografien & Erinnerungen
Store Page Url: https://audible.de/pd/B00ECGL1Y2?ipRedirectOverride=true&overrideBaseCountry=true
Length: 35h 3m 
Summary: "<p>John D. Rockefeller, Sr., history’s first billionaire and the patriarch of America’s most famous dynasty, is an icon whose true nature has eluded three generations of historians. Now Ron Chernow, a National Book Award-winning biographer, gives us a detailed and insightful history of the mogul. <i>Titan</i> is the first full-length biography based on unrestricted access to Rockefeller’s exceptionally rich trove of papers. A landmark publication full of startling revelations, the book indelibly alters our image of this most enigmatic capitalist. </p> <p>Born the son of a flamboyant, bigamous snake-oil salesman and a pious, straitlaced mother, Rockefeller rose from rustic origins to become the world’s richest man by creating America’s most powerful and feared monopoly, Standard Oil. Branded \"the Octopus\" by legions of muckrakers, the trust refined and marketed nearly 90 percent of the oil produced in America. </p> <p>Rockefeller was likely the most controversial businessman in our nation’s history. Critics charged that his empire was built on unscrupulous tactics: grand-scale collusion with the railroads, predatory pricing, industrial espionage, and wholesale bribery of political officials. The titan spent more than 30 years dodging investigations until Teddy Roosevelt and his trustbusters embarked on a marathon crusade to bring Standard Oil to bay. </p> <p>While providing abundant evidence of Rockefeller’s misdeeds, Chernow discards the stereotype of the cold-blooded monster to sketch an unforgettably human portrait of a quirky, eccentric original. A devout Baptist and temperance advocate, Rockefeller gave money more generously than anyone before him - his chosen philanthropies included the Rockefeller Foundation, the University of Chicago, and what is today Rockefeller University. <i>Titan</i> presents a finely nuanced portrait of a fascinating, complex man, synthesizing his public and private lives and disclosing numerous family scandals, tragedies, and misfortunes that have never before come to light. </p> <p>John D. Rockefeller’s story captures a pivotal moment in American history, documenting the dramatic post–Civil War shift from small business to the rise of giant corporations that irrevocably transformed the nation. With cameos by Joseph Pulitzer, William Randolph Hearst, Jay Gould, William Vanderbilt, Ida Tarbell, Andrew Carnegie, Carl Jung, J. P. Morgan, William James, Henry Clay Frick, Mark Twain, and Will Rogers, <i>Titan</i> turns Rockefeller’s life into a vivid tapestry of American society in the late 19th and early 20th centuries. It is Ron Chernow’s signal triumph that he writes this monumental saga with all the sweep, drama, and insight that this giant subject deserves.</p>"
Child Category: Historisch
Web Player: https://www.audible.com/webplayer?asin=B00ECGL1Y2
Release Date: 2012-12-30
---
# Titan: The Life of John D. Rockefeller, Sr.

## Titan

[https://m.media-amazon.com/images/I/518u3PrQSAS._SL500_.jpg](https://m.media-amazon.com/images/I/518u3PrQSAS._SL500_.jpg)
