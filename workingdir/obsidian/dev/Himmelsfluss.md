---
Search In Goodreads: https://www.goodreads.com/search?q=Dennis%20E.%20Taylor%20-%20Himmelsfluss
Authors: Dennis E. Taylor, Urban Hofstetter - Übersetzer
Ratings: 3143
Format: Ungekürztes Hörbuch
Language: German
Publishers: Random House Audio, Deutschland
Book Numbers: 4
Tags: 
    - Naturwissenschaften
    - Weltraumerkundung
Added: 167
Progress: Beendet
Categories: 
    - Science Fiction & Fantasy 
    - Science Fiction
Sample: https://samples.audible.de/bk/rhde/004425/bk_rhde_004425_sample.mp3
Asin: B09WRQ17VR
Title: "Himmelsfluss: Bobiverse 4"
Title Short: "Himmelsfluss"
Narrators: Simon Jäger
Blurb: "Hundert Jahre ist es jetzt her, dass Bender sich auf den Weg zu den Sternen gemacht hat. Seither haben Bob und die Klone nichts mehr von ihm..."
Series: Bobiverse (book 4)
Cover: https://m.media-amazon.com/images/I/51PqX0aWr5L._SL500_.jpg
Parent Category: Science Fiction & Fantasy
Store Page Url: https://audible.de/pd/B09WRQ17VR?ipRedirectOverride=true&overrideBaseCountry=true
Length: 17h 23m 
Summary: "<p>Hundert Jahre ist es jetzt her, dass Bender sich auf den Weg zu den Sternen gemacht hat. Seither haben Bob und die Klone nichts mehr von ihm gehört. Bob ist fest entschlossen, eine Expedition in den Deep Space zu organisieren, um herauszufinden, was mit Bender geschehen ist, doch er hat ein gewaltiges Problem: Inzwischen sind seine Klone in der 24. Generation. Manche von ihnen haben sich so verändert, dass sie gar nicht mehr er selbst zu sein scheinen. Sie haben ihre eigenen Pläne. Und so droht über Bobs Suchaktion ein Bürgerkrieg zwischen den Klonen auszubrechen...</p> <p>>> Diese ungekürzte Hörbuch-Fassung genießt du exklusiv nur bei Audible.</p>"
Child Category: Science Fiction
Web Player: https://www.audible.com/webplayer?asin=B09WRQ17VR
Release Date: 2022-03-29
---
# Himmelsfluss: Bobiverse 4

## Himmelsfluss

### Bobiverse (book 4)

[https://m.media-amazon.com/images/I/51PqX0aWr5L._SL500_.jpg](https://m.media-amazon.com/images/I/51PqX0aWr5L._SL500_.jpg)
