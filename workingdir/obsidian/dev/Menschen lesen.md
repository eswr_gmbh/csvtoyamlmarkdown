---
Search In Goodreads: https://www.goodreads.com/search?q=Joe%20Navarro%20-%20Menschen%20lesen
Authors: Joe Navarro
Ratings: 3262
Isbn 10: 3868822135
Format: Gekürztes Hörbuch
My Rating: 5
Language: German
Publishers: mvg Verlag
Isbn 13: 9783868822137
Tags: 
    - Kommunikation & soziale Kompetenz
Added: 123
Categories: 
    - Beziehungen
    - Elternschaft & persönliche Entwicklung 
    - Persönliche Entwicklung
Sample: https://samples.audible.de/bk/riva/000014/bk_riva_000014_sample.mp3
Asin: B00UN0J5CQ
Title: "Menschen lesen: Ein FBI-Agent erklärt, wie man Körpersprache entschlüsselt"
Title Short: "Menschen lesen"
Narrators: Michael J. Diekmann
Blurb: "Als FBI-Agent war es 25 Jahre lang Joe Navarros Aufgabe, Spione, Mörder und Verbrecher anhand ihrer Körpersprache zu entlarven..."
Cover: https://m.media-amazon.com/images/I/61-x4sh0nPL._SL500_.jpg
Parent Category: Beziehungen, Elternschaft & persönliche Entwicklung
Store Page Url: https://audible.de/pd/B00UN0J5CQ?ipRedirectOverride=true&overrideBaseCountry=true
Length: 4 Std. 42 Min.
Summary: "Als FBI-Agent war es 25 Jahre lang Joe Navarros Aufgabe, Spione, Mörder und Verbrecher anhand ihrer Körpersprache zu entlarven. Denn nur 20 Prozent unserer Kommunikation laufen über das gesprochene Wort. Zu 80 Prozent kommunizieren wir nonverbal und unbewusst. In diesem Hörbuch erklärt Navarro, wie man sein Gegenüber durchschaut, wie man Gefühle und Verhaltensweisen präzise entschlüsselt, Fallstricken ausweicht und souverän Körperhaltung und Mimik entlarvt, die in die Irre führen sollen."
Child Category: Persönliche Entwicklung
Web Player: https://www.audible.com/webplayer?asin=B00UN0J5CQ
Release Date: 2015-04-13
---
# Menschen lesen: Ein FBI-Agent erklärt, wie man Körpersprache entschlüsselt

## Menschen lesen

[https://m.media-amazon.com/images/I/61-x4sh0nPL._SL500_.jpg](https://m.media-amazon.com/images/I/61-x4sh0nPL._SL500_.jpg)
