package ch.eswr;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class CsvToObsidanYaml {

    private static final String sourcePhat = "/Users/mikelinsi/Project/CsvToYamlMarkDown/workingdir/dump/bookes.csv";


    static class Book {
        HashMap<String, String> data = new HashMap<>();

        void add( String key, String val ) {
            this.data.put( key, val );
        }
    }


    public static void main( String[] args ) throws IOException {
        final Properties prop = new Properties();
        final String configFilePath = "/Users/mikelinsi/Project/CsvToYamlMarkDown/workingdir/dump/config.properties";
        final FileInputStream propsInput = new FileInputStream( configFilePath );
        prop.load( propsInput );

        for ( final Map.Entry<Object, Object> objectObjectEntry : prop.entrySet() ) {
            System.out.println( "objectObjectEntry = " + objectObjectEntry );
        }


        final List<Book> bookList = new ArrayList<>();

        final Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse( new FileReader( sourcePhat ) );


        for ( CSVRecord record : records ) {

            Book book = new Book();
            bookList.add( book );
            for ( final Map.Entry<String, Integer> stringIntegerEntry : record.getParser().getHeaderMap().entrySet() ) {
                final String key = stringIntegerEntry.getKey();
                final Integer value = stringIntegerEntry.getValue();

                final String val = StringEscapeUtils.unescapeHtml4( record.get( value ) );

                final String keyForProd = key.replace( " ", "_" );

                final String o = prop.getProperty( keyForProd, "String_Native" );
                if ( StringUtils.isNoneEmpty( o ) ) {
                    switch ( o ) {
                        case "String_Native":
                            book.add( key, val );
                            break;
                        case "String_Escape":
                            book.add( key, "\"" + val.replace( "\"", "\\\"" ) + "\"" );
                            break;
                        case "Array":
                            if ( StringUtils.isNotBlank( val) ) {
                                String vall = "\n    - ";
                                vall = vall + String.join( "\n    -", val.replace( "\"", "\\\"" ).split( ",|>" ) );
                                book.add( key, vall );
                            }
                            break;
                        case "double":
                            if ( StringUtils.isNoneEmpty( val ) ) {
                                try {
                                    Double ignore = Double.valueOf( val );
                                    book.add( key, val );
                                } catch ( Exception e ) {
                                    System.out.println( "usp, not a number on: " + key + " " + e.getMessage() );
                                }
                            } else {
                                book.add( key, "" );
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

        }

        for ( final Book book : bookList ) {
            final Optional<Map.Entry<String, String>> titleShort = book.data.entrySet().stream().filter( x -> x.getKey().equals( "Title Short" ) ).findFirst();
            if ( titleShort.isPresent() ) {
                final List<String> lines = new ArrayList<>();
                lines.add( "---" );
                for ( final Map.Entry<String, String> stringStringEntry : book.data.entrySet() ) {
                    if ( StringUtils.isNoneBlank( stringStringEntry.getValue() ) ) {
                        lines.add( stringStringEntry.getKey() + ": " + stringStringEntry.getValue() );
                    }
                }
                lines.add( "---" );
                final Path file = Paths.get(
                        "/Users/mikelinsi/Library/Mobile Documents/iCloud~md~obsidian/Documents/MainDB/03 Books/OriginalIdeas/" + titleShort.get().getValue().replace( "\"", "" ) + ".md" );
                if ( !Files.exists( file ) ) {
                    Files.write( file, lines, StandardCharsets.UTF_8 );
                } else {
                    final List<String> oldContent = Files.readAllLines( file );

                    // bug, wenn kein einzeinges yaml type vorhanden ist.
                    int countOfYamlTag = 0;
                    for ( final String s : oldContent ) {
                        if ( s.equals( "---" ) ) {
                            countOfYamlTag++;
                            continue;
                        }
                        if ( countOfYamlTag >= 2 ) {
                            lines.add( s );
                        }
                    }
                    Files.write( file, lines, StandardCharsets.UTF_8 );
                }

            }
        }

    }

}
