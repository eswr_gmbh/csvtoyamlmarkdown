package ch.eswr;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringEscapeUtils;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MergeCsvFiles {

    private static final String sourcePhat = "/Users/mikelinsi/Project/CsvToYamlMarkDown/workingdir/dump/";
    private static final String ORIGINAL_FILE = "bookes.csv";
    private static final String updateFileName = "ALE-spreadsheet-library.csv";


    public static void main( String[] args ) throws IOException {
        final Iterable<CSVRecord> dbRecords = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse( new FileReader( sourcePhat + ORIGINAL_FILE ) );
        final Iterable<CSVRecord> updateRecord = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse( new FileReader( sourcePhat + updateFileName ) );

        final LinkedHashMap<String, List<String>> bookLines = new LinkedHashMap<>();
        bookLines.put( "header", ( ( CSVParser ) dbRecords ).getHeaderNames() );
        for ( final CSVRecord dbRecord : dbRecords ) {

            for ( final Map.Entry<String, Integer> stringIntegerEntry : dbRecord.getParser().getHeaderMap().entrySet() ) {
                final String key = stringIntegerEntry.getKey();
                final String value = StringEscapeUtils.unescapeHtml4( dbRecord.get( stringIntegerEntry.getValue() ) );
                if ( key.equals( "Asin" ) ) {
                    bookLines.put( value, dbRecord.toList() );
                }
            }
        }

        for ( final CSVRecord record : updateRecord ) {
            for ( final Map.Entry<String, Integer> stringIntegerEntry : record.getParser().getHeaderMap().entrySet() ) {
                final String key = stringIntegerEntry.getKey();
                final String value = StringEscapeUtils.unescapeHtml4( record.get( stringIntegerEntry.getValue() ) );
                if ( key.equals( "Asin" ) ) {
                    if ( bookLines.get( value ) == null ) {
                        System.out.println( "add update: " + value );
                        bookLines.put( value, record.toList() );
                    } else {
                        System.out.println( "do nothing on:" + value );
                    }
                }
            }
        }

        System.out.println( "bookLines = " + bookLines.size() );

        final Path merged_file = Paths.get( sourcePhat + "merged.csv" );

        try (
                BufferedWriter writer = Files.newBufferedWriter( merged_file );
                CSVPrinter csvPrinter = new CSVPrinter( writer,
                        CSVFormat.DEFAULT.builder().setHeader( bookLines.get( "header" ).toArray( new String[bookLines.get( "header" ).size()] ) ).build() );
        ) {

            for ( final Map.Entry<String, List<String>> stringListEntry : bookLines.entrySet() ) {
                if ( !stringListEntry.getKey().equals( "header" ) ) {
                    csvPrinter.printRecord( stringListEntry.getValue() );
                }
            }

            csvPrinter.flush();
        }


        // todo move old DB to backup
        //  use new merged file as DB
        Path originalFile = Path.of( sourcePhat + ORIGINAL_FILE );
        Path backup = Path.of( sourcePhat + ORIGINAL_FILE + "_" + new Date().getTime() );
        Files.move( originalFile, backup );
        Files.move( merged_file, originalFile );


    }
}
